DROP TABLE IF EXISTS item_model CASCADE;

CREATE TABLE item_model (
    id bigint not null primary key,
    string_field varchar(255),
    long_field bigint,
    integer_field integer,
    boolean_field boolean

);

INSERT INTO item_model (id, string_field, long_field, integer_field, boolean_field) VALUES (1, 'value', 100, 100, false);