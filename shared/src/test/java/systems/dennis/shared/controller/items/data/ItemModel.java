package systems.dennis.shared.controller.items.data;

import jakarta.persistence.Entity;
import lombok.Data;
import systems.dennis.shared.entity.BaseEntity;

@Data
@Entity
public class ItemModel extends BaseEntity {

    private String stringField;

    private Long longField;

    private Integer integerField;

    private Boolean booleanField;
}
