package systems.dennis.shared.controller.items.data;

import lombok.Data;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.pojo_form.PojoFormElement;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.pojo_view.list.PojoListViewField;

import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data
@PojoListView(actions = {})
public class ItemForm implements DefaultForm {

    @PojoFormElement(type = HIDDEN)
    private Long id;

    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private String stringField;

    @PojoFormElement(required = true)
    private Long longField;

    @PojoFormElement(required = true)
    private Integer integerField;

    @PojoFormElement(required = true)
    private Boolean booleanField;
}
