package systems.dennis.shared.controller.items.data;

import org.springframework.stereotype.Repository;
import systems.dennis.shared.repository.PaginationRepository;

@Repository
public interface ItemRepo extends PaginationRepository<ItemModel> {
}
