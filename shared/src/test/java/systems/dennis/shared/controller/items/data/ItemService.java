package systems.dennis.shared.controller.items.data;

import org.springframework.stereotype.Service;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.service.PaginationService;

@Service
@DataRetrieverDescription(model = ItemModel.class, form = ItemForm.class, repo = ItemRepo.class)
public class ItemService extends PaginationService<ItemModel> {

    public ItemService(WebContext holder) {
        super(holder);
    }
}
