package systems.dennis.shared.controller.items;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import systems.dennis.shared.annotations.security.ISecurityUtils;
import systems.dennis.shared.entity.KeyValue;

@SpringBootTest
@AutoConfigureMockMvc
@Sql(value = "/data.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class EditFieldItemControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private ISecurityUtils securityUtilsMock;

    @Test
    void shouldUpdateEntityWhenValidValuesProvided() throws Exception {
        Mockito.doNothing().when(securityUtilsMock).assignUser(Mockito.any(), Mockito.any());
        KeyValue keyValue = new KeyValue("stringField", "test value");

        mockMvc.perform(MockMvcRequestBuilders.put("/edit_test/edit_field/id/1")
                        .content(objectMapper.writeValueAsString(keyValue))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.keyValue.key").value("stringField"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.keyValue.value").value("test value"));
    }

    @Test
    void shouldUpdateEntityWithLongValue() throws Exception {
        Mockito.doNothing().when(securityUtilsMock).assignUser(Mockito.any(), Mockito.any());
        KeyValue keyValue = new KeyValue("longField", 12345L);

        mockMvc.perform(MockMvcRequestBuilders.put("/edit_test/edit_field/id/1")
                        .content(objectMapper.writeValueAsString(keyValue))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.keyValue.key").value("longField"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.keyValue.value").value(12345L));
    }

    @Test
    void shouldUpdateEntityWithIntegerValue() throws Exception {
        Mockito.doNothing().when(securityUtilsMock).assignUser(Mockito.any(), Mockito.any());
        KeyValue keyValue = new KeyValue("integerField", 12345L);

        mockMvc.perform(MockMvcRequestBuilders.put("/edit_test/edit_field/id/1")
                        .content(objectMapper.writeValueAsString(keyValue))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.keyValue.key").value("integerField"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.keyValue.value").value(12345L));
    }

    @Test
    void shouldUpdateEntityWithBooleanValue() throws Exception {
        Mockito.doNothing().when(securityUtilsMock).assignUser(Mockito.any(), Mockito.any());
        KeyValue keyValue = new KeyValue("booleanField", true);

        mockMvc.perform(MockMvcRequestBuilders.put("/edit_test/edit_field/id/1")
                        .content(objectMapper.writeValueAsString(keyValue))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.keyValue.key").value("booleanField"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.keyValue.value").value(true));
    }
}
