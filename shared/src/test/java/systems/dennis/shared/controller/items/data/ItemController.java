package systems.dennis.shared.controller.items.data;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.items.EditFieldItemController;
import systems.dennis.shared.utils.ApplicationContext;

@RestController
@CrossOrigin
@WebFormsSupport(value = ItemService.class)
@RequestMapping("/edit_test")
public class ItemController extends ApplicationContext
        implements EditFieldItemController<ItemModel, ItemForm> {

    public ItemController(WebContext context) {
        super(context);
    }
}
