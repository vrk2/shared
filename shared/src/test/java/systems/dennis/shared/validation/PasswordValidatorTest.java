package systems.dennis.shared.validation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import systems.dennis.shared.annotations.security.ISecurityUtils;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_form.ValidationResult;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class PasswordValidatorTest {
    private static final String EXPECTED_ERROR_MESSAGE = "incorrect.password";
    private final PasswordValidator passwordValidator = new PasswordValidator();
    @Mock
    private WebContext.LocalWebContext context;
    @Mock
    private ISecurityUtils securityUtilsMock;

    @BeforeEach
    public void setup() {
        Mockito.lenient().when(context.getBean(ISecurityUtils.class)).thenReturn(securityUtilsMock);
    }

    @Test
    @DisplayName("Should return true when password contains at least one uppercase letter digit and special symbols")
    void shouldReturnTrueWhenPasswordContainsAtLeastOneUppercaseLetterDigitAndSpecialCharacter() {
        String password = "1Ab11&J235&kl";
        assertValidationResult(password, true);
    }

    @Test
    @DisplayName("Should return true when password contains leading or trailing spaces")
    void shouldReturnTrueWhenPasswordContainLeadingOrTrailingSpaces() {
        String password = " jsgh15$M8 ";
        assertValidationResult(password, true);
    }

    @Test
    @DisplayName("Should return true when password contains valid special symbols")
    void shouldReturnTrueWhenPasswordContainValidSpecialSymbols() {
        String password = "fH45!@#$%^&*()-_=+{};:,<.>/\\";
        assertValidationResult(password, true);
    }

    @Test
    @DisplayName("Should return Exception when password does not contain special symbol")
    void shouldReturnExceptionWhenPasswordDoesNotContainSpecialSymbol() {
        String password = "Ab11235kl";
        assertValidationResult(password, false);
    }

    @Test
    @DisplayName("Should return Exception when password does not contain uppercase letter")
    void shouldReturnExceptionWhenPasswordDoesNotContainUppercaseLetter() {
        String password = "ngb11235kl";
        assertValidationResult(password, false);
    }

    @Test
    @DisplayName("Should return Exception when password is less than eight characters")
    void shouldReturnExceptionWhenPasswordIsLessThanEightCharacters() {
        String password = "js15$M8";
        assertValidationResult(password, false);
    }

    @Test
    @DisplayName("Should return Exception when password contains cyrillic letters")
    void shouldReturnExceptionWhenPasswordContainCyrillicLetters() {
        String password = "jsghпиa15$M8";
        assertValidationResult(password, false);
    }

    @Test
    @DisplayName("Should return Exception when password contains chinese characters")
    void shouldReturnExceptionWhenPasswordContainChineseCharacters() {
        String password = "jsgh異體字15$M8";
        assertValidationResult(password, false);
    }

    @Test
    @DisplayName("Should return Exception when password contains spaces")
    void shouldReturnExceptionWhenPasswordContainSpaces() {
        String password = "jsgh  15$M8";
        assertValidationResult(password, false);
    }

    private void assertValidationResult(String password, boolean expectedResult) {
        Object object = new Object();
        ValidationResult result = passwordValidator.validate(null, object, null, password, false, context);

        if (!expectedResult) {
            assertThat(result.getErrorMessage()).isEqualTo(EXPECTED_ERROR_MESSAGE);
        }
        assertThat(result.getResult()).isEqualTo(expectedResult);
    }
}
