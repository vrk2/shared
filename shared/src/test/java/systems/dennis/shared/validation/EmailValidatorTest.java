package systems.dennis.shared.validation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import systems.dennis.shared.annotations.security.ISecurityUtils;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_form.ValidationResult;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class EmailValidatorTest {
    private static final String EXPECTED_ERROR_MESSAGE = "incorrect.email";
    private final EmailValidator emailValidator = new EmailValidator();
    @Mock
    private WebContext.LocalWebContext context;
    @Mock
    private ISecurityUtils securityUtilsMock;

    @BeforeEach
    public void setup() {
        Mockito.lenient().when(context.getBean(ISecurityUtils.class)).thenReturn(securityUtilsMock);
    }

    @Test
    @DisplayName("Should return true when valid email starting with digit")
    void shouldReturnTrueWhenValidEmailStartingWithDigit() {
        String email = "123temst@gmail.com";
        assertValidationResult(email, true);
    }

    @Test
    @DisplayName("Should return true when valid email contains dot")
    void shouldReturnTrueWhenValidEmailContainsDot() {
        String email = "my.email.test@gmail.com";
        assertValidationResult(email, true);
    }

    @Test
    @DisplayName("Should return true when valid email contains underscore")
    void shouldReturnTrueWhenValidEmailContainsUnderscore() {
        String email = "luke_skywalker54@test.com";
        assertValidationResult(email, true);
    }

    @Test
    @DisplayName("Should return true when valid email contains dash")
    void shouldReturnPTrueWhenValidEmailContainsDash() {
        String email = "98-email987-test@mail.ru";
        assertValidationResult(email, true);
    }

    @Test
    @DisplayName("Should return true when valid email contains leading or trailing spaces")
    void shouldReturnTrueWhenValidEmailContainsLeadingOrTrailingSpaces() {
        String email = "  hfmk_test@my.email.com  ";
        assertValidationResult(email, true);
    }

    @Test
    @DisplayName("Should return exception when email starting with digit")
    void shouldReturnExceptionWhenEmailStartingWithDigit() {
        String email = ".jlk_test@my.email.com";
        assertValidationResult(email, false);
    }

    @Test
    @DisplayName("Should return exception when email starting with dash")
    void shouldReturnExceptionWhenEmailStartingWithDash() {
        String email = "-jlk_test@my.email.com";
        assertValidationResult(email, false);
    }

    @Test
    @DisplayName("Should return exception when email starting with underscore")
    void shouldReturnExceptionWhenEmailStartingWithUnderscore() {
        String email = "_jlk_test@my.email.com";
        assertValidationResult(email, false);
    }

    @Test
    @DisplayName("Should return exception when email contains special symbol")
    void shouldReturnExceptionWhenEmailContainsSpecialSymbol() {
        String email = "jlk*/,&test@myemail.ru";
        assertValidationResult(email, false);
    }

    @Test
    @DisplayName("Should return exception when email contains consecutive dots")
    void shouldReturnExceptionWhenEmailContainsConsecutiveDots() {
        String email = "789jl..st@gmail.com";
        assertValidationResult(email, false);
    }

    @Test
    @DisplayName("Should return exception when email contains consecutive symbol")
    void shouldReturnExceptionWhenEmailContainsConsecutiveSymbol() {
        String email = "789jl-_st@gmail.com";
        assertValidationResult(email, false);
    }

    @Test
    @DisplayName("Should return exception when email missing top level domain")
    void shouldReturnExceptionWhenEmailMissingTopLevelDomain() {
        String email = "789jlst@gmail";
        assertValidationResult(email, false);
    }

    @Test
    @DisplayName("Should return exception when domain starts with symbol")
    void shouldReturnExceptionWhenDomainStartsWithSymbol() {
        String email = "789jlst@.gmail.com";
        assertValidationResult(email, false);
    }

    private void assertValidationResult(String email, boolean expectedResult) {
        ValidationResult result = emailValidator.validate(null, null, null, email, false, context);
        assertThat(result.getResult()).isEqualTo(expectedResult);

        if (!expectedResult) {
            assertThat(result.getErrorMessage()).isEqualTo(EXPECTED_ERROR_MESSAGE);
        }
    }
}