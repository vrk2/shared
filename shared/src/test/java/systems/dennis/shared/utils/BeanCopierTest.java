package systems.dennis.shared.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import systems.dennis.shared.utils.bean_copier.BeanCopier;
import systems.dennis.shared.utils.bean_copier.DataTransformer;
import systems.dennis.shared.utils.bean_copier.DateAndStringConverter;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class BeanCopierTest {


    @Autowired
    BeanCopier beanCopier;

    @Test
    public void testCopy2Empty() {
        var res = beanCopier.copy(null, BeanCopierTest.b.class);
        assertNull(res.getA());
        assertEquals(res.getB(), 5);
    }

    @Test
    public void testCopy() {
        a item = new a();
        item.setA("test");
        item.setB(43);
        var res = beanCopier.copy(item, BeanCopierTest.b.class);

        assertEquals("test", res.a);
        assertEquals(res.getB(), 43);
        assertNull(res.getC());
    }

    @Test
    public void testSubClass(){
        a item = new a();
        item.setA("test");
        item.setB(43);
        item.setZ("R");

        var res = beanCopier.copy(item, BeanCopierTest.c.class);

        assertEquals("R",  res.z);
        assertEquals("test",  res.getA());
        assertNull(res.getC());
    }


    @Test
    public void testClone() {
        a item = new a();

        a clone = beanCopier.clone(item);

        assertEquals(clone, item);
    }

    @Test
    public void  testTransform (){
        var b = new TransformerB("test", "0001-01-01T06:00:00");

        var res = beanCopier.copy(b, TransformerA.class);

        assertNotNull(res.b);
        assertEquals("test", res.a);
        assertEquals("Sat Jan 01 06:00:00 CET 1", String.valueOf(res.b));

    }

    @Data @AllArgsConstructor
    public static class TransformerA{

        public TransformerA(){}
        private String a;
        private Date b;
    }


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TransformerB {
        private String a;

        @DataTransformer(transFormWith = DateAndStringConverter.class, params = "yyyy-MM-dd'T'hh:mm:ss")
        private String b;
    }


    @Data
    public static
    class a {
        private String a;
        private Integer b;
        private String z;
    }

    @Data
    public static class b {
        private String c;
        private String a;
        private int b = 5;
    }

    @Data
    public static class c extends b {
        private String z;
    }
}
