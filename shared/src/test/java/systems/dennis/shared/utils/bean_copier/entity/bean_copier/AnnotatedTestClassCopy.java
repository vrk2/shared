package systems.dennis.shared.utils.bean_copier.entity.bean_copier;

import lombok.Data;
import org.springframework.stereotype.Component;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.utils.bean_copier.Transient;

@Data
@Component
public class AnnotatedTestClassCopy extends BaseEntity {

    private String stringField;

    private Integer integerField;

    @Transient
    private Long longField;

    public static Double doubleField;

    private Byte byteField;

    @Transient
    private Short shortField;

    public static Float floatField;

    private Character characterField;

}
