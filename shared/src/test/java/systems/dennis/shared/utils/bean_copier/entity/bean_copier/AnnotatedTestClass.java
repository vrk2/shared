package systems.dennis.shared.utils.bean_copier.entity.bean_copier;

import lombok.Data;
import org.springframework.stereotype.Component;
import systems.dennis.shared.utils.bean_copier.DataTransformer;
import systems.dennis.shared.utils.bean_copier.DateAndStringConverter;
import systems.dennis.shared.utils.bean_copier.Transient;

import jakarta.persistence.Column;

@Data
@Component
public class AnnotatedTestClass extends AnnotatedTestClassParent{
    @Column
    private String stringField;

    @Column
    @DataTransformer(transFormWith = DateAndStringConverter.class, params = "yyyy-MM-dd'T'hh:mm:ss")
    private Integer integerField;

    @Column
    @Transient
    private Long longField;

    @Column
    public static double doubleField;

}
