package systems.dennis.shared.utils.bean_copier;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.utils.bean_copier.entity.bean_copier.AnnotatedTestClass;

import java.lang.reflect.Field;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@SpringBootTest
class DateAndStringConverterTest {

    private WebContext.LocalWebContext context;
    private final DateAndStringConverter converter = new DateAndStringConverter();
    private final Date simpleDate = new Date();

    DataTransformer transformer = null;

    {
        Class<AnnotatedTestClass> annotatedClass = AnnotatedTestClass.class;
        for (Field field : annotatedClass.getDeclaredFields()) {
            transformer = field.getAnnotation(DataTransformer.class);
            if (transformer != null) {
                break;
            }
        }
    }

    @Test
    public void testTransformMethodReturnsNullWhenDataTransformerIsNull() {
        log.info("Method testTransformMethodReturnsNullWhenDataTransformerIsNull() started in class {}",
                getClass().getName());
        assertNull(converter.transform(simpleDate, null, String.class, context));
    }

    @Test
    public void testTransformMethodReturnsNullWhenObjectIsWrongFormatString() {
        log.info("Method testTransformMethodReturnsNullWhenObjectIsWrongFormatString() started in class {}",
                getClass().getName());
        String wrongFormatString = "1970|01|01";
        assertNull(converter.transform(wrongFormatString, transformer, Date.class, context));
    }

    @Test
    public void testTransformMethodReturnsSameObjectWhenTypesAreEqual() {
        log.info("Method testTransformMethodReturnsSameObjectWhenTypesAreEqual() started in class {}",
                getClass().getName());
        Object actualDate = converter.transform(simpleDate, transformer, Date.class, context);
        assertTrue(actualDate instanceof Date);
        assertEquals(simpleDate, actualDate);
    }

    @Test
    public void testTransformMethodReturnsDateWhenObjectIsCorrectAndTypeIsString() {
        log.info("Method testTransformMethodReturnsDateWhenObjectIsCorrectAndTypeIsString() started in class {}",
                getClass().getName());
        Object dateString = converter.transform(simpleDate, transformer, String.class, context);
        Object actualDate = converter.transform(dateString, transformer, Date.class, context);
        assertTrue(actualDate instanceof Date);
    }

    @Test
    public void testTransformMethodReturnsStringWhenObjectIsDate() {
        log.info("Method testTransformMethodReturnsStringWhenObjectIsDate() started in class {}",
                getClass().getName());
        Object dateString = converter.transform(simpleDate, transformer, String.class, context);
        assertTrue(dateString instanceof String);
    }

    @Test
    public void testTransformMethodReturnsStringWhenObjectIsDateAndTargetClassAny() {
        log.info("Method testTransformMethodReturnsStringWhenObjectIsDateAndTargetClassAny() started in class {}",
                getClass().getName());
        Class anyClass = Integer.class;
        Object dateString = converter.transform(simpleDate, transformer, anyClass, context);
        Object originalDate = converter.transform(dateString, transformer, Date.class, context);
        assertTrue(dateString instanceof String);
        assertTrue(originalDate instanceof Date);
    }

    @Test
    public void testTransformMethodReturnsEpochStringWhenObjectIsAnyNumber() {
        log.info("Method testTransformMethodReturnsEpochStringWhenObjectIsAnyNumber() started in class {}",
                getClass().getName());
        Integer anyNumber = 3;
        Date epoch = new Date(0);
        Object epochWrapString = converter.transform(anyNumber, transformer, Date.class, context);
        Object epochActual = converter.transform(epochWrapString, transformer, Date.class, context);
        assertTrue(epochActual instanceof Date);
        assertEquals(epochActual, epoch);
    }
}