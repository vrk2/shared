package systems.dennis.shared.utils.bean_copier.entity.bean_copier;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.utils.bean_copier.Transient;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SetFieldValueClass implements DefaultForm {
    private Long id;

    private String value;

    @Transient
    private String trans;
}
