package systems.dennis.shared.utils.bean_copier.entity.bean_copier;

import lombok.Data;
import org.springframework.stereotype.Component;
import systems.dennis.shared.entity.BaseEntity;

import jakarta.persistence.Column;

@Data
@Component
public class AnnotatedTestClassParent extends BaseEntity {
    @Column
    private Character characterField;
}
