package systems.dennis.shared.utils.bean_copier.entity.abstract_transformer;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class AbstractTransformerTestClassChild extends AbstractTransformerTestClass {
    private String string = "AbstractTransformerTestClassChild";
}
