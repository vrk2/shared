package systems.dennis.shared.utils.bean_copier.entity.bean_copier;

import lombok.Data;
import org.springframework.stereotype.Component;
import systems.dennis.shared.utils.bean_copier.Transient;

import jakarta.persistence.Column;

@Data
@Component
public class AnnotatedTestClassChild extends AnnotatedTestClass {

    @Column
    private Byte byteField;

    @Column
    @Transient
    private Short shortField;

    @Column
    public static Float floatField;
}
