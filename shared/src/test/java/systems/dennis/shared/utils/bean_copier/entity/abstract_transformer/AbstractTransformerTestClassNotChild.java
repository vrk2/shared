package systems.dennis.shared.utils.bean_copier.entity.abstract_transformer;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class AbstractTransformerTestClassNotChild {
    private String name = "AbstractTransformerTestClassNotChild";
}
