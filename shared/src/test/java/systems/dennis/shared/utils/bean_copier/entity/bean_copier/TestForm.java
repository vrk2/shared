package systems.dennis.shared.utils.bean_copier.entity.bean_copier;

import lombok.Data;
import systems.dennis.shared.entity.DefaultForm;

@Data
public class TestForm implements DefaultForm {
    private Long id;
    private String name;
    private Boolean aBoolean = true;
}
