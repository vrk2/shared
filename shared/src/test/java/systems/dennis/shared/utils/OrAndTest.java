package systems.dennis.shared.utils;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static systems.dennis.shared.utils.OrAnd.and;
import static systems.dennis.shared.utils.OrAnd.or;


public class OrAndTest {

    public OrAndTest(){}

    @Test
    public void testOr(){
        assertTrue(or(1, 2,1));
        assertTrue(or(1, 2,4,6,1));
    }

    @Test
    public void testAnd(){
        assertFalse(and(1, 2,1));
        assertFalse(and(1, 2,4,6,1));
        assertTrue(and(true, Boolean.TRUE, 1==1 ));
    }

}