package systems.dennis.shared.utils.bean_copier;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import systems.dennis.shared.exceptions.AbstractTransformerException;
import systems.dennis.shared.utils.bean_copier.entity.abstract_transformer.AbstractTransformerTestClass;
import systems.dennis.shared.utils.bean_copier.entity.abstract_transformer.AbstractTransformerTestClassChild;
import systems.dennis.shared.utils.bean_copier.entity.abstract_transformer.AbstractTransformerTestClassNotChild;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@SpringBootTest
class AbstractTransformerTest {

    private final AbstractTransformer transformer = (object, transformerDescription, toType, context) -> null;

    @Autowired
    private AbstractTransformerTestClassChild testClassChild;

    @Test
    public void testReturnSameIfTypesAreEqualMethodReturnsFalseWhenToTypeIsNotAssignableFromObjectFrom() {
        log.info("Method testReturnSameIfTypesAreEqualMethodReturnsFalseWhenToTypeIsNotAssignableFromObjectFrom() " +
                "started in class {}", getClass().getName());
        assertFalse(transformer.returnSameIfTypesAreEqual(testClassChild, AbstractTransformerTestClassNotChild.class));
    }

    @Test
    public void testReturnSameIfTypesAreEqualMethodReturnsTrueWhenToTypeIsAssignableFromObjectFrom() {
        log.info("Method testReturnSameIfTypesAreEqualMethodReturnsTrueWhenToTypeIsAssignableFromObjectFrom() " +
                "started in class {}", getClass().getName());
        assertTrue(transformer.returnSameIfTypesAreEqual(testClassChild, AbstractTransformerTestClassChild.class));
        assertTrue(transformer.returnSameIfTypesAreEqual(testClassChild, AbstractTransformerTestClass.class));
    }

    @Test
    public void testReturnSameIfTypesAreEqualMethodReturnsTrueWhenObjectFromIsNull() {
        log.info("Method testReturnSameIfTypesAreEqualMethodReturnsTrueWhenObjectFromIsNull() started in class {}",
                getClass().getName());
        assertTrue(transformer.returnSameIfTypesAreEqual(null, Class.class));
    }

    @Test
    public void testReturnSameIfTypesAreEqualMethodThrowsExceptionWhenClassToTypeIsNull() {
        log.info("Method testReturnSameIfTypesAreEqualMethodThrowsExceptionWhenClassToTypeIsNull() started in class {}",
                getClass().getName());
        assertThrows(AbstractTransformerException.class, () -> transformer.returnSameIfTypesAreEqual(Class.class, null));
    }

}