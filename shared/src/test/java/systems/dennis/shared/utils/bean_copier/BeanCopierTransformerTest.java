package systems.dennis.shared.utils.bean_copier;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import systems.dennis.shared.beans.LocaleBean;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.utils.bean_copier.entity.bean_copier.AnnotatedTestClass;
import systems.dennis.shared.utils.bean_copier.entity.bean_copier.AnnotatedTestClassChild;
import systems.dennis.shared.utils.bean_copier.entity.bean_copier.AnnotatedTestClassCopy;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@Slf4j
@SpringBootTest
class BeanCopierTransformerTest {
    @Autowired
    private WebContext context;

    private final BeanCopierTransformer transformer = new BeanCopierTransformer();
    private final Date simpleDate = new Date();
    DataTransformer dataTransformer = null;

    private final AnnotatedTestClassChild annotatedChild = new AnnotatedTestClassChild();
    private AnnotatedTestClassCopy annotatedCopy;

    {
        annotatedChild.setStringField("String");
        annotatedChild.setIntegerField(111);
        annotatedChild.setLongField(100L);
        AnnotatedTestClass.doubleField = 2.5;
        annotatedChild.setByteField((byte) 1);
        annotatedChild.setShortField((short) 2);
        AnnotatedTestClassChild.floatField = 1.5F;
        annotatedChild.setCharacterField('&');
    }

    @Test
    public void testTransformMethodReturnsNullWhenDataTransformerIsNull() {
        log.info("Method testTransformMethodReturnsNullWhenContextIsNull() started in class {}",
                getClass().getName());
        assertNull(transformer.transform(simpleDate, dataTransformer, String.class, null));
    }

    @Test
    public void testTransformMethodReturnsCopyWhenFieldIsCorrect() {
        context.getBean(LocaleBean.class);
        var lwc = WebContext.LocalWebContext.of("scope", context);
        log.info("Method testTransformMethodReturnsCopyWhenFieldIsCorrect() started in class {}",
                getClass().getName());
        annotatedCopy = (AnnotatedTestClassCopy) transformer.transform(annotatedChild, dataTransformer,
                AnnotatedTestClassCopy.class, lwc);
        assertEquals(annotatedChild.getByteField(), annotatedCopy.getByteField());
    }
}