package systems.dennis.shared.utils.bean_copier.entity.data_transformer;

import lombok.Data;
import lombok.NoArgsConstructor;
import systems.dennis.shared.utils.bean_copier.DataTransformer;
import systems.dennis.shared.utils.bean_copier.DateAndStringConverter;

import java.util.Date;

import static org.assertj.core.api.recursive.comparison.ComparisonDifference.DEFAULT_TEMPLATE;
import static systems.dennis.shared.utils.bean_copier.DateAndStringConverter.DEFAULT_DATE_TEMPLATE;

@Data
@NoArgsConstructor
public class CopiedObject {
    @DataTransformer(transFormWith = DateAndStringConverter.class, params = DEFAULT_DATE_TEMPLATE)
    private String fromStringToDateType;
    @DataTransformer(transFormWith = DateAndStringConverter.class, params = DEFAULT_DATE_TEMPLATE)
    private Date fromDateToStringType;
}
