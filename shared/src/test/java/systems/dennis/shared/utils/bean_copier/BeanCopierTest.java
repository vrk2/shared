package systems.dennis.shared.utils.bean_copier;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.exceptions.BeanCopierException;
import systems.dennis.shared.utils.bean_copier.entity.bean_copier.*;
import systems.dennis.shared.utils.bean_copier.entity.data_transformer.CopiedObject;
import systems.dennis.shared.utils.bean_copier.entity.data_transformer.TargetObject;

import jakarta.persistence.Column;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static systems.dennis.shared.utils.bean_copier.DateAndStringConverter.DEFAULT_DATE_TEMPLATE;

@Slf4j
@SpringBootTest
class BeanCopierTest {
    @Autowired
    private BeanCopier beanCopier;
    private final AnnotatedTestClassChild annotatedChild = new AnnotatedTestClassChild();
    private AnnotatedTestClassCopy annotatedFieldsObjectCopy = new AnnotatedTestClassCopy();

    {
        annotatedChild.setStringField("String");
        annotatedChild.setIntegerField(111);
        annotatedChild.setLongField(100L);
        AnnotatedTestClass.doubleField = 2.5;
        annotatedChild.setByteField((byte) 1);
        annotatedChild.setShortField((short) 2);
        AnnotatedTestClassChild.floatField = 1.5F;
        annotatedChild.setCharacterField('&');
    }


    @Test
    public void testCopyTransientFieldsMethodReturnsCopyForTransientFieldsTargetClassWhichNotStatic() {
        log.info("Method testCopyTransientFieldsMethodReturnsCopyForTransientFieldsTargetClassWhichNotStatic() " +
                "started in class {}", getClass().getName());
        beanCopier.copyTransientFields(annotatedChild, annotatedFieldsObjectCopy);
        assertEquals(annotatedChild.getShortField(), annotatedFieldsObjectCopy.getShortField());
        assertEquals(annotatedChild.getLongField(), annotatedFieldsObjectCopy.getLongField());
    }

    @Test
    public void testCopyTransientFieldsMethodDontReturnCopyForNotTransientFieldsTargetClass() {
        log.info("Method testCopyTransientFieldsMethodDontReturnCopyForNotTransientFieldsTargetClass() " +
                "started in class {}", getClass().getName());
        beanCopier.copyTransientFields(annotatedChild, annotatedFieldsObjectCopy);
        assertNotEquals(annotatedChild.getIntegerField(), annotatedFieldsObjectCopy.getIntegerField());
        assertNotEquals(annotatedChild.getByteField(), annotatedFieldsObjectCopy.getByteField());
    }


    @Test
    public void testCopyMethodReturnsNullWhenObjectFromIsNull() {
        log.info("Method testCopyMethodReturnsNullWhenObjectFromIsNull() started in class {}",
                getClass().getName());
        assertNull(beanCopier.copy(null, Class.class));
    }

    @Test
    public void testCopyMethodThrowsExceptionWhenTargetClassIsNull() {
        log.info("Method testCopyMethodThrowsExceptionWhenTargetClassIsNull() started in class {}",
                getClass().getName());
        assertThrows(BeanCopierException.class, () -> beanCopier.copy(annotatedChild, null));
    }

    @Test
    public void testCopyMethodReturnsCopyForFieldsTargetClassWhichNotStaticAndWithoutTransient() {
        log.info("Method testCopyMethodReturnsCopyForFieldsTargetClassWhichNotStaticAndWithoutTransient() started " +
                "in class {}", getClass().getName());
        annotatedFieldsObjectCopy = beanCopier.copy(annotatedChild, AnnotatedTestClassCopy.class);
        assertEquals(annotatedChild.getByteField(), annotatedFieldsObjectCopy.getByteField());
    }

    @Test
    public void testCopyMethodReturnsNoCopyForStaticFields() {
        log.info("Method testCopyMethodReturnsNoCopyForStaticFields() started " +
                "in class {}", getClass().getName());
        assertNull(AnnotatedTestClassCopy.doubleField);
        assertNull(AnnotatedTestClassCopy.floatField);
    }

    @Test
    public void testCopyMethodReturnsNoCopyForTransientAnnotatedFields() {
        log.info("Method testCopyMethodReturnsNoCopyForTransientAnnotatedFields() started " +
                "in class {}", getClass().getName());
        annotatedFieldsObjectCopy = beanCopier.copy(annotatedChild, AnnotatedTestClassCopy.class);
        assertNull(annotatedFieldsObjectCopy.getLongField());
        assertNull(annotatedFieldsObjectCopy.getShortField());
    }

    @Test
    public void testCopyMethodReturnsCopyForFieldsTargetClassAndAllItParents() {
        log.info("Method testCopyMethodReturnsNoCopyForTransientAnnotatedFields() started " +
                "in class {}", getClass().getName());
        annotatedFieldsObjectCopy = beanCopier.copy(annotatedChild, AnnotatedTestClassCopy.class);
        assertEquals(annotatedChild.getStringField(), annotatedFieldsObjectCopy.getStringField());
        assertEquals(annotatedChild.getIntegerField(), annotatedFieldsObjectCopy.getIntegerField());
        assertEquals(annotatedChild.getCharacterField(), annotatedFieldsObjectCopy.getCharacterField());
    }

    @Test
    public void testFindFieldMethodReturnsNullWhenNameOfFieldIsNull() {
        log.info("Method testFindFieldMethodReturnsNullWhenNameOfFieldIsNull() started " +
                "in class {}", getClass().getName());
        assertNull(BeanCopier.findField(null, Class.class));
    }

    @Test
    public void testFindFieldMethodReturnsNullWhenSourceClassIsNull() {
        log.info("Method testFindFieldMethodReturnsNullWhenSourceClassIsNull() started " +
                "in class {}", getClass().getName());
        String fieldName = "String";
        assertNull(BeanCopier.findField(fieldName, null));
    }

    @Test
    public void testFindFieldMethodReturnsTargetFieldWhenClassContainsIt() {
        log.info("Method testFindFieldMethodReturnsTargetFieldWhenClassContainsIt() started " +
                "in class {}", getClass().getName());
        String fieldName = AnnotatedTestClass.class.getDeclaredFields()[0].getName();
        var actualFieldName = BeanCopier.findField(fieldName, AnnotatedTestClass.class).getName();
        assertEquals(fieldName, actualFieldName);
    }

    @Test
    public void testFindFieldMethodReturnsTargetFieldWhenParentsClassContainsIt() {
        log.info("Method testFindFieldMethodReturnsTargetFieldWhenParentsClassContainsIt() started " +
                "in class {}", getClass().getName());
        String fieldName = AnnotatedTestClassParent.class.getDeclaredFields()[0].getName();
        var actualFieldName = BeanCopier.findField(fieldName, AnnotatedTestClassChild.class).getName();
        assertEquals(fieldName, actualFieldName);
    }

    @Test
    public void testFindFieldMethodThrowsExceptionWhenDoNotContainIt() {
        log.info("Method testFindFieldMethodThrowsExceptionWhenDoNotContainIt() started " +
                "in class {}", getClass().getName());
        String unknownField = "Unknown";
        assertThrows(BeanCopierException.class, () -> BeanCopier.findField(unknownField, AnnotatedTestClass.class));
    }

    @Test
    public void testWithEachMethodReturnsFieldsListWhenParametersAreCorrect() {
        log.info("Method testWithEachMethodReturnsFieldsListWhenParametersAreCorrect() started " +
                "in class {}", getClass().getName());
        var fieldsList = new ArrayList<String>();
        BeanCopier.withEach(AnnotatedTestClassParent.class, entity -> fieldsList.add(entity.getName()));
        int actualLength = fieldsList.size();
        int expectedLength = AnnotatedTestClassParent.class.getDeclaredFields().length +
                BaseEntity.class.getDeclaredFields().length;
        assertEquals(expectedLength, actualLength);
    }

    @Test
    public void testWithEachMethodReturnsFieldsListWithoutStaticAndTransientParameters() {
        log.info("Method testWithEachMethodReturnsFieldsListWithoutStaticAndTransientParameters() started " +
                "in class {}", getClass().getName());
        var fieldsList = new ArrayList<String>();
        BeanCopier.withEach(AnnotatedTestClassCopy.class, entity -> fieldsList.add(entity.getName()));
        int actualLength = fieldsList.size();
        int numberStaticAndTransientFields = 4;
        int expectedLength = AnnotatedTestClassCopy.class.getDeclaredFields().length - numberStaticAndTransientFields +
                BaseEntity.class.getDeclaredFields().length;
        assertEquals(expectedLength, actualLength);
    }

    @Test
    public void testValuesMethodReturnsEmptyMapNameValueOfFieldsWhenClassIsNull() {
        log.info("Method testValuesMethodReturnsEmptyMapNameValueOfFieldsWhenClassIsNull() started " +
                "in class {}", getClass().getName());
        var nameValueFieldsMap = BeanCopier.values(null, new BaseEntity());
        assertTrue(nameValueFieldsMap.isEmpty());
    }

    @Test
    public void testValuesMethodReturnsMapNameValueOfFieldsWhenClassIsCorrect() {
        log.info("Method testValuesMethodReturnsMapNameValueOfFieldsWhenClassIsCorrect() started " +
                "in class {}", getClass().getName());
        var testForm = new TestForm();
        var nameValueFieldsMap = BeanCopier.values(testForm, new BaseEntity());
        int expectedFieldsNumber = TestForm.class.getDeclaredFields().length;
        int actualFieldsNumber = nameValueFieldsMap.size();
        assertEquals(expectedFieldsNumber, actualFieldsNumber);
    }

    @Test
    public void testFindAnnotatedFieldsMethodReturnsEmptyListWhenClassIsNull() {
        log.info("Method testFindAnnotatedFieldsMethodReturnsEmptyListWhenClassIsNull() started " +
                "in class {}", getClass().getName());
        var annotatedFieldsList = BeanCopier.findAnnotatedFields(null, Column.class);
        assertTrue(annotatedFieldsList.isEmpty());
    }

    @Test
    public void testFindAnnotatedFieldsMethodReturnsEmptyListWhenAnnotationIsNull() {
        log.info("Method testFindAnnotatedFieldsMethodReturnsEmptyListWhenAnnotationIsNull() started " +
                "in class {}", getClass().getName());
        var annotatedFieldsList = BeanCopier.findAnnotatedFields(Class.class, null);
        assertTrue(annotatedFieldsList.isEmpty());
    }

    @Test
    public void testFindAnnotatedFieldsMethodReturnsFieldsListWhenClassAndAnnotationAreCorrect() {
        log.info("Method testFindAnnotatedFieldsMethodReturnsFieldsListWhenClassAndAnnotationAreCorrect() started " +
                "in class {}", getClass().getName());
        var fieldsList = BeanCopier.findAnnotatedFields(AnnotatedTestClassParent.class, Column.class);
        int expectedFieldsNumber = AnnotatedTestClassParent.class.getDeclaredFields().length;
        assertEquals(expectedFieldsNumber, fieldsList.size());
    }

    @Test
    public void testFindAnnotatedFieldsMethodReturnsEmptyListWhenThereIsNoFieldsWithThisAnnotation() {
        log.info("Method testFindAnnotatedFieldsMethodReturnsEmptyListWhenThereIsNoFieldsWithThisAnnotation() started " +
                "in class {}", getClass().getName());
        var fieldsList = BeanCopier.findAnnotatedFields(AnnotatedTestClassParent.class, DataTransformer.class);
        int realFieldsNumber = AnnotatedTestClassParent.class.getDeclaredFields().length;
        assertNotEquals(realFieldsNumber, fieldsList.size());
    }




    @Test
    public void testSetFieldValueMethodSetNewValueWhenFieldWasFoundAndNotTransient() {
        log.info("Method testSetFieldValueMethodSetNewValueWhenFieldWasFoundAndNotTransient() started " +
                "in class {}", getClass().getName());
        var obj = new SetFieldValueClass(1L, "Old", "Default");
        String newValue = "New";
        BeanCopier.setFieldValue(obj, BeanCopier.findField("value", obj.getClass()), newValue);
        BeanCopier.setFieldValue(obj, BeanCopier.findField("trans", obj.getClass()), newValue);
        assertEquals(newValue, obj.getValue());
        assertNotEquals(newValue, obj.getTrans());
    }

    @Test
    public void testReadValueMethodGetValueWhenFieldWasFound() throws InvocationTargetException, IllegalAccessException {
        log.info("Method testReadValueMethodGetValueWhenFieldWasFound() started " +
                "in class {}", getClass().getName());
        var obj = new SetFieldValueClass(1L, "Old", "Default");

        var fromField = BeanCopier.readValue(obj, BeanCopier.findField("value", obj.getClass()));
        var fromString = BeanCopier.readValue(obj, "value");
        assertEquals(fromField, obj.getValue());
        assertEquals(fromString, obj.getValue());
    }

    @Test
    public void testSetValueFromNotNullToNullIsCorrect() throws InvocationTargetException, IllegalAccessException {
        log.info("Method testReadValueMethodGetValueWhenFieldWasFound() started " +
                "in class {}", getClass().getName());
        var obj = new SetFieldValueClass(1L, "Old", "Default");


        BeanCopier.setFieldValue(obj, BeanCopier.findField("value", SetFieldValueClass.class), null);
        assertNull(obj.getValue());
        BeanCopier.setFieldValue(obj, BeanCopier.findField("value", SetFieldValueClass.class), "50");
        assertEquals("50", obj.getValue());
    }

    private final CopiedObject copiedObject = new CopiedObject();
    private final SimpleDateFormat formatForDateNow = new SimpleDateFormat(DEFAULT_DATE_TEMPLATE);

    {
        copiedObject.setFromStringToDateType("09.08.2020");
        copiedObject.setFromDateToStringType(new Date());
    }

    @Test
    public void testCopyMethodTransformStringToDateWhenCopiedFieldHasDataTransformerAnnotation() {
        log.info("Method testCopyMethodTransformStringToDateWhenCopiedFieldHasDataTransformerAnnotation() started " +
                "in class {}", getClass().getName());
        var targetObject = beanCopier.copy(copiedObject, TargetObject.class);
        String actualDate = formatForDateNow.format(targetObject.getFromStringToDateType());
        String expectedDate = copiedObject.getFromStringToDateType();
        assertNotNull(targetObject.getFromStringToDateType());
        assertEquals(expectedDate, actualDate);
    }

    @Test
    public void testCopyMethodTransformDateToStringWhenCopiedFieldHasDataTransformerAnnotation() {
        log.info("Method testCopyMethodTransformStringToDateWhenCopiedFieldHasDataTransformerAnnotation() started " +
                "in class {}", getClass().getName());
        var targetObject = beanCopier.copy(copiedObject, TargetObject.class);
        String actualDate = targetObject.getFromDateToStringType();
        String expectedDate = formatForDateNow.format(copiedObject.getFromDateToStringType());
        assertNotNull(targetObject.getFromDateToStringType());
        assertEquals(expectedDate, actualDate);
    }

}