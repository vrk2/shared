package systems.dennis.shared.pojo_form;

import systems.dennis.shared.config.WebContext;

import java.util.ArrayList;

public interface DataProvider<LABEL_TYPE> {
    ArrayList<ItemValue<LABEL_TYPE>> getItems(WebContext.LocalWebContext context);
}
