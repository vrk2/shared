package systems.dennis.shared.pojo_form;

import lombok.Data;
import org.apache.logging.log4j.util.Strings;
import systems.dennis.shared.entity.KeyValue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class ValidationContext implements Serializable {
    private boolean containsErrors;
    private Map<String, List<ValidationResult>> data = new HashMap<>();

    public List<String> getValidationMessages(){
        List<String> res = new ArrayList<>();
        for (List<ValidationResult> key: data.values()){
           for (ValidationResult result : key){
               if (!result.getResult() && !Strings.isBlank(result.getErrorMessage())){
                   res.add(result.getErrorMessage());
               }
           }
        }
        return res;
    }

    public List<KeyValue> getFieldErrors(){
        List<KeyValue> res = new ArrayList<>();
        for (var key: data.keySet()){
            for (ValidationResult result : data.get(key)){
                if (!result.getResult() && !Strings.isBlank(result.getErrorMessage())){
                    res.add(new KeyValue(key, result.getErrorMessage()));
                }
            }
        }
        return res;
    }

    public boolean hasErrors(String field){
        List<ValidationResult> results =  data.get(field);
        if (results == null) return  true;
        for (ValidationResult result : results){
            if (!result.getResult()){
                return true;
            }
        }
        return false;
    }

    public boolean isContainsErrors() {
        return containsErrors;
    }

    public void setContainsErrors(boolean b) {
        this.containsErrors = b;
    }

    public void setData(Map<String, List<ValidationResult>> data) {
        this.data = data;
    }
}
