package systems.dennis.shared.pojo_form;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_view.DefaultDataConverter;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeZoneDateConverter implements DefaultDataConverter<Date, String> {
    @Override
    public Date convert(Date date, String inuse, WebContext.LocalWebContext context){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.setTimeZone(TimeZone.getTimeZone("CEST"));
        return cal.getTime();
    }
}
