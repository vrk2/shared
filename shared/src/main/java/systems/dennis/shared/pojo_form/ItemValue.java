package systems.dennis.shared.pojo_form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemValue<T> implements Serializable {
    private T label;
    private Long value;

    public static <E> ItemValue <E> of(E label, Long id){
        return new ItemValue<>(label, id);
    }
}
