package systems.dennis.shared.pojo_form;

import systems.dennis.shared.pojo_view.DEFAULT_TYPES;
import systems.dennis.shared.pojo_view.DefaultDataConverter;
import systems.dennis.shared.pojo_view.list.Remote;
import systems.dennis.shared.validation.ValueValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
public @interface PojoFormElement {


    boolean visible() default true;

    int order() default 0;

    boolean ID() default false;

    boolean autocomplete() default true;

    Class<? extends DefaultDataConverter> dataConverter() default FromElementValueConverter.class;

    String format() default "";

    Checkable checked() default @Checkable(isCheckElement = false);

    String type() default DEFAULT_TYPES.DEFAULT_TYPE;


    String placeHolder() default DEFAULT_TYPES.DEFAULT_EMPTY_VALUE;

    boolean required() default false;

    @Deprecated (since = "2.2. Use @Validator instead")
    Class<? extends ValueValidator>[] validators() default {};

    Class<? extends DataProvider> dataProvider() default DataProvider.class;

    String defaultValue() default "";

    boolean showLabel() default true;

    boolean showPlaceHolder() default false;

    String fieldNote() default  "";

    Remote remote() default @Remote( );

    String group() default "";


    boolean available() default true;
}
