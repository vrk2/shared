package systems.dennis.shared.form;

import lombok.Data;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.pojo_form.PojoFormElement;
import systems.dennis.shared.pojo_view.UIAction;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.pojo_view.list.PojoListViewField;

import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data
@PojoListView(actions = {})
public class FavoriteItemForm implements DefaultForm {

    @PojoFormElement(type = HIDDEN)
    private Long id;

    private Long modelId;

    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private String type;

    @PojoFormElement (available = false)
    @PojoListViewField( available = false)
    private Long userDataId;

    @PojoListViewField(actions = {@UIAction(component = "delete")})
    @PojoFormElement(available = false)
    private Integer action;
}
