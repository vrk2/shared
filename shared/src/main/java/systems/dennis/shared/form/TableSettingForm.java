package systems.dennis.shared.form;

import lombok.Data;
import systems.dennis.shared.annotations.Validation;
import systems.dennis.shared.controller.items.magic.MagicQuery;
import systems.dennis.shared.controller.items.magic.MagicRequest;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.pojo_form.PojoFormElement;
import systems.dennis.shared.pojo_view.UIAction;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.pojo_view.list.PojoListViewField;
import systems.dennis.shared.validation.FieldIsUniqueValidator;
import systems.dennis.shared.validation.ValueNotEmptyValidator;

import java.util.List;

import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data
@PojoListView(actions = {})
public class TableSettingForm implements DefaultForm {

    @PojoFormElement(type = HIDDEN)
    private Long id;

    @Validation(ValueNotEmptyValidator.class)
    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private String name;


    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private String topic;

    @PojoFormElement(required = true)
    private List<String> fieldsOrder;

    private MagicRequest magicQuery;

    @PojoFormElement
    @PojoListViewField (searchable = true)
    private String url;


    private Boolean addToMenu;


    @PojoFormElement (available = false)
    @PojoListViewField( available = false)
    private Long currentUser;

    @PojoListViewField(actions = {@UIAction(component = "delete")})
    @PojoFormElement(available = false)
    private Integer action;

    @Override
    public String asValue() {
        return name;
    }
}
