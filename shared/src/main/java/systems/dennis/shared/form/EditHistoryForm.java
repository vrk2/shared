package systems.dennis.shared.form;

import lombok.Data;
import systems.dennis.shared.entity.DefaultForm;

import java.util.Date;

@Data
public class EditHistoryForm implements DefaultForm {

    private Long id;

    private Long idEditedObject;

    private Date editedDate;

    private String classEditedObject;

    private String oldContent;

    private String newContent;
}
