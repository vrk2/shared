package systems.dennis.shared.form;

import lombok.Data;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.pojo_form.PojoFormElement;
import systems.dennis.shared.pojo_view.UIAction;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.pojo_view.list.PojoListViewField;

import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data
@PojoListView(actions = {})
public class AppSettingsForm implements DefaultForm {
    @PojoFormElement(type = HIDDEN)
    private Long id;

    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private String key;

    @PojoFormElement(required = true)
    @PojoListViewField(searchable = true)
    private String value;

    @PojoListViewField(actions = {@UIAction(component = "edit"), @UIAction(component = "delete")}, sortable = false)
    @PojoFormElement(available = false)
    private Integer action;
}
