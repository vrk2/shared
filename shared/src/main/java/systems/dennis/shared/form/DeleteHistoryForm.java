package systems.dennis.shared.form;

import lombok.Data;
import systems.dennis.shared.entity.DefaultForm;

import java.util.Date;

@Data
public class DeleteHistoryForm implements DefaultForm {
    private Long id;

    private Long idDeletedObject;

    private Date deletedDate;

    private String classDeletedObject;

    private String content;
}
