package systems.dennis.shared.db;

import systems.dennis.shared.entity.db.DbInjection;
import lombok.Data;

import java.util.List;

@Data
public class DbUpdaterModel {
    List<DbInjection> dbInjectionList;
}