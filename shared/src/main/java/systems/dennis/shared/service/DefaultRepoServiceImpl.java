package systems.dennis.shared.service;

import lombok.SneakyThrows;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.ItemNotUserException;
import systems.dennis.shared.exceptions.UnmodifiedItemSaveAttemptException;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;

/**
 * Default implementation for the AbstractService
 *
 * @param <DB_TYPE> extends @{@link DefaultForm}
 */
@Slf4j
public abstract class DefaultRepoServiceImpl<DB_TYPE extends BaseEntity> implements AbstractService<DB_TYPE> {



    /**
     *
     * Returns all records from db
     * @deprecated since version 1.0.1 as need to be used with the limit
     * Default method to get all values should be used with care or better not to be used!
     *
     */
    @Override
    @Deprecated
    public List<DB_TYPE> find() {
        return find(null ,null, 0).getContent();
    }



    @SneakyThrows
    @Override
    public DB_TYPE save(DB_TYPE object)  {
        if (object == null) {
            throw new IllegalArgumentException("Can not save null Object");
        }
        object = preAdd(object);
        return afterAdd(getRepository().save(object));
    }


    @Override
    public DB_TYPE edit(DB_TYPE object) throws ItemNotFoundException, UnmodifiedItemSaveAttemptException {
        if (object == null) {
            throw new IllegalArgumentException("Can not save null Object");
        }
        DB_TYPE original = getRepository().findById(object.getId()).orElseThrow(() -> new ItemNotFoundException(object.getId()));
        var res =  getRepository().save(preEdit(object, original));
        afterEdit(object, original);
        return res;
    }

    public void afterEdit(DB_TYPE object, DB_TYPE original){

    }
    @Override
    public void delete(Long id) {
        getRepository().deleteById(id);
    }

    @Override
    public void deleteItems(List<Long> ids) throws ItemNotUserException, ItemNotFoundException {
        for (Long id : ids) {
            getRepository().deleteById(id);
        }
    }

    @Override
    public boolean exists(DB_TYPE object) {

        if (object != null) {
            return getRepository().existsById(object.getId());
        }
        throw new IllegalArgumentException("Entity should be be not null instance");
    }

    @Override
    public Optional<DB_TYPE> findById(Long id) {
        return getRepository().findById(id);
    }
}
