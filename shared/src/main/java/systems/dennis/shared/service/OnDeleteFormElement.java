package systems.dennis.shared.service;

public interface OnDeleteFormElement {
    public String onDelete(boolean ok, String id, Exception e);
}
