package systems.dennis.shared.service;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.annotations.security.ISecurityUtils;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.forms.QueryObject;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.KeyValue;
import systems.dennis.shared.exceptions.ItemDoesNotContainsIdValueException;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.ItemNotUserException;
import systems.dennis.shared.exceptions.UnmodifiedItemSaveAttemptException;
import systems.dennis.shared.repository.PaginationRepository;
import systems.dennis.shared.repository.QueryCase;
import systems.dennis.shared.utils.ApplicationContext;
import systems.dennis.shared.utils.bean_copier.BeanCopier;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Slf4j
public abstract class PaginationService<DB_TYPE extends BaseEntity>
        extends ApplicationContext
        implements AbstractService<DB_TYPE> {

    public PaginationService(WebContext holder) {
        super(holder);
    }


    @Override
    public Logger getLogger() {
        return log;
    }

    @Override
    public boolean exists(DB_TYPE object) {
        return getRepository().existsById(object.getId());
    }

    @SneakyThrows
    @Override
    public DB_TYPE save(DB_TYPE model) {

        if (model.getId() != null && model.getId() < 1) {
            model.setId(null);
        }

        if (model.getId() != null) {
            return edit(getBean(BeanCopier.class).clone(model));
        }

        preAdd(model);
        assignUser(model);

        var res = getRepository().save(model);

        afterAdd(res);

        return res;
    }

    public void preFetchOriginal(DB_TYPE form) {

    }

    public Page<DB_TYPE> getPageData(int currentPage, int size, QueryObject<String> parameters, Sort sort, Class<? extends DB_TYPE> pojo) {
        if (sort == null) {
            return getRepository().findAll(getContext().getRequestSpecification(pojo, getAdditionalCases(parameters)), PageRequest.of(currentPage - 1, size));
        }
        return getRepository().findAll(getContext().getRequestSpecification(pojo, getAdditionalCases(parameters)), PageRequest.of(currentPage - 1, size, sort));
    }

    public List<QueryCase> getAdditionalCases(QueryObject<String> parameters) {
        return Collections.emptyList();
    }


    @Override
    public List<DB_TYPE> find() {
        List<DB_TYPE> result = new ArrayList<>();
        getRepository().findAll().forEach(result::add);
        return result;
    }

    @Override
    public DB_TYPE edit(DB_TYPE model) throws ItemNotUserException, ItemNotFoundException, UnmodifiedItemSaveAttemptException, ItemDoesNotContainsIdValueException {
        Long editedId = model.getId();
        var cloneOfOriginal = findByIdClone(editedId).orElseThrow(() -> new ItemNotFoundException(editedId));
        //Here need to be cloned to avoid Hibernate to take original object from cache

        if (Objects.equals(model, cloneOfOriginal)) {
            throw new UnmodifiedItemSaveAttemptException("Object not changed");
        }

        var copier = getContext().getBean(BeanCopier.class);
        copier.copyTransientFields(model, cloneOfOriginal);

        assignUser(model, cloneOfOriginal);

        model = preEdit(model, cloneOfOriginal); // change to retrieve data

        var res = getRepository().save(model);
        saveVersionIfRequired(cloneOfOriginal, model);

        afterEdit(res, cloneOfOriginal); // afterEdit

        return res;
    }

    public void afterEdit(DB_TYPE object, DB_TYPE original) {

    }


    @Override
    public KeyValue editField(Long id, KeyValue keyValue)
            throws ItemNotUserException, ItemNotFoundException, UnmodifiedItemSaveAttemptException, ItemDoesNotContainsIdValueException, IllegalAccessException, InvocationTargetException {

        DB_TYPE original = getRepository().findById(id).orElseThrow(() -> new ItemNotFoundException(id));
        var cloneOfOriginal = getContext().getBean(BeanCopier.class).clone(original);
        var field = BeanCopier.findField(keyValue.getKey(), cloneOfOriginal.getClass());

        BeanCopier.setFieldValue(cloneOfOriginal, field, keyValue.getValue());
        cloneOfOriginal = edit(cloneOfOriginal);

        var valueFromModel = BeanCopier.readValue(cloneOfOriginal, keyValue.getKey());

        return new KeyValue(keyValue.getKey(), valueFromModel);
    }


    @Override
    public void deleteItems(List<Long> ids) throws ItemNotUserException, ItemNotFoundException {
        ids.forEach(this::delete);
    }

    @Override
    public <F extends PaginationRepository<DB_TYPE>> F getRepository() {
        return (F) getContext().getBean(getClass().getAnnotation(DataRetrieverDescription.class).repo());
    }


    @SuppressWarnings("Unchecked")
    public <T extends ISecurityUtils> T getUtils() {
        return (T) getContext().getBean(ISecurityUtils.class);
    }
}
