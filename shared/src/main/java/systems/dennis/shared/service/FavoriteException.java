package systems.dennis.shared.service;

import org.springframework.http.HttpStatus;
import systems.dennis.shared.exceptions.StatusException;

public class FavoriteException extends StatusException {
    public FavoriteException(String s) {
        super(s, HttpStatus.BAD_REQUEST);
    }
}
