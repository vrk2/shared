package systems.dennis.shared.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.form.AppSettingsForm;
import systems.dennis.shared.model.AppSettingsModel;
import systems.dennis.shared.repository.AppSettingsRepo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Service
@DataRetrieverDescription(model = AppSettingsModel.class, form = AppSettingsForm.class, repo = AppSettingsRepo.class)
public class AppSettingsService extends PaginationService<AppSettingsModel> {
    private final Logger log = LoggerFactory.getLogger(AppSettingsService.class);

    @Value("${global.api.config.enabled}")
    private Boolean isEnabled;

    public AppSettingsService(WebContext holder) {
        super(holder);
    }

    public void readSettingsAndSave(String resourcePath) {
        if (isEnabled && isDataEmpty()) {
            Properties properties = new Properties();
            try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(resourcePath)) {
                properties.load(inputStream);

                for (String key : properties.stringPropertyNames()) {
                    String value = properties.getProperty(key);

                    AppSettingsModel setting = new AppSettingsModel();
                    setting.setKey(key);
                    setting.setValue(value);
                    getRepository().save(setting);
                }
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
    }

    private boolean isDataEmpty() {
        return getRepository().count() == 0;
    }
}
