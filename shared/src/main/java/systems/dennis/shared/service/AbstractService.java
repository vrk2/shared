package systems.dennis.shared.service;

import lombok.SneakyThrows;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.annotations.security.ISecurityUtils;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.items.magic.MagicRequest;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.entity.KeyValue;
import systems.dennis.shared.exceptions.*;
import systems.dennis.shared.model.DeleteHistoryModel;
import systems.dennis.shared.model.EditHistoryModel;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.repository.DefaultSearchSpecification;
import systems.dennis.shared.repository.PaginationRepository;
import systems.dennis.shared.repository.QueryCase;
import systems.dennis.shared.utils.ErrorSupplier;
import systems.dennis.shared.utils.Supplier;
import systems.dennis.shared.utils.bean_copier.BeanCopier;
import systems.dennis.shared.utils.bean_copier.ObjectDefinition;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import static systems.dennis.shared.controller.forms.Serviceable.findDeclaredClass;
import static systems.dennis.shared.controller.forms.Serviceable.log;
import static systems.dennis.shared.utils.bean_copier.BeanCopier.findField;

/**
 * Implementation should implement methods which are necessary for web service to work properly
 * In this implementation all methods, like add, edit, delete or list are in the same interface. Form more complex functionality, it can make sence to move all this logical functions to the separate service
 *
 * @param <DB_TYPE> A class of the entity to be saved. Should be persistant entity, in the best an implementation of {@link DefaultForm}
 */
public interface AbstractService<DB_TYPE extends BaseEntity> {
    /**
     * A simple fetch all implementation
     *
     * @return list of all records of T
     */
    List<DB_TYPE> find();

    /**
     * Updates an object in DB
     *
     * @param object - object to be updated, with ID
     * @return Updated T object
     * @throws ItemNotUserException                Exceptions on thrown when trying to update and object by user, not owner of the object
     * @throws ItemNotFoundException               When object cannot be found by id of the #object, this exception is thrown
     * @throws UnmodifiedItemSaveAttemptException  Exception says, that there are no changes in the object which should be updated
     * @throws ItemDoesNotContainsIdValueException Exception is throw when Objects has null ID
     */
    DB_TYPE edit(DB_TYPE object) throws ItemNotUserException, ItemNotFoundException, UnmodifiedItemSaveAttemptException, ItemDoesNotContainsIdValueException;

    KeyValue editField(Long id, KeyValue keyValue) throws ItemNotUserException, ItemNotFoundException, UnmodifiedItemSaveAttemptException, ItemDoesNotContainsIdValueException, IllegalAccessException, InvocationTargetException;

    /**
     * Removes object from DB
     *
     * @param id - id of the object to be removed
     * @throws ItemNotUserException  Exceptions is thrown on trying to update and object by user, not owner of the object
     * @throws ItemNotFoundException When object cannot be found by id of the #object, this exception is thrown
     */
    default void delete(Long id) throws ItemNotUserException, ItemNotFoundException {
        var object = findById(id).orElseThrow(() -> new ItemNotFoundException(id));
        preDelete(object);
        getRepository().delete(object);
        saveDeleteHistoryIfRequired(object);
        afterDelete(object);
    }

    /**
     * Removes objects from DB
     *
     * @param ids - ids of objects to be removed
     * @throws ItemNotUserException  Exception is thrown when trying to update an object from a user, not owner of the object
     * @throws ItemNotFoundException When object cannot be found by id of the #object, this exception is thrown
     */
    void deleteItems(List<Long> ids) throws ItemNotUserException, ItemNotFoundException;

    /**
     * Before object is saved in DB this method is called
     *
     * @param object   - an object variant to be updated
     * @param original - an original object from DB
     * @return - a final object to be updated
     * @throws ItemNotFoundException              When object cannot be found by id of the #object, this exception is thrown
     * @throws UnmodifiedItemSaveAttemptException Exception says, that there are no changes in the object which should be updated
     */
    default DB_TYPE preEdit(DB_TYPE object, DB_TYPE original) throws UnmodifiedItemSaveAttemptException, ItemNotFoundException {
        return object;
    }

    default void assignUser(DB_TYPE pojo) {
        try {
            getContext().getBean(ISecurityUtils.class).assignUser(pojo);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    default void assignUser(DB_TYPE pojo, DB_TYPE original) {
        getContext().getBean(ISecurityUtils.class).assignUser(pojo, original);
    }

    default void saveVersionIfRequired(DB_TYPE original, DB_TYPE object) {

        try {
            Boolean isRequired = getContext().getEnv("global.save.versions", false, Boolean::valueOf);
            if (isRequired) {

                var editableObject = EditHistoryModel.from(original, object);
                getContext().getBean(EditHistoryService.class).save(editableObject);
            }
        } catch (Exception e) {
            getLogger().error("Cannot save original version for object", e);
        }
    }

    /**
     * Before object is added this method is called
     *
     * @param object to be saved
     * @return a modified object before saving
     * @throws ItemForAddContainsIdException No id should be in edit object
     */
    default DB_TYPE preAdd(DB_TYPE object) throws ItemForAddContainsIdException {
        return object;
    }

    WebContext.LocalWebContext getContext();


    @SneakyThrows
    default Page<DB_TYPE> search(String field, String value, int page, Integer size, Long[] additionalIds) {
        var model = getModel().getConstructor().newInstance();
        return getRepository().findAll(getSearchRequestSpecification(field, value, additionalIds), PageRequest.of(page, size, model.defaultSearchOrderField().getValue(), model.defaultSearchOrderField().getKey()));
    }

    default Specification<DB_TYPE> getSearchRequestSpecification(String field, String value, Long[] additionalIds){
        Specification<DB_TYPE> spec =  QueryCase.containsInSensitive(field, value).specification();
        Specification<DB_TYPE> additionalSpecification = getAdditionalSpecification();
        if (additionalSpecification != null) {
            spec = spec.and(additionalSpecification);
        }

        return spec;
    }

    /**
     * What to do with object after it is stored in DB
     *
     * @param object Saved object with ID
     * @return modified object
     */
    default DB_TYPE afterAdd(DB_TYPE object) {
        return object;
    }

    /**
     * What to do with object before it is deleted from DB
     *
     * @param object deleted object with ID
     * @return deleted object
     */
    default DB_TYPE preDelete(DB_TYPE object) {
        return object;
    }

    /**
     * What to do with object after it is deleted from DB
     *
     * @param object deleted object with ID
     * @return deleted object
     */
    default DB_TYPE afterDelete(DB_TYPE object) {
        return object;
    }

    /**
     * Save object after it was deleted from DB
     * if "global.save.deleted=true"
     *
     * @param deletedObject deleted object with ID
     */
    default void saveDeleteHistoryIfRequired(DB_TYPE deletedObject) {
        try {
            Boolean isRequired = getContext().getEnv("global.save.deleted", false, Boolean::valueOf);
            if (isRequired) {
                var deleteHistoryModel = DeleteHistoryModel.from(deletedObject);
                getContext().getBean(DeleteHistoryService.class).save(deleteHistoryModel);
            }
        } catch (Exception e) {
            getLogger().error("Cannot save original version for object", e);
        }
    }

    Logger getLogger();

    /**
     * Verifies whether object exist or not ( normally, by Id)
     *
     * @param object Object to check
     * @return true if object exists, false otherwise
     */
    boolean exists(DB_TYPE object);

    DB_TYPE save(DB_TYPE form);

    @SneakyThrows
    default <F extends DB_TYPE> F findByIdOrThrow(Long id) {
        DB_TYPE res = findById(id).orElseThrow(() -> new ItemNotFoundException(id));
        return (F) res;
    }

    default <T> T findById(Long id, Supplier<T> orElse) {
        var res = findById(id);
        if (res.isEmpty()) {
            return orElse.onNull();
        }
        return (T) res.get();
    }

    default <T extends DB_TYPE> DefaultSearchSpecification<T> getSelfCreatedItems(Long currentUser){
        return getSelfCreatedItemsQuery(currentUser).specification();
    }


    default QueryCase  getSelfCreatedItemsQuery(Long currentUser){
        return QueryCase.equalsOf("userDataId", currentUser);
    }



    default Optional<DB_TYPE> findByIdClone(Long id) {
        var el = findById(id);

        if (el.isPresent()) {
            return Optional.of(getContext().getBean(BeanCopier.class).clone(el.get()));
        } else {
            return el;
        }

    }

    default Optional<DB_TYPE> findById(Long id) {
        if (id == null || id < 0) {
            throw new IllegalIdException(id);
        }
        Specification<DB_TYPE> specification = getAdditionalSpecification();

        Optional<DB_TYPE> element;
        if (specification != null) {
            element = getRepository().findOne(specification.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.equal(root.get("id"), id)));
        } else {
            element = getRepository().findOne(QueryCase.equalsOf("id", id).integer().specification());
        }

        var deleteHistoryService = getContext().getBean(DeleteHistoryService.class);
        if (element.isEmpty()) {
            var deletedObjectExists = deleteHistoryService.existsByTypeAndDeletedId(id,
                    this.getClass().getAnnotation(DataRetrieverDescription.class).model().getSimpleName());
            if (deletedObjectExists) {
                throw new ItemWasDeletedException(id, getModel());
            }
        }
        return element;
    }

    default <T> T findById(Long id, Exception els) {
        return findById(id, new ErrorSupplier<>(els));
    }

    default <T extends BaseEntity> Class<T> getModel() {
        var res = findDeclaredClass(getClass(), getClass().getAnnotation(DataRetrieverDescription.class));

        return (Class<T>) res.model();
    }

    default <T extends DefaultForm> Class<T> getForm() {
        var res = findDeclaredClass(getClass(), getClass().getAnnotation(DataRetrieverDescription.class));

        return (Class<T>) res.form();
    }

    /**
     * Returns all records from db having id more than from (or ignoring if null) and limited of limit
     * Default method to get all values should be used with care or better not to be used !
     *
     * @param from  - id gt from
     * @param limit - max
     * @param page  - a page to select
     * @return List of res
     */
    default Page<DB_TYPE> find(Long from, Integer limit, Integer page) {
        return find(null, from, limit, page);
    }

    default Page<DB_TYPE> find(DefaultSearchSpecification<DB_TYPE> searchSpecification, Long from, Integer limit, Integer page) {
        return find((Specification<DB_TYPE>) searchSpecification, from, limit, page);
    }

    default Page<DB_TYPE> find(Specification<DB_TYPE> searchSpecification, Long from, Integer limit, Integer page) {

        PageRequest request = null;


        if (limit == null || limit == -1){
            limit = 200;
        }

        if (limit != null) {
            request = PageRequest.of(Objects.requireNonNullElse(page, 0), limit);
        }


        if (from != null) {
            if (searchSpecification == null) {
                searchSpecification = QueryCase.moreOrEquals(DefaultForm.ID_FIELD, from).specification();
            }
        }

        if (searchSpecification != null) {
            if (request != null) {
                return getRepository().findAll(searchSpecification, request);
            } else {
                return getRepository().findAll(searchSpecification, Pageable.unpaged());
            }
        } else {
            if (request != null) {
                return getRepository().findAll(request);
            } else {
                return getRepository().findAll(Pageable.unpaged());
            }
        }
    }

    <F extends PaginationRepository<DB_TYPE>> F getRepository();

    default Page<DB_TYPE> search(MagicRequest request) {
        Page<DB_TYPE> data;
        if (request.getQuery().isEmpty() && request.getCases().isEmpty() && getAdditionalSpecification() == null) {
            data = getRepository().findAll(PageRequest.of(request.getPage(), request.getLimit(), createFromRequest(request)));

            return data;
        } else {
            //todo make a proper search!

            DefaultSearchSpecification<DB_TYPE> specification = QueryCase.ofNotNull("id").specification();

            Specification<DB_TYPE> finalSpecification = specification;
            for (var query : request.getQuery()) {
                QueryCase modifyQuery = modifyQuery(query.toQuery(getContext()));
                if (Objects.nonNull(modifyQuery)) {
                    specification.addCase(modifyQuery);
                } else {
                    specification.addCase(query.toQuery(getContext()));
                }
            }
            for (var query : request.getCases()) {
                QueryCase modifyQuery = modifyQuery(query);
                if (Objects.nonNull(modifyQuery)) {
                    specification.addCase(modifyQuery);
                } else {
                    specification.addCase(query);
                }
            }

            Specification<DB_TYPE> spec = getAdditionalSpecification();
            if (spec!= null){
                finalSpecification =  specification.and(spec);
            }

            data = getRepository().findAll(finalSpecification,
                    PageRequest.of(request.getPage(), request.getLimit(), createFromRequest(request)));
            return data;
        }

    }

    default Sort createFromRequest(MagicRequest request) {
        if (request.getSort() == null || request.getSort().isEmpty()) {
            return Sort.unsorted();
        }

        List<Sort> sorts = new ArrayList<>();


        request.getSort().stream().filter(Objects::nonNull).forEach(x -> sorts.add(
                Sort.by(x.getDesc() ? Sort.Direction.DESC : Sort.Direction.ASC, getFieldOrder(x.getField()))));
        if (sorts.size() > 1) {

            var firstSort = sorts.get(0);
            for (int i = 1; i < sorts.size(); i++) {

                firstSort  = firstSort.and(sorts.get(i));
            }

            return firstSort;
        } else {
            return sorts.get(0);
        }
    }

    default String getFieldOrder(String field) {
        try {
            var sortField = findField(field, getModel());

            if (DefaultForm.class.isAssignableFrom(sortField.getType())) {
                return sortField.getName() + "." + ((BaseEntity) sortField.getType().getConstructor().newInstance()).defaultSearchOrderField().getKey();
            }
            return field;
        } catch (Exception e) {
            return field;
        }

    }

    default long count() {
        Specification<DB_TYPE> spec = getAdditionalSpecification();
        return getRepository().count(spec);
    }

    default long count(DefaultSearchSpecification spec) {
        Specification<DB_TYPE> customSpecification = getAdditionalSpecification();

        if (customSpecification != null) {
            return getRepository().count(customSpecification.and(spec));
        }

        return getRepository().count(spec);
    }

    @SneakyThrows
    default UrlResource download(MagicRequest request) {
        Page<DB_TYPE> data;
        var path = getContext().getEnv("global.download.path", "./", x -> x);
        Specification<DB_TYPE> customSpecification = getAdditionalSpecification();

        if (request.getQuery().isEmpty()) {
            if (customSpecification != null) {
                data = getRepository().findAll(customSpecification, PageRequest.of(0, 100000, createFromRequest(request)));
            } else {
                data = getRepository().findAll(PageRequest.of(0, 100000, createFromRequest(request)));
            }
//  to excel logic

        } else {
            //todo make a proper search!

            DefaultSearchSpecification<DB_TYPE> specification = QueryCase.ofNotNull("id").specification();

            for (var query : request.getQuery()) {
                specification.addCase(query.toQuery(getContext()));
            }

            if (customSpecification != null) {
                data = getRepository().findAll(specification.and(customSpecification),
                        PageRequest.of(0, 100000, createFromRequest(request)));
            } else {
                data = getRepository().findAll(specification, (PageRequest.of(0, 100000, createFromRequest(request))));
            }
        }

        List<Map<String, Object>> res = new ArrayList<>();
        var bc = getContext().getBean(BeanCopier.class);

        for (BaseEntity entity : data.getContent()) {
            var form = bc.copy(entity, getForm());
            Map<String, Object> values = BeanCopier.values(form, entity);
            res.add(values);
        }

        if (res.size() == 0) {
           res.add(Collections.singletonMap("res", " ---- NO DATA ---"));
        }


        Workbook workbook = null;

        String fileName = path + UUID.randomUUID().toString() + ".xls";

        if (fileName.endsWith("xlsx")) {
            workbook = new XSSFWorkbook();
        } else {
            workbook = new HSSFWorkbook();
        }

        Sheet sheet = workbook.createSheet("data");

        var headRow = 1;
        var cln = 0;
        Row row = sheet.createRow(headRow);
        for (String el : res.get(0).keySet()) {
            if ("action".equalsIgnoreCase(el)) {
                continue;
            }

            Cell cell0 = row.createCell(cln);
            cell0.setCellValue(el);
            cln++;

        }

        var dataRow = 2;
        var datacln = 0;

        for (Map<String, Object> element : res) {
            Row datarow = sheet.createRow(dataRow);


            for (Object el : element.values()) {
                Cell cell = datarow.createCell(datacln);
                if (el instanceof ObjectDefinition) {
                    el = ((ObjectDefinition) el).getValue();
                }

                if (el == null) {
                    el = "";
                }

                if (el.toString().length() > 5000){
                    el = String.valueOf(el).substring(0, 5000);
                }

                cell.setCellValue(String.valueOf(el));
                datacln++;
            }
            datacln = 0;
            dataRow++;

        }

        FileOutputStream fos = new FileOutputStream(fileName);
        workbook.write(fos);
        fos.close();
        System.out.println(fileName + " written successfully");

        return new UrlResource(new File(fileName).toURI());

    }



    default String getItemFavoriteType(){
        if (getForm().getAnnotation(PojoListView.class) == null){
            return getForm().getSimpleName();
        }

        return  getForm().getAnnotation(PojoListView.class).favoriteType();
    }

    default UrlResource getInterNalResource(String resource){

        var path = getContext().getEnv("global.download.path", "./", x -> x);
        String fileName = path + resource;
        try {
            return new UrlResource(new File(fileName).toURI());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    default Specification<DB_TYPE> getAdditionalSpecification() {
        return null;
    }

    default void preSearch(String what, String type){};

    default QueryCase modifyQuery(QueryCase queryCase){
        return null;
    }
}
