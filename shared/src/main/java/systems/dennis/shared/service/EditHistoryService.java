package systems.dennis.shared.service;

import org.springframework.stereotype.Service;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.form.EditHistoryForm;
import systems.dennis.shared.model.EditHistoryModel;
import systems.dennis.shared.repository.EditHistoryRepo;
import systems.dennis.shared.repository.QueryCase;

import java.util.List;
import java.util.Optional;

@Service
@DataRetrieverDescription(model = EditHistoryModel.class, form = EditHistoryForm.class, repo = EditHistoryRepo.class)
public class EditHistoryService extends PaginationService<EditHistoryModel> {
    public EditHistoryService(WebContext holder) {
        super(holder);
    }

    @Override
    public Optional<EditHistoryModel> findById(Long id) {
        return getRepository().findOne(QueryCase.equalsOf("id", id).integer().specification());
    }

    public Optional<EditHistoryModel> findByEditedTypeAndId(String type, Long id) {
        EditHistoryRepo repo = getRepository();
        return repo.findFirstByClassEditedObjectAndIdEditedObject(type, id);
    }

    public List<EditHistoryModel> findByEditedType(String type) {
        EditHistoryRepo repo = getRepository();
        return repo.findAllByClassEditedObject(type);
    }
}
