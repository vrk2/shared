package systems.dennis.shared.service;

import org.springframework.stereotype.Service;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.TableSettingModel;
import systems.dennis.shared.exceptions.ItemForAddContainsIdException;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.ItemNotUserException;
import systems.dennis.shared.exceptions.UnmodifiedItemSaveAttemptException;
import systems.dennis.shared.form.TableSettingForm;
import systems.dennis.shared.repository.TableSettingRepo;

import java.util.List;
import java.util.Objects;

@Service
@DataRetrieverDescription(model = TableSettingModel.class, form = TableSettingForm.class, repo = TableSettingRepo.class)
public class TableSettingService extends PaginationService<TableSettingModel> {

    public TableSettingService(WebContext holder) {
        super(holder);
    }

    @Override
    public TableSettingModel preAdd(TableSettingModel object) throws ItemForAddContainsIdException {
        object.setCurrentUser(getCurrentUser());

        var obj = getRepository().findFirstByTopicAndNameAndCurrentUser(object.getTopic(), object.getName(), object.getCurrentUser());

        obj.ifPresent(tableSettingModel -> object.setId(tableSettingModel.getId()));

        return object;
    }

    @Override
    public TableSettingModel preEdit(TableSettingModel object, TableSettingModel original) throws UnmodifiedItemSaveAttemptException, ItemNotFoundException {
        if (!Objects.equals(object.getCurrentUser(), getCurrentUser())){
            throw new ItemNotUserException();
        }
        return super.preEdit(object, original);
    }

    @Override
    public TableSettingModel preDelete(TableSettingModel object) {
        if (!Objects.equals(object.getCurrentUser(), getCurrentUser())){
            throw new ItemNotUserException();
        }
        return super.preDelete(object);
    }

    public List<TableSettingModel> findByUserAndTopic(String topic) {
        var currentId = getCurrentUser();
        return getRepository().findAllByTopicAndCurrentUser(topic, currentId);
    }

    @Override
    public TableSettingRepo getRepository() {
        return super.getRepository();
    }
}
