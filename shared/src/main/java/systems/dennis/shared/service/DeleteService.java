package systems.dennis.shared.service;

public interface DeleteService {
    void deleteById(String id);
}
