package systems.dennis.shared.service;

import org.springframework.stereotype.Service;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.form.DeleteHistoryForm;
import systems.dennis.shared.model.DeleteHistoryModel;
import systems.dennis.shared.repository.DeleteHistoryRepo;

import java.util.List;
import java.util.Optional;

@Service
@DataRetrieverDescription(model = DeleteHistoryModel.class, form = DeleteHistoryForm.class, repo = DeleteHistoryRepo.class)
public class DeleteHistoryService extends PaginationService<DeleteHistoryModel> {
    public DeleteHistoryService(WebContext holder) {
        super(holder);
    }

    @Override
    public Optional<DeleteHistoryModel> findById(Long id) {
        return getRepository().findById(id);
    }

    public boolean existsByTypeAndDeletedId(Long id, String type) {
        DeleteHistoryRepo repo = getRepository();
        return repo.existsByIdDeletedObjectAndClassDeletedObject(id, type);
    }

    public Optional<DeleteHistoryModel> findByTypeAndDeletedId(String type, Long id) {
        DeleteHistoryRepo repo = getRepository();
        return repo.findFirstByIdDeletedObjectAndClassDeletedObject(id, type);
    }

    public List<DeleteHistoryModel> findByType(String type) {
        DeleteHistoryRepo repo = getRepository();
        return repo.findAllByClassDeletedObject(type);
    }
}
