package systems.dennis.shared.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.SearchEntityApi;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.exceptions.ItemAlreadyExistsException;
import systems.dennis.shared.exceptions.ItemForAddContainsIdException;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.ItemNotUserException;
import systems.dennis.shared.form.FavoriteItemForm;
import systems.dennis.shared.model.FavoriteItemModel;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.repository.FavoriteItemRepo;
import systems.dennis.shared.repository.QueryCase;

import java.util.Optional;

@Service
@DataRetrieverDescription(model = FavoriteItemModel.class, form = FavoriteItemForm.class, repo = FavoriteItemRepo.class)
public class FavoriteItemService extends PaginationService<FavoriteItemModel> {

    @Value("${global.settings.favorite.enabled:true}")
    private Boolean isFavoriteEnabled;
    public FavoriteItemService(WebContext holder) {
        super(holder);
    }

    @Override
    public FavoriteItemModel preAdd(FavoriteItemModel object) throws ItemForAddContainsIdException {

        if (!getIsFavoriteEnabled()){
            throw new FavoriteException("global.exceptions.favorite.without_type.disabled");
        }

        Long currentUser = getCurrentUser();
        if (object.getType() == null){
            throw new FavoriteException ("global.exceptions.favorite.without_type");
        }

        if (existByModelAndUser(object.getModelId(), object.getType(), currentUser)) {
            throw new ItemAlreadyExistsException(object.getModelId().toString());
        }
        object.setUserDataId(currentUser);
        return object;
    }

    public String findBySearchName(String name){
        return SearchEntityApi.findServiceByType(name).getAnnotation(DataRetrieverDescription.class).form().getSimpleName();
    }



    public void delete(FavoriteItemForm id) throws ItemNotUserException, ItemNotFoundException {
        id.setUserDataId(getCurrentUser());

        var item =  findByModelAndUser(id.getModelId(), id.getType(), id.getUserDataId());
        item.ifPresent(favoriteItemModel -> delete(favoriteItemModel.getId()));

    }

    public Boolean getIsFavoriteEnabled(){
        return isFavoriteEnabled;
    }

    public boolean isFavoriteForObject(FavoriteItemForm form){
        form.setUserDataId(getCurrentUser());
        if (form.getModelId() == null){
            throw new FavoriteException("global.exceptions.favorite.favorite.no_model_id");
        }

        return existByModelAndUser(form.getModelId(), form.getType(), form.getUserDataId());

    }

    public boolean isFavoriteForObject(BaseEntity entity) {
        if (!getIsFavoriteEnabled()){
            return false;
        }
        if (entity == null) {
            return false;
        }


        String entityDescriptor = entity.getClass().getSimpleName();

        if (entity.getClass().getAnnotation(PojoListView.class) != null){
            entityDescriptor =  entityDescriptor.getClass().getAnnotation(PojoListView.class).favoriteType();
        }

        return this.existByModelAndUser(entity.getId(), entityDescriptor, getCurrentUser());
    }

    private Boolean existByModelAndUser(Long modelId, String entityDescriptor, Long currentUser) {
        if (!getIsFavoriteEnabled()){
            return false;
        }

        long count = getRepository().count(QueryCase.equalsOf("userDataId", currentUser).specification()
                .addCase(QueryCase.equalsOf("type", entityDescriptor))
                .addCase(QueryCase.equalsOf("modelId", modelId)));
        return count > 0;
    }

    private Optional<FavoriteItemModel> findByModelAndUser(Long modelId, String entityDescriptor, Long currentUser) {
        if (!getIsFavoriteEnabled()){
            return Optional.empty();
        }

        return getRepository().findOne(QueryCase.equalsOf("userDataId", currentUser).specification()
                .addCase(QueryCase.equalsOf("type", entityDescriptor))
                .addCase(QueryCase.equalsOf("modelId", modelId)));
    }
}
