package systems.dennis.shared.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.persistence.AttributeConverter;
import org.springframework.stereotype.Component;
import systems.dennis.shared.controller.items.magic.MagicRequest;
import systems.dennis.shared.exceptions.StandardException;

import static systems.dennis.shared.utils.Mapper.mapper;

@Component
public class MagicQueryConverter implements AttributeConverter<MagicRequest, String> {

    @Override
    public String convertToDatabaseColumn(MagicRequest magicQuery) {
        try {
            return mapper.writeValueAsString(magicQuery);
        } catch (JsonProcessingException e) {
            throw new StandardException(e, "JSON writing error");
        }
    }

    @Override
    public MagicRequest convertToEntityAttribute(String value) {
        try {
            return mapper.readValue(value, MagicRequest.class);
        } catch (JsonProcessingException e) {
            throw new StandardException(e, "JSON reading error");
        }
    }
}
