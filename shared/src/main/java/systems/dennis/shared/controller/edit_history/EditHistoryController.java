package systems.dennis.shared.controller.edit_history;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.annotations.security.SelfOnlyRole;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.items.ListItemController;
import systems.dennis.shared.exceptions.EditableObjectNotFoundException;
import systems.dennis.shared.form.EditHistoryForm;
import systems.dennis.shared.model.EditHistoryModel;
import systems.dennis.shared.service.EditHistoryService;
import systems.dennis.shared.utils.ApplicationContext;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v2/edited/history")
@WebFormsSupport(value = EditHistoryService.class)
public class EditHistoryController extends ApplicationContext
        implements ListItemController<EditHistoryModel, EditHistoryForm> {

    public EditHistoryController(WebContext context) {
        super(context);
    }

    @SelfOnlyRole
    @GetMapping("/find/{type}/{id}")
    public EditHistoryForm findByEditedTypeAndId(@PathVariable String type, @PathVariable Long id) {
        EditHistoryService service = getService();
        return toForm(service.findByEditedTypeAndId(type, id)
                .orElseThrow(() -> new EditableObjectNotFoundException(type, id)));
    }

    @SelfOnlyRole
    @GetMapping("/find/{type}")
    public List<EditHistoryForm> findAllByType(@PathVariable String type) {
        EditHistoryService service = getService();
        return service.findByEditedType(type)
                .stream()
                .map(this::toForm)
                .collect(Collectors.toList());
    }


}
