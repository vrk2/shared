package systems.dennis.shared.controller.forms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.items.AddItemController;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.repository.PaginationRepository;
import systems.dennis.shared.service.AbstractService;

import java.lang.annotation.Annotation;

@SuppressWarnings("ALL")
public interface Serviceable {
    Logger log = LoggerFactory.getLogger(AddItemController.class);

    default <E extends BaseEntity, T extends AbstractService<E>> T getService() {
        return (T) getContext().getBean(getServiceClass());
    }

    default <E extends BaseEntity> Class<? extends AbstractService<?>> getServiceClass(){
        return getClass().getAnnotation(WebFormsSupport.class).value();
    }

    WebContext.LocalWebContext getContext();




    static DataRetrieverDescription findDeclaredClass(Class c, DataRetrieverDescription model) {

        if (model == null) {
            WebFormsSupport ser = (WebFormsSupport) c.getAnnotation(WebFormsSupport.class);
            if (ser == null) {
                throw new UnsupportedOperationException("No @FormToModel annotation, no @RepoService annotation on " + c);
            }
            var serviceModel = ser.value();

            model = serviceModel.getAnnotation(DataRetrieverDescription.class);


            if (model == null) {
                throw new UnsupportedOperationException("@FormToModel annotation is not present on " + c + " and not present at service : " + ser.value());
            }

            if (ser.form() != DefaultForm.class) {
                DataRetrieverDescription finalModel = model;
                DataRetrieverDescription description = new DataRetrieverDescription() {
                    @Override
                    public Class<? extends Annotation> annotationType() {
                        return finalModel.annotationType();
                    }

                    @Override
                    public Class<? extends BaseEntity> model() {
                        return finalModel.model();
                    }

                    @Override
                    public Class<? extends DefaultForm> form() {
                        return ser.form();
                    }

                    @Override
                    public Class<? extends PaginationRepository<?>> repo() {
                        return finalModel.repo();
                    }
                };
                return description;
            }
        }
        return model;
    }

    default <T extends DefaultForm> Class<T> getForm() {
        var res = findDeclaredClass(getClass(), getClass().getAnnotation(DataRetrieverDescription.class));

        return (Class<T>) res.form();
    }

    default <T extends DefaultForm> Class<T> getModel() {
        var res = findDeclaredClass(getClass(), getClass().getAnnotation(DataRetrieverDescription.class));

        return (Class<T>) res.model();
    }

    default Logger getLog() {
        return log;
    }
}
