package systems.dennis.shared.controller;

import lombok.Data;

import java.util.Date;

@Data
public class ApiErrorDescription {
    private String errorMessage;
    private Date date = new Date();
    private String url;

    public static ApiErrorDescription of(Exception e, String res) {
        ApiErrorDescription description = new ApiErrorDescription();
        description.setErrorMessage(e.getMessage());
        description.setUrl(res);
        return description;
    }
}
