package systems.dennis.shared.controller;

import lombok.Data;
import systems.dennis.shared.utils.PojoListField;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
public class DefaultFormDetailsContainer {
    private Map<String,  Object> data;

    private List<PojoListField> fields = new ArrayList<>();
}
