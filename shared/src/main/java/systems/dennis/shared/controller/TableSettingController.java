package systems.dennis.shared.controller;

import org.springframework.web.bind.annotation.*;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.items.AddItemController;
import systems.dennis.shared.controller.items.DeleteItemController;
import systems.dennis.shared.entity.TableSettingModel;
import systems.dennis.shared.form.TableSettingForm;
import systems.dennis.shared.service.TableSettingService;
import systems.dennis.shared.utils.ApplicationContext;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v2/shared/table_setting")
@WebFormsSupport(value = TableSettingService.class)
@CrossOrigin
public class TableSettingController
        extends ApplicationContext
        implements AddItemController<TableSettingModel, TableSettingForm>,
        DeleteItemController {

    public TableSettingController(WebContext context) {
        super(context);
    }

    @GetMapping("/list")
    @WithRole
    public List<TableSettingForm> findByUserAndTopic(@RequestParam (required = true) String topic){
        return  getService().findByUserAndTopic (topic).stream().map(this::toForm).collect(Collectors.toList());
    }

    @Override
    public TableSettingService getService() {
        return AddItemController.super.getService();
    }
}
