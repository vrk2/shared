package systems.dennis.shared.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@RestController ()
@RequestMapping("/__health")
public class HealthCheck {

    @GetMapping
    public String ok(){
        return "OK";
    }

    @GetMapping(value = "/check", produces = "application/json")

    public int checkHealth(@RequestParam String path){

        try {
            URI uri = new URI(path);
            path = uri.getScheme() + "://" + uri.getHost() + ":" + uri.getPort() + "/";
        } catch (URISyntaxException e) {
           return -3;
        }

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(path + "__health"))
                .GET() // GET is default
                .build();

        HttpResponse<Void> response = null;
        try {
            response = client.send(request,
                    HttpResponse.BodyHandlers.discarding());
            return response.statusCode();
        } catch (IOException e) {
            e.printStackTrace();
            return  -2;
        } catch (InterruptedException e) {
            return  -1;
        }

    }
}
