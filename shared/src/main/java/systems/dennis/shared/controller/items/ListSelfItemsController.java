package systems.dennis.shared.controller.items;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import systems.dennis.shared.annotations.security.Secured;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.controller.forms.Serviceable;
import systems.dennis.shared.controller.items.magic.MagicList;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.repository.DefaultSearchSpecification;
import systems.dennis.shared.repository.QueryCase;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * List  object interface. Implementing this interface, will automatically create ../list/
 *
 * @param <DB_TYPE>       Entity which is managed by this interface and Service
 */
@RestController
@Secured
public interface ListSelfItemsController<DB_TYPE extends BaseEntity, FORM extends DefaultForm> extends Serviceable, MagicList<FORM>, Transformable< DB_TYPE, FORM> {

    @GetMapping("/list")
    @ResponseBody
    @WithRole
    default ResponseEntity<Page<FORM>> getData(@RequestParam(value = "from", required = false) Long from,
                                               @RequestParam (value = "limit", required = false) Integer limit,
                                               @RequestParam(value = "page", required = false) Integer page) {

        DefaultSearchSpecification<BaseEntity> specification =modifySearchSpecification(QueryCase.equalsOf("userDataId", getContext().getCurrentUser()).specification());
        var data = getService().find(specification, from, limit, page);
        List<FORM> result= new ArrayList<>();


        if (data.isEmpty()){

            result = (List<FORM>) onEmpty().doOnEmptyResults();
        }else {
            for (var item : data) {
                result.add(toForm((DB_TYPE) item));
            }
        }

        Page<FORM> request = new PageImpl<>(result, PageRequest.of(page, limit), result.size());
        return ResponseEntity.ok(request);
    }

    default OnEmptyResult onEmpty(){
        return OnEmptyResult.INSTANCE;
    }

    default DefaultSearchSpecification<BaseEntity> modifySearchSpecification(DefaultSearchSpecification specification){
        return specification;
    }


}
