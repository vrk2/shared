package systems.dennis.shared.controller.items.magic;

import lombok.Data;
import lombok.SneakyThrows;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.SearchEntityApi;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.OperationNotAllowedException;
import systems.dennis.shared.repository.QueryCase;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class MagicQuery {

    public static final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyy");
    private String field;
    private String type;
    private Object value;

    private String searchName;

    private String dataType;

    @SneakyThrows
    public QueryCase toQuery(WebContext.LocalWebContext context) {

        if (dataType.equals("checkbox")) {
            if (value.equals("null")) {
                return QueryCase.ofNull(field);
            }
            value = Boolean.valueOf(value.toString());

        }

        if (dataType.equals("date")) {

            this.value = format.parse(((String) value).replaceAll("/", "."), new ParsePosition(0));
        }

        if (dataType.equals("object_chooser")) {
            if (value != null && !value.toString().trim().equals("")) {
                try {
                    value = context.getBean(SearchEntityApi.findServiceByType(searchName)).findById(Long.valueOf(value.toString())).orElseThrow(() -> new ItemNotFoundException((Long) value));
                } catch (Exception e) {
                    System.out.println(value);
                }
            }
        }

        if (Objects.equals(this.type, "=")) {
            if (value == null || value.toString().isBlank()) {
                return QueryCase.ofNull(field);
            }
            return QueryCase.equalsOf(field, value);
        }

        if (Objects.equals(this.type, "!=")) {
            if (value == null || value.toString().isBlank()) {
                return QueryCase.ofNotNull(field);
            }
            return QueryCase.notEqualsOf(field, value);
        }

        if (Objects.equals(this.type, ">=")) {
            return QueryCase.moreOrEquals(field, value);
        }

        if (Objects.equals(this.type, ">")) {
            return QueryCase.more(field, value);
        }

        if (Objects.equals(this.type, "<=")) {
            return QueryCase.lessOrEquals(field, value);
        }

        if (Objects.equals(this.type, "<")) {
            return QueryCase.less(field, value);
        }

        if (Objects.equals(this.type, "%...%")) {
            return QueryCase.containsInSensitive(field, value);
        }
        if (Objects.equals(this.type, "!%...%")) {
            return QueryCase.notContainsInSensitive(field, value);
        }

        if (Objects.equals(this.type, "%...")) {
            return QueryCase.endsInSensitive(field, value);
        }
        if (Objects.equals(this.type, "!%...")) {
            return QueryCase.notEndsInSensitive(field, value);
        }

        if (Objects.equals(this.type, "...%")) {
            return QueryCase.startsInSensitive(field, value);
        }
        if (Objects.equals(this.type, "!...%")) {
            return QueryCase.notStartsInSensitive(field, value);
        }

        if (Objects.equals(this.type, "is_null")) {
            return QueryCase.ofNull(field);
        }

        if (Objects.equals(this.type, "not_null")) {
            return QueryCase.ofNotNull(field);
        }

        if (Objects.equals(this.type, "in")) {
            List<Object> sourceList = new ArrayList<>();
            if (value != null) {
                Object model;
                var service = context.getBean(SearchEntityApi.findServiceByType(searchName));

                for (Integer integer : (List<Integer>) value) {
                    try {
                        Long id = Long.valueOf(integer.toString());
                        model = service.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
                        sourceList.add(model);
                    } catch (Exception e) {
                        System.out.println(value);
                    }
                }
            }
            return QueryCase.in(field, sourceList);
        }
        if (Objects.equals(this.type, "not_in")) {
            List<Object> sourceList = new ArrayList<>();
            if (value != null) {
                Object model;
                var service = context.getBean(SearchEntityApi.findServiceByType(searchName));

                for (Integer integer : (List<Integer>) value) {
                    try {
                        Long id = Long.valueOf(integer.toString());
                        model = service.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
                        sourceList.add(model);
                    } catch (Exception e) {
                        System.out.println(value);
                    }
                }
            }
            return QueryCase.notIn(field, sourceList);
        }

        throw new OperationNotAllowedException("Do not know operator send to query");
    }
}
