package systems.dennis.shared.controller.items;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import systems.dennis.shared.annotations.security.Secured;
import systems.dennis.shared.annotations.security.SelfOnlyRole;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.controller.forms.Serviceable;
import systems.dennis.shared.controller.items.magic.MagicForm;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.entity.KeyValue;
import systems.dennis.shared.exceptions.EditFieldException;
import systems.dennis.shared.exceptions.FieldNotFoundException;
import systems.dennis.shared.exceptions.ItemDoesNotContainsIdValueException;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.UnmodifiedItemSaveAttemptException;
import systems.dennis.shared.utils.bean_copier.BeanCopier;

import java.lang.reflect.InvocationTargetException;


/**
 * Update object interface. Implementing this interface, will automatically create ../edit_field/field/{field}
 *
 * @param <DB_TYPE> Entity which is managed by this interface and Service
 */
@RestController
@Secured
public interface EditFieldItemController<DB_TYPE extends BaseEntity, FORM extends DefaultForm>
        extends Serviceable, Transformable<DB_TYPE, FORM>, MagicForm<FORM> {

    @WithRole
    @PutMapping(value = "/edit_field/id/{id}")
    @ResponseBody
    default ResponseEntity<UpdateFieldResponse> editField(@PathVariable Long id, @RequestBody KeyValue keyValue)
            throws ItemDoesNotContainsIdValueException, UnmodifiedItemSaveAttemptException, ItemNotFoundException, IllegalAccessException, InvocationTargetException {
        if (keyValue.getKey() == null) {
            throw new EditFieldException(keyValue);
        }

        var field = BeanCopier.findField(keyValue.getKey(), getForm());
        if (field == null) {
            throw new FieldNotFoundException(keyValue.getKey());
        }

        var type = field.getType();
        Object value = keyValue.getValue();

        if (type == Long.class) {
            value = ((Integer) value).longValue();
        }

        validate(keyValue);

        var transformedToModelTypeKeyValue = transformValueToModelType(new KeyValue(keyValue.getKey(), value));
        KeyValue keyValueFromModel = getService().editField(id, transformedToModelTypeKeyValue);

        var transformedToFormTypeKeyValue = transformValueToFormType(keyValueFromModel);

        var response = new UpdateFieldResponse(id, transformedToFormTypeKeyValue);
        return ResponseEntity.ok(response);
    }

}
