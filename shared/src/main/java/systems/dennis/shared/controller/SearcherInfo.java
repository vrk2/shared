package systems.dennis.shared.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import systems.dennis.shared.service.AbstractService;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearcherInfo {
    private String field;
    private Class <? extends AbstractService> searchClass;
}
