package systems.dennis.shared.controller.items.magic;

import lombok.Data;
import systems.dennis.shared.repository.QueryCase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
public class MagicRequest implements Serializable   {
    private Integer page = 0;
    private Integer limit = 20;

    private List<QueryCase> cases = new ArrayList<>();
    private List<MagicOrder> sort = Collections.singletonList(MagicOrder.ID_DESC);

    private List<MagicQuery> query = Collections.emptyList();
}
