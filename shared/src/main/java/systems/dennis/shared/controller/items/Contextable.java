package systems.dennis.shared.controller.items;

import systems.dennis.shared.config.WebContext;

public interface Contextable {
    WebContext.LocalWebContext getContext();
}
