package systems.dennis.shared.controller.items;

import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.forms.Serviceable;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.entity.KeyValue;
import systems.dennis.shared.utils.bean_copier.BeanCopier;
import systems.dennis.shared.utils.bean_copier.DataTransformer;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface Transformable<DB_TYPE extends BaseEntity, FORM extends DefaultForm> {

    @SneakyThrows
    default DB_TYPE fromForm(FORM form) {
        var cl = Serviceable.findDeclaredClass(getClass(), getClass().getAnnotation(DataRetrieverDescription.class)).model();

        return afterTransform((DB_TYPE) getContext().getBean(BeanCopier.class).copy(form, cl));

    }

    @SneakyThrows
    default FORM toForm(DB_TYPE model) {
        var cl = Serviceable.findDeclaredClass(getClass(), getClass().getAnnotation(DataRetrieverDescription.class)).form();
        var res = getContext().getBean(BeanCopier.class).copy(model, cl);

        return afterTransform((FORM) res);
    }

    @SneakyThrows
    default KeyValue transformValueToTargetType(KeyValue keyValue, Class<?> targetClass,
                                                Class<? extends DefaultForm> sourceClass) {

        Field editField = BeanCopier.findField(keyValue.getKey(), sourceClass);
        DataTransformer transformer = editField.getAnnotation(DataTransformer.class);

        Object transformableValue = keyValue.getValue();

        if (transformer != null) {
            var converter = transformer.transFormWith().newInstance();
            if (transformableValue instanceof BaseEntity) {
                transformableValue = converter.transform(transformableValue, transformer, ((BaseEntity) transformableValue).getId().getClass(), getContext());
            } else {
                transformableValue = converter.transform(transformableValue, transformer, targetClass, getContext());
            }
        }

        return new KeyValue(keyValue.getKey(), transformableValue);
    }

    @SneakyThrows
    default KeyValue transformValueToModelType(KeyValue keyValue) {
        var description = getClassDescription();
        var targetClass = description.model();
        var sourceClass = description.form();

        return transformValueToTargetType(keyValue, targetClass, sourceClass);
    }

    @SneakyThrows
    default KeyValue transformValueToFormType(KeyValue keyValue) {
        var description = getClassDescription();
        var targetClass = description.form();
        var sourceClass = description.model();

        return transformValueToTargetType(keyValue, targetClass, sourceClass);
    }

    default DataRetrieverDescription getClassDescription() {
        return getClass()
                .getAnnotation(WebFormsSupport.class).value()
                .getAnnotation(DataRetrieverDescription.class);
    }

    default FORM afterTransform(FORM ob) {
        return ob;
    }

    default DB_TYPE afterTransform(DB_TYPE ob) {
        return ob;
    }

    default Page<Map<String, Object>> toFormPage(Page<DB_TYPE> page) {
        BeanCopier beanCopier = getContext().getBean(BeanCopier.class);
        var cl = Serviceable.findDeclaredClass(getClass(), getClass().getAnnotation(DataRetrieverDescription.class)).model();

        List<Map<String, Object>> data = new ArrayList<>();
        for (int i = 0; i < page.getContent().size(); i++) {
            BaseEntity model = page.getContent().get(i);
            var form = beanCopier.copy(model, cl);
            data.add(BeanCopier.values(form, model));
        }
        return new PageImpl<>(data, page.getPageable(), page.getTotalElements());
    }

    WebContext.LocalWebContext getContext();
}
