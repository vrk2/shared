package systems.dennis.shared.controller.items.magic;

import lombok.SneakyThrows;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.controller.DefaultFormDetailsContainer;
import systems.dennis.shared.controller.forms.Serviceable;
import systems.dennis.shared.controller.items.Contextable;
import systems.dennis.shared.controller.items.Transformable;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.pojo_view.DEFAULT_TYPES;
import systems.dennis.shared.pojo_view.UIAction;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.pojo_view.list.PojoListViewField;
import systems.dennis.shared.utils.GeneratedPojoList;
import systems.dennis.shared.utils.PojoListField;
import systems.dennis.shared.utils.bean_copier.BeanCopier;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.TEXT;

public interface MagicList<T extends DefaultForm> extends Contextable {

    Map<String, GeneratedPojoList> pojoListCache = new HashMap<>();

    @GetMapping(value = {"/root/fetch/list", "/root/fetch/list/"}, produces = "application/json")
    @ResponseBody
    @WithRole
    default GeneratedPojoList fetchForm() {

        Class<? extends DefaultForm> formClass = getClass().getAnnotation(WebFormsSupport.class).value()
                .getAnnotation(DataRetrieverDescription.class).form();

        return pojoListCache.containsKey(formClass.getName()) ? pojoListCache.get(formClass.getName()) : getPojoList(formClass);
    }

    @SneakyThrows
    default GeneratedPojoList getPojoList(Class<? extends DefaultForm> formClass) {
        GeneratedPojoList generatedPojoList = new GeneratedPojoList();

        if (formClass.getAnnotation(PojoListView.class) != null) {
            copyTo(formClass.getAnnotation(PojoListView.class), generatedPojoList, formClass);
        } else {
            generatedPojoList.setShowTitle(true);
            generatedPojoList.setObjectType(formClass.getSimpleName());
        }
        generatedPojoList.setTableTitle(getContext().getScoped(getClass().getSimpleName() + "_list").toLowerCase());
        generatedPojoList.setDefaultField(getDefaultField());

        BeanCopier.withEach(formClass, field -> getDescription(field, generatedPojoList));

        generatedPojoList.setDefaultSorting(formClass.getConstructor().newInstance().defaultSearchOrderField());
        pojoListCache.put(formClass.getName(), generatedPojoList);
        return generatedPojoList;
    }

    default String getDefaultField() {
        return "name";
    }

    @SneakyThrows
    @GetMapping(value = {"/root/fetch/details/{id}", "/root/fetch/details/{id}/"}, produces = "application/json")
    @WithRole
    default DefaultFormDetailsContainer objectDetails(@PathVariable("id") Long id) {

        var bean = ((Serviceable) this).getService();
        var res = (BaseEntity) bean.findById(id).orElseThrow(() -> new ItemNotFoundException(id));

        DefaultFormDetailsContainer container = new DefaultFormDetailsContainer();
        container.setData(BeanCopier.values(((Transformable) this).toForm(res), res));
        container.setFields(fetchForm().getFields());

        return container;
    }


    @PostMapping(value = {"/root/fetch/data", "/root/fetch/data/"}, produces = "application/json", consumes = "application/json")
    @ResponseBody
    @WithRole
    default Page<Map<String, Object>> fetchData(@RequestBody MagicRequest request) {

        var service = getContext().getBean(getClass().getAnnotation(WebFormsSupport.class).value());

        var res = service.search(request);

        var pageList = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < res.getContent().size(); i++) {
            BaseEntity model = res.getContent().get(i);
            var form = ((Transformable) this).toForm(model);
            pageList.add(BeanCopier.values(form, model));
        }
        return new PageImpl<>(pageList, res.getPageable(), res.getTotalElements());
    }


    @SneakyThrows
    @PostMapping(value = {"/root/download/data", "/root/download/data/", }, consumes = "application/json" )
    @ResponseBody
    @WithRole
    default GeneratedReport download(@RequestBody MagicRequest request) {


        UrlResource generatedReportPath = getContext().getBean(getClass().getAnnotation(WebFormsSupport.class).value()).download(request);

        GeneratedReport report = new GeneratedReport();

        report.setPathToDownload(generatedReportPath.getFilename());
        report.setType("application/oset-stram");

        return report;

    }
    @SneakyThrows
    @GetMapping(value = {"/root/download/data/{name}", "/root/download/data/{name}/", } )
    @ResponseBody

    default ResponseEntity<UrlResource> fetchFile(@PathVariable String name) {


        UrlResource generatedReportPath = getContext().getBean(getClass().getAnnotation(WebFormsSupport.class).value()).getInterNalResource(name);

         return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + name + "\"").body(generatedReportPath);


    }

    default void getDescription(Field f, GeneratedPojoList generatedPojoList) {

        var el = f.getAnnotation(PojoListViewField.class);

        PojoListField field = new PojoListField();

        if (el == null) {
            field.setOrder(0);
            field.setSearchType(TEXT);
            field.setRemoteType("");
//            field.setCss();
            field.setSearchable(false);
            field.setShowContent(true);
            field.setSortable(true);

            field.setType(TEXT);
            field.setVisible(true);
            field.setGroup(null);
        } else {
            if (!el.available()) {
                return;
            }
            field.setOrder(el.order());
            field.setSearchType(el.remote().searchType());
            field.setActions(Arrays.stream(el.actions()).map(UIAction::component).collect(Collectors.toList()));
//            field.setCss();

            field.setSearchable(el.searchable());
            field.setShowContent(el.showContent());
            field.setSortable(el.sortable());
            field.setFormat(el.format());
            field.setType(el.type());
            field.setGroup(el.group());

            if (!"".equals(el.remote().fetcher())) {
                field.setType(DEFAULT_TYPES.OBJECT_SEARCH);
            }

            if (!"".equals(el.remote().searchName())) {
                field.setSearchField(el.remote().searchField());
                field.setSearchName(el.remote().searchName());
            }
            field.setVisible(el.visible());
            field.setRemoteType(el.remote().fetcher());
        }
        field.setField(f.getName());
        field.setTranslation(getContext().getScoped(f.getDeclaringClass().getSimpleName() + "." + f.getName()).toLowerCase());
        generatedPojoList.getFields().add(field);
    }

    default void copyTo(PojoListView annotation, GeneratedPojoList generatedPojoList, Class<? extends DefaultForm> formClass) {
        generatedPojoList.setShowTitle(annotation.enableTitle());
        generatedPojoList.setSearchEnabled(annotation.enableSearching());
        generatedPojoList.setObjectType(annotation.favoriteType().isEmpty() ? formClass.getSimpleName() : annotation.favoriteType());
        generatedPojoList.setListActions(Arrays.asList(annotation.actions()));
        if (annotation.enableTitle() && !Objects.equals(annotation.title(), "")) {
            generatedPojoList.setTableTitle(annotation.title());
        } else {
            generatedPojoList.setTableTitle(this.getClass().getSimpleName() + ".list.title");
        }
    }


}
