package systems.dennis.shared.controller;

import org.springframework.web.bind.annotation.*;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.beans.LocaleBean;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.KeyValue;
import systems.dennis.shared.utils.ApplicationContext;

import java.util.List;

@RestController
@CrossOrigin()
@RequestMapping("/api/v1/translation")
public class TranslationService extends ApplicationContext {

    private LocaleBean localeBean;

    public TranslationService(WebContext context, LocaleBean localeBean) {
        super(context);
        this.localeBean = localeBean;
    }

    @GetMapping("/{text}/{lang}")
    public String translate(@PathVariable("text") String text, @PathVariable("lang") String lang) {
        return getContext().getMessageTranslation(text, lang);
    }

    @GetMapping("/cache/clean")
    @WithRole("ROLE_ADMIN")
    public void cleanCache() {
        localeBean.clearCache();
    }

    @GetMapping(value = "/list/{lang}", produces = "application/json")
    public List<KeyValue> translations(@PathVariable("lang") String lang) {
        return localeBean.getMessageTranslations(lang);
    }

}
