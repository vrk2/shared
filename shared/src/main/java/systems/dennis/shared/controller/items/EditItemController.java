package systems.dennis.shared.controller.items;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import systems.dennis.shared.annotations.security.Secured;
import systems.dennis.shared.controller.forms.Serviceable;
import systems.dennis.shared.controller.items.magic.MagicForm;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.exceptions.*;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.annotations.security.SelfOnlyRole;
import systems.dennis.shared.annotations.security.WithRole;

/**
 * Update object interface. Implementing this interface, will automatically create ../edit/
 *
 * @param <DB_TYPE> Entity which is managed by this interface and Service
 */
@RestController
@Secured
public interface EditItemController<DB_TYPE extends BaseEntity, FORM extends DefaultForm> extends Serviceable, Transformable< DB_TYPE, FORM>, MagicForm<FORM>,
        EditFieldItemController<DB_TYPE, FORM> {


    @PutMapping(value = "/edit")
    @ResponseBody
    @SelfOnlyRole
    @WithRole
    default ResponseEntity<FORM> edit(@RequestBody FORM form) throws ItemForAddContainsIdException, ItemDoesNotContainsIdValueException, UnmodifiedItemSaveAttemptException, ItemNotFoundException {
        validate(form, true);
        return ResponseEntity.ok(toForm((DB_TYPE) getService().edit(fromForm(form))));
    }

}
