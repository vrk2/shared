package systems.dennis.shared.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.items.DeleteItemController;
import systems.dennis.shared.controller.items.EditItemController;
import systems.dennis.shared.controller.items.ListItemController;
import systems.dennis.shared.exceptions.ItemDoesNotContainsIdValueException;
import systems.dennis.shared.exceptions.ItemForAddContainsIdException;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.ItemNotUserException;
import systems.dennis.shared.exceptions.UnmodifiedItemSaveAttemptException;
import systems.dennis.shared.form.AppSettingsForm;
import systems.dennis.shared.model.AppSettingsModel;
import systems.dennis.shared.service.AppSettingsService;
import systems.dennis.shared.utils.ApplicationContext;

import java.util.List;

@RestController
@RequestMapping("/api/v2/shared/app_settings")
@WebFormsSupport(value = AppSettingsService.class)
@CrossOrigin
public class AppSettingsController
        extends ApplicationContext
        implements ListItemController<AppSettingsModel, AppSettingsForm>,
        EditItemController<AppSettingsModel, AppSettingsForm>,
        DeleteItemController {

    public AppSettingsController(WebContext context) {
        super(context);
    }

    @Override
    @GetMapping("/list")
    @WithRole("ROLE_APP_CONFIGURATOR")
    public ResponseEntity<List<AppSettingsForm>> get(Long from, Integer limit, Integer page) {
        return ListItemController.super.get(from, limit, page);
    }

    @Override
    @PutMapping( "/edit")
    @WithRole("ROLE_APP_CONFIGURATOR")
    public ResponseEntity<AppSettingsForm> edit(AppSettingsForm form) throws ItemForAddContainsIdException, ItemDoesNotContainsIdValueException, UnmodifiedItemSaveAttemptException, ItemNotFoundException {
        return EditItemController.super.edit(form);
    }

    @Override
    @DeleteMapping ("/delete/{id}")
    @WithRole("ROLE_APP_CONFIGURATOR")
    public void delete(@PathVariable Long id) throws ItemNotUserException, ItemNotFoundException {
        DeleteItemController.super.delete(id);
    }
}
