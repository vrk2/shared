package systems.dennis.shared.controller.items;


import org.springframework.web.bind.annotation.*;
import systems.dennis.shared.annotations.security.Secured;
import systems.dennis.shared.annotations.security.SelfOnlyRole;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.controller.forms.Serviceable;
import systems.dennis.shared.controller.items.magic.MagicForm;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.exceptions.ItemForAddContainsIdException;
import org.springframework.http.ResponseEntity;

/**
 * An interface which adds possibility to add data
 * <p>
 * requires a service, to be processed, which is accessed threw the getService method.
 */
@RestController
@Secured
public interface AddItemController<DB_TYPE extends BaseEntity, FORM extends DefaultForm> extends Serviceable, Transformable<DB_TYPE, FORM>, MagicForm<FORM> {

    /**
     * Performs add procedure
     *
     * @param model model
     * @return an object which had been saved in DB with it's ID
     * @throws ItemForAddContainsIdException - this exception, means that object is tried to be updated instead of added. To devide add and edit is necessary as two different operations with different privileges
     */
    @PostMapping(value = "/add")
    @ResponseBody
    @WithRole
    default ResponseEntity<FORM> add(@RequestBody FORM model) throws ItemForAddContainsIdException {
        validate(model, model.getId() != null);
        return ResponseEntity.ok(toForm((DB_TYPE) getService().save(fromForm(model))));
    }





}
