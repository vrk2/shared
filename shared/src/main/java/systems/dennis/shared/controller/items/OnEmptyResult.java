package systems.dennis.shared.controller.items;

import java.util.List;

public interface OnEmptyResult {
    public static OnEmptyResult INSTANCE = () -> null;

    List<?> doOnEmptyResults();
}
