package systems.dennis.shared.controller.items.magic;

import lombok.SneakyThrows;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.annotations.FormTransient;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.controller.forms.Serviceable;
import systems.dennis.shared.controller.forms.ValidateForm;
import systems.dennis.shared.controller.items.Transformable;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.pojo_form.PojoForm;
import systems.dennis.shared.pojo_form.PojoFormElement;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.utils.GeneratedPojoForm;
import systems.dennis.shared.utils.PojoFormField;
import systems.dennis.shared.utils.bean_copier.BeanCopier;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public interface MagicForm<FORM extends DefaultForm> extends ValidateForm<FORM> {

    Map<String, GeneratedPojoForm> pojoFormCache = new HashMap<>();

    @GetMapping(value = "/root/fetch/form", produces = "application/json")
    @ResponseBody
    @WithRole
    default GeneratedPojoForm fetchForm(@RequestParam(required = false) Long id) {

        Class<? extends DefaultForm> formClass = getClass().getAnnotation(WebFormsSupport.class).value()
                .getAnnotation(DataRetrieverDescription.class).form();

        var receivedForm = getPojoForm(formClass, id);
        receivedForm.setAction(getClass().getAnnotation(RequestMapping.class).value()[0] + (id == null ?  "/add" : "/edit"));
        receivedForm.setMethod( id== null ? HttpMethod.POST.name(): HttpMethod.PUT.name());

        if (id != null) {
            setPojoFormValue(id, receivedForm);
        }
        return receivedForm;
    }

    default GeneratedPojoForm getPojoForm(Class<? extends DefaultForm> formClass, Long id) {

        if (pojoFormCache.containsKey(formClass.getName())) {
            var res = pojoFormCache.get(formClass.getName());
            return getContext().getBean(BeanCopier.class).clone(res);

        } else {
            GeneratedPojoForm generatedPojoForm = new GeneratedPojoForm();

            PojoListView ann  = formClass.getAnnotation(PojoListView.class);
            if (Objects.equals(ann.favoriteType(), "")){
                generatedPojoForm.setObjectType(formClass.getSimpleName());
            } else {
                generatedPojoForm.setObjectType(ann.favoriteType());
            }

            if (formClass.getAnnotation(PojoForm.class) != null) {
                copyTo(formClass.getAnnotation(PojoForm.class), generatedPojoForm);
            } else {
                generatedPojoForm.setTitle(getContext().getScoped(getClass().getSimpleName()).toLowerCase());
                generatedPojoForm.setCommitText("global.submit");
                generatedPojoForm.setShowTitle(true);
            }


            BeanCopier.withEach(formClass, field -> getDescription(field, generatedPojoForm));

            pojoFormCache.put(formClass.getName(), generatedPojoForm);

            generatedPojoForm.getFieldList().sort(Comparator.comparing(PojoFormField::getOrder));

            return getContext().getBean(BeanCopier.class).clone(generatedPojoForm);
        }
    }

    default void setPojoFormValue(Long id, GeneratedPojoForm pojoForm) {
        var service = ((Serviceable) this).getService();
        var model = service.findByIdOrThrow(id);
        var form = ((Transformable) this).toForm(model);
        pojoForm.setValue(BeanCopier.values(form, model));
    }

    default void copyTo(PojoForm annotation, GeneratedPojoForm generatedPojoForm) {
        generatedPojoForm.setTitle(annotation.title().title());
        generatedPojoForm.setShowTitle(annotation.title().show());
        generatedPojoForm.setCommitText(annotation.commitButtonText());
    }

    @SneakyThrows
    default void getDescription(Field x, GeneratedPojoForm generatedForm) {

        if (x.getAnnotation(FormTransient.class) == null) {
            if (x.getAnnotation(PojoFormElement.class) != null && x.getAnnotation(PojoFormElement.class).available()) {
                generatedForm.getFieldList().add(PojoFormField.from(x, getContext()));
            }
        }
    }
}