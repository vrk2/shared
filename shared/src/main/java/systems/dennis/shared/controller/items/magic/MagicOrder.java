package systems.dennis.shared.controller.items.magic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MagicOrder {

    public static final MagicOrder ID_DESC = new MagicOrder("id", true);


    private String field;
    private Boolean desc;
}
