package systems.dennis.shared.controller.items;

import systems.dennis.shared.annotations.security.Id;
import systems.dennis.shared.annotations.security.Secured;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.controller.forms.Serviceable;
import systems.dennis.shared.exceptions.DeleteNotPossibleException;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.ItemNotUserException;
import systems.dennis.shared.annotations.security.SelfOnlyRole;
import org.springframework.web.bind.annotation.*;
import systems.dennis.shared.exceptions.ItemWasDeletedException;
import systems.dennis.shared.utils.security.DefaultIdChecker;

import java.util.List;

/**
 * Delete object interface. Implementing this interface, will automatically create ../delete/{id}
 */
@RestController
@Secured
public interface DeleteItemController extends Serviceable {


    @WithRole
    @SelfOnlyRole
    @RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
    default void delete(@Id(checker = DefaultIdChecker.class) @PathVariable Long id) throws ItemNotUserException, ItemNotFoundException {
        try {
            getService().delete(id);
        } catch (ItemWasDeletedException | ItemNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new DeleteNotPossibleException();
        }
    }


    @RequestMapping(value="/delete/deleteItems", method=RequestMethod.POST)
    default void deleteItems(@RequestParam List<Long> ids) throws ItemNotUserException, ItemNotFoundException {
        for (Long id : ids) {
            delete(id);
        }
    }

}
