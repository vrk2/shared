package systems.dennis.shared.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.exceptions.*;

import static systems.dennis.shared.utils.Mapper.mapper;

@Slf4j
@ControllerAdvice
public class GlobalExceptionController {
    public static String REDIRECT_PARAM = "redirect";
    private ExternalControllerAdvice external = null;

    @Value("${pages.auth.login:/client_login}")
    private String loginUrl;
    private final WebContext.LocalWebContext context;

    public GlobalExceptionController(WebContext context) {
        try {
            this.external = context.getBean(ExternalControllerAdvice.class);
        } catch (Exception e) {
            log.debug(e.getMessage());
        }
        this.context = WebContext.LocalWebContext.of("global.exception", context);
    }


    @SneakyThrows
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Object exception(Exception e, HttpServletResponse response, WebRequest request) {
        e.printStackTrace();
        if (external == null) {
            if (e instanceof StatusException) {
                response.setStatus(((StatusException) e).getCode().value());

            }
            log.error("", e);

            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.setContentType("application/json");
            return mapper.writeValueAsString(ApiErrorDescription.of(e, ((ServletWebRequest) request).getRequest().getRequestURI()));
        }
        return external.onException(e.getClass(), e, response, request, this);
    }


    @SneakyThrows
    @ExceptionHandler(ValidationFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Object validationFailed(ValidationFailedException e, HttpServletResponse response, WebRequest request) {
        return new ObjectMapper().writeValueAsString(e.getErrorMessages());
    }


    @ExceptionHandler(UnmodifiedItemSaveAttemptException.class)
    @ResponseStatus(HttpStatus.NOT_MODIFIED)
    @ResponseBody
    public Object unmodifiedItemSaveAttempt(UnmodifiedItemSaveAttemptException ex, HttpServletResponse response, WebRequest request) {
        return String.valueOf(exception(ex, response, request));
    }


    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({ItemNotFoundException.class})
    @ResponseBody
    public String itemNotFound(Exception e, HttpServletResponse response, WebRequest request) {
        return String.valueOf(exception(e, response, request));
    }


    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler({AccessDeniedException.class})
    @ResponseBody
    public Object accessDenied(Exception e, WebRequest request) throws Exception {

        var res = ((ServletWebRequest) request).getRequest().getRequestURI();
        return ApiErrorDescription.of(e, res);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({DeleteNotPossibleException.class})
    @ResponseBody
    public String deleteNotPossible(Exception e) {
        return e.getMessage();
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({IllegalIdException.class})
    @ResponseBody
    public String illegalId(Exception e) {
        return e.getMessage();
    }


    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({AuthorizationFailedException.class})
    @ResponseBody
    public Object authorizationFailed(Exception e, WebRequest request, HttpServletResponse response) {
        request.setAttribute(REDIRECT_PARAM, request.getContextPath(), RequestAttributes.SCOPE_SESSION);
        return exception(e, response, request);
    }


    @SneakyThrows
    @ExceptionHandler({AuthorizationNotFoundException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Object authorizationNotFound(Exception e, WebRequest request, HttpServletResponse response) {
        var res = ((ServletWebRequest) request).getRequest().getRequestURI();

            response.setStatus(HttpStatus.FORBIDDEN.value());
            return new ObjectMapper().writeValueAsString(ApiErrorDescription.of(e, res));

    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ItemAlreadyExistsException.class})
    @ResponseBody
    public String itemAlreadyExists(Exception e) {
        return e.getMessage();
    }


    @SneakyThrows
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ItemWasDeletedException.class})
    @ResponseBody
    public String itemWasDeleted(Exception e, WebRequest request) {
        var res = ((ServletWebRequest) request).getRequest().getRequestURI();
        if (res.startsWith("/api/v")) {
            return new ObjectMapper().writeValueAsString(ApiErrorDescription.of(e, res));
        }
        return e.toString();
    }


    @SneakyThrows
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({HistoryObjectNotFoundException.class})
    @ResponseBody
    public String historyObjectNotFound(Exception e, WebRequest request) {
        var res = ((ServletWebRequest) request).getRequest().getRequestURI();
        if (res.startsWith("/api/v")) {
            return new ObjectMapper().writeValueAsString(ApiErrorDescription.of(e, res));
        }
        return e.toString();
    }

    @SneakyThrows
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({EditableObjectNotFoundException.class})
    @ResponseBody
    public String editableObjectNotFound(Exception e, WebRequest request) {
        var res = ((ServletWebRequest) request).getRequest().getRequestURI();
        if (res.startsWith("/api/v")) {
            return new ObjectMapper().writeValueAsString(ApiErrorDescription.of(e, res));
        }
        return e.toString();
    }

    @SneakyThrows
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(TranslationException.class)
    @ResponseBody
    public String translationException(Exception e, WebRequest request) {
        var res = ((ServletWebRequest) request).getRequest().getRequestURI();
        if (res.startsWith("/api/v")) {
            return new ObjectMapper().writeValueAsString(ApiErrorDescription.of(e, res));
        }
        return e.toString();
    }
}
