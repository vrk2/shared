package systems.dennis.shared.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.items.Transformable;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.exceptions.SearchException;
import systems.dennis.shared.service.AbstractService;
import systems.dennis.shared.utils.ApplicationContext;
import systems.dennis.shared.utils.bean_copier.BeanCopier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/api/v1/search/type/")
@CrossOrigin
public class SearchEntityApi extends ApplicationContext {

    private static Map<String, SearcherInfo> searchBeans = new HashMap<>();

    public static void registerSearch(String type, SearcherInfo info) {
        searchBeans.putIfAbsent(type, info);
    }

    public SearchEntityApi(WebContext context) {
        super(context);
    }

    public static Class<? extends AbstractService> findServiceByType(String searchName) {
        return searchBeans.get(searchName).getSearchClass();
    }


    @GetMapping(value = "/toString/{type}/{id}", produces = "text/plain;charset=UTF-8" )
    @WithRole
    @ResponseBody
    public ResponseEntity<String> findById(@PathVariable String type, @PathVariable Long id) {
        if (id < 1) {
            return ResponseEntity.ok("search_select");
        }

        var bean = getBean(searchBeans.get(type).getSearchClass());

        var form = bean.getClass().getAnnotation(DataRetrieverDescription.class).form();

        return ResponseEntity.ok(getContext().getBean(BeanCopier.class).copy(getBean(searchBeans.get(type).getSearchClass()).findById(id).orElseThrow(), form).asValue());
    }

    @GetMapping(value = "{type}", produces = "application/json")
    @WithRole
    public PageImpl<Map<String, Object>> GET(@PathVariable("type") String type,
                                                                        @RequestParam(value = "page", required = false) Integer page, @RequestParam (required = false) Integer limit,
                                                                        @RequestParam(value = "s", required = false) String what, @RequestParam (required = false) Long[] additionalIds) {



        var bean = getBean(searchBeans.get(type).getSearchClass());
        bean.preSearch(what, type);

            if (bean == null) {
            throw new SearchException(type);
        }

        if (page == null || page < 0) {
            page = 0;
        }

        var searchBean = searchBeans.get(type);

        var res =  bean.search(searchBean.getField(), what, page, limit == null ? 10 : limit, additionalIds);

        var pageList = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < res.getContent().size(); i++) {
            BaseEntity model = (BaseEntity) res.getContent().get(i);
            DefaultForm form = (DefaultForm) getBean(BeanCopier.class).copy(model, bean.getForm());
            pageList.add(BeanCopier.values(form, model));
        }
        return new PageImpl<>(pageList, res.getPageable(), res.getTotalElements());



    }



}
