package systems.dennis.shared.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.annotations.security.Secured;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.items.AddItemController;
import systems.dennis.shared.controller.items.DeleteItemController;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.ItemNotUserException;
import systems.dennis.shared.form.FavoriteItemForm;
import systems.dennis.shared.model.FavoriteItemModel;
import systems.dennis.shared.repository.DefaultSearchSpecification;
import systems.dennis.shared.repository.QueryCase;
import systems.dennis.shared.service.FavoriteException;
import systems.dennis.shared.service.FavoriteItemService;
import systems.dennis.shared.utils.ApplicationContext;
import systems.dennis.shared.utils.bean_copier.BeanCopier;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v2/shared/favorite")
@WebFormsSupport(value = FavoriteItemService.class)
@CrossOrigin
@Secured
public class FavoriteItemController extends ApplicationContext
        implements AddItemController<FavoriteItemModel, FavoriteItemForm>,
        DeleteItemController {

    public FavoriteItemController(WebContext context) {
        super(context);
    }



    @GetMapping("/enabled")
    @ResponseBody
    @WithRole
    public ResponseEntity<FavoriteResponse> enabled() {

        return ResponseEntity.ok(new FavoriteResponse(getService().getIsFavoriteEnabled(), ""));
    }



    @GetMapping("/byName")
    @ResponseBody
    @WithRole
    public ResponseEntity<FavoriteResponse> byName(@RequestParam("name") String name) {

        return ResponseEntity.ok(new FavoriteResponse(getService().getIsFavoriteEnabled(), getService().findBySearchName(name)));
    }



    @PostMapping("/is_favorite")
    @ResponseBody
    @WithRole
    public ResponseEntity<FavoriteResponse> isFavorite(@RequestBody FavoriteItemForm form) {
        return ResponseEntity.ok(new FavoriteResponse(getService().isFavoriteForObject(form), ""));
    }


    @PostMapping("/delete")

    public FavoriteResponse delete(@RequestBody FavoriteItemForm id) throws ItemNotUserException, ItemNotFoundException {
        getService().delete(id);
        return  new FavoriteResponse(true, "");
    }

    @GetMapping("/list/{type}/{searchName}")
    @ResponseBody
    @WithRole
    public ResponseEntity<Page<Map<String, Object>>> findByType(@PathVariable String type, @PathVariable("searchName") String name,
                                                             @RequestParam(value = "from", required = false) Long from,
                                                             @RequestParam(value = "limit", required = false) Integer limit,
                                                             @RequestParam(value = "page", required = false) Integer page) {

        if (!getService().getIsFavoriteEnabled()){
            return ResponseEntity.ok(Page.empty());
        }

        var searchBean = SearchEntityApi.findServiceByType(name);

        if (searchBean == null){
            throw new FavoriteException("global.exception.favorite.no_search_type_present");
        }

        DefaultSearchSpecification<FavoriteItemModel> specification = QueryCase.equalsOf("userDataId", getContext().getCurrentUser()).specification()
                .addCase(QueryCase.equalsOf("type", type));
        Page<FavoriteItemModel> models = getContext().getBean(FavoriteItemService.class).find(specification, from, limit, page);

        List<Long> ids = models.stream().map(FavoriteItemModel::getModelId).collect(Collectors.toList());

        var bean = getContext().getBean(searchBean);



        return ResponseEntity.ok(toFormPage(bean.find(QueryCase.in(BaseEntity.ID_FIELD, ids).specification(),0L,-1, 0), searchBean));
    }


    public Page<Map<String, Object>> toFormPage(Page<?extends  BaseEntity> page, Class<?> c) {
        var ann = c.getAnnotation(DataRetrieverDescription.class);
        var copier = getBean(BeanCopier.class);
        List<Map<String, Object>> data = new ArrayList<>();
        for (BaseEntity entity : page.getContent()){

            data.add(BeanCopier.values(copier.copy(entity, ann.form()), entity));
        }

        return new PageImpl<>(data, Pageable.unpaged(), data.size());
    }

    @Override
    public FavoriteItemService getService() {
        return AddItemController.super.getService();
    }
}
