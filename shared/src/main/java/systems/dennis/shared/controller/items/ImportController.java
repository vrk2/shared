package systems.dennis.shared.controller.items;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import systems.dennis.shared.annotations.security.Secured;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.config.WebConstants;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.exceptions.ItemForAddContainsIdException;
import systems.dennis.shared.importer.Importer;

import java.util.List;

@Secured
@CrossOrigin
public interface ImportController<DB_TYPE extends BaseEntity, FORM extends DefaultForm> extends Importer<DB_TYPE, FORM>,  Transformable<DB_TYPE, FORM> {
    @PostMapping(value = {WebConstants.WEB_API_IMPORT,WebConstants.WEB_API_IMPORT + "/" })
    @ResponseBody
    @WithRole
    default ResponseEntity<List<FORM>> startImport() throws ItemForAddContainsIdException {

        var res = this.fetchAndStore(getContext());
        return ResponseEntity.ok(res);
    }

    WebContext.LocalWebContext getContext();
}
