package systems.dennis.shared.controller.forms;

public interface QueryObject<T> {
    T get(String key);
}
