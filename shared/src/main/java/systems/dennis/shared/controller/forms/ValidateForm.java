package systems.dennis.shared.controller.forms;

import lombok.SneakyThrows;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.annotations.Validation;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.entity.KeyValue;
import systems.dennis.shared.exceptions.ValidationFailedException;
import systems.dennis.shared.pojo_form.ValidationContext;
import systems.dennis.shared.pojo_form.ValidationResult;
import systems.dennis.shared.service.AbstractService;
import systems.dennis.shared.utils.bean_copier.BeanCopier;
import systems.dennis.shared.validation.ValueValidator;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public interface ValidateForm<FORM extends DefaultForm> {

    @SneakyThrows
    default void validate(KeyValue keyValue) {
        var formClass = getClassOfForm();
        var field = BeanCopier.findField(keyValue.getKey(), formClass);

        Map<String, List<ValidationResult>> validationResult = new HashMap<>();
        validateField(keyValue, field, true, validationResult, keyValue.getValue(),  ((Serviceable) this).getServiceClass() );

        checkValidation(validationResult);
    }

    @SneakyThrows
    default void validate(FORM form, boolean edit) {
        validate(form, edit, ((Serviceable) this).getServiceClass() );
    }
    @SneakyThrows
    default void validate(FORM form, boolean edit, Class<? extends AbstractService> serviceable) {
        var formClass = serviceable.getAnnotation(DataRetrieverDescription.class).form();
        var validatedFields = BeanCopier.findAnnotatedFields(formClass, Validation.class);

        Map<String, List<ValidationResult>> validationResult = new HashMap<>();
        for (Field field : validatedFields) {
            validateField(form, field, edit, validationResult, BeanCopier.readValue(form, field), serviceable);
        }

        checkValidation(validationResult);
    }

    default void validateField(Object objectOfForm, Field field, boolean edit,
                               Map<String, List<ValidationResult>> validation, Object valueOfField, Class<? extends AbstractService> serviceClass)
            throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {


        var fieldValidation = field.getAnnotation(Validation.class);

        if (fieldValidation != null) {
            List<ValidationResult> validationResults = new ArrayList<>();

            for (Class<? extends ValueValidator> cl : fieldValidation.value()) {
                var validationClass = cl.getConstructor().newInstance();
                field.setAccessible(true);

                var validationResult = validationClass
                        .validate(serviceClass, objectOfForm, field.getName(), valueOfField, edit, getContext());

                validationResults.add(validationResult);
                field.setAccessible(false);
            }
            validation.put(field.getName(), validationResults);
        }
    }

    default void checkValidation(Map<String, List<ValidationResult>> validationResult) {
        ValidationContext context = new ValidationContext();
        context.setData(validationResult);

        if (!context.getValidationMessages().isEmpty()) {
            throw new ValidationFailedException(context);
        }
    }

    default Class<? extends DefaultForm> getClassOfForm() {
        return ((Serviceable) this).getForm();
    }

    WebContext.LocalWebContext getContext();
}
