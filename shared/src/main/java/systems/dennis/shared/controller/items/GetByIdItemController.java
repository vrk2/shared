package systems.dennis.shared.controller.items;

import systems.dennis.shared.annotations.security.Secured;
import systems.dennis.shared.controller.forms.Serviceable;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Secured
public interface GetByIdItemController <DB_TYPE extends BaseEntity, FORM extends DefaultForm> extends Serviceable {
    @SneakyThrows
    @GetMapping("/id/{id}")
    @ResponseBody
    default ResponseEntity<FORM> get(@PathVariable ("id") Long id) {
        DB_TYPE type = (DB_TYPE) getService().findById(id).orElseThrow(() -> new ItemNotFoundException(id));
        if (type == null){
            throw new ItemNotFoundException(id);
        }
        return ResponseEntity.ok(toForm(type));
    }

    FORM toForm(DB_TYPE item);

}
