package systems.dennis.shared.controller.items;


import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import systems.dennis.shared.annotations.security.Secured;
import systems.dennis.shared.controller.forms.Serviceable;
import systems.dennis.shared.annotations.security.WithRole;
import systems.dennis.shared.controller.items.magic.MagicList;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * List  object interface. Implementing this interface, will automatically create ../list/
 *
 * @param <DB_TYPE> Entity which is managed by this interface and Service
 */
@RestController
@Secured
public interface ListItemController<DB_TYPE extends BaseEntity, FORM extends DefaultForm> extends Transformable<DB_TYPE, FORM>, Serviceable, MagicList<DB_TYPE> {

    @GetMapping("/list")
    @ResponseBody
    @WithRole
    default ResponseEntity<List<FORM>> get(@RequestParam(value = "from", required = false) Long from,
                                           @RequestParam(value = "limit", required = false) Integer limit,
                                           @RequestParam(value = "page", required = false) Integer page) {

        Specification<BaseEntity> specification = getService().getAdditionalSpecification();
        List<DB_TYPE> data;

        if (specification != null) {
            data = getService().find(specification, from, limit, page).stream().map(x -> (DB_TYPE) x).collect(Collectors.toList());
        } else {
            data = getService().find(from, limit, page).stream().map(x -> (DB_TYPE) x).collect(Collectors.toList());
        }

        List<FORM> result = new ArrayList<>();
        for (DB_TYPE item : data) {
            result.add(toForm(item));
        }

        return ResponseEntity.ok(result);

    }
}
