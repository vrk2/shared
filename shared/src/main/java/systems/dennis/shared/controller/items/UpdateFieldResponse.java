package systems.dennis.shared.controller.items;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import systems.dennis.shared.entity.KeyValue;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateFieldResponse implements Serializable {
    private Long id;
    private KeyValue keyValue;
}
