package systems.dennis.shared.controller.delete_history;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.annotations.security.SelfOnlyRole;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.items.ListItemController;
import systems.dennis.shared.exceptions.HistoryObjectNotFoundException;
import systems.dennis.shared.form.DeleteHistoryForm;
import systems.dennis.shared.model.DeleteHistoryModel;
import systems.dennis.shared.service.DeleteHistoryService;
import systems.dennis.shared.utils.ApplicationContext;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v2/deleted/history")
@WebFormsSupport(value = DeleteHistoryService.class)
public class DeleteHistoryController extends ApplicationContext implements ListItemController<DeleteHistoryModel, DeleteHistoryForm> {


    public DeleteHistoryController(WebContext context) {
        super(context);
    }

    @GetMapping("/find/{type}/{id}")
    @SelfOnlyRole
    public DeleteHistoryForm findByDeletedIdAndType(@PathVariable String type, @PathVariable Long id) {
        DeleteHistoryService service = getService();
        return toForm(service.findByTypeAndDeletedId(type, id).orElseThrow(() -> new HistoryObjectNotFoundException(id, type)));
    }

    @GetMapping("/find/{type}")
    @SelfOnlyRole
    public List<DeleteHistoryForm> findByType(@PathVariable String type) {
        DeleteHistoryService service = getService();
        return service.findByType(type)
                .stream()
                .map(this::toForm)
                .collect(Collectors.toList());
    }
}
