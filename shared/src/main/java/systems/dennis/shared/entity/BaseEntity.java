package systems.dennis.shared.entity;

import jakarta.persistence.*;
import lombok.Data;

@MappedSuperclass
@Data
public class    BaseEntity implements DefaultForm {
    @Id
    @GeneratedValue (strategy = GenerationType.TABLE)
    private Long id;

    @Override
    public Long getId() {
        return id;
    }


}
