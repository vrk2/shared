package systems.dennis.shared.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import lombok.Data;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.Type;
import org.hibernate.type.SqlTypes;
import systems.dennis.shared.annotations.FormTransient;
import systems.dennis.shared.controller.items.magic.MagicQuery;
import systems.dennis.shared.controller.items.magic.MagicRequest;
import systems.dennis.shared.converters.MagicQueryConverter;

import java.util.List;

@Data
@Entity
public class TableSettingModel extends BaseEntity {

    private String name;

    @Column(columnDefinition = "varchar(1500)")
    private String topic;

    @ElementCollection
    private List<String> fieldsOrder;

    @Column(columnDefinition = "text")
    @JdbcTypeCode(SqlTypes.JSON)
    @Convert(converter = MagicQueryConverter.class)
    private MagicRequest magicQuery;

    private Boolean addToMenu;
    @Column(columnDefinition = "varchar(1500)")
    private String url;


    @FormTransient
    @Column (name = "c_user")
    private Long currentUser;

    @Override
    public String asValue() {
        return name;
    }
}
