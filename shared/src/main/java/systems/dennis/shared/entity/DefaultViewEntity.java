package systems.dennis.shared.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
public class DefaultViewEntity {
    private  static int nextVal = 0;
    @Id
    @Column(updatable = false, insertable = false)
    private Long id = random();

    private static long random() {
        return ++nextVal;
    }

    public void setId(Long id){

    }

    public Long getId(){
        return id;
    }
}
