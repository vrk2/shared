package systems.dennis.shared.entity;

/**
 when class implement this interface import start check this object should be imported;
 this interface should be implemented by form;
 */
public interface ImportFilter {

    /**
     *
     * @return true if object should be imported, false - otherwise;
     */
    boolean accept();
}
