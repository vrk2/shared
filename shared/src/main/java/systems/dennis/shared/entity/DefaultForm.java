package systems.dennis.shared.entity;

import org.springframework.data.domain.Sort;

import java.io.Serializable;

public interface DefaultForm extends Serializable {
    String ID_FIELD= "id";
    Long getId();

    default KeyValue defaultSearchOrderField(){
        return  new KeyValue(ID_FIELD,  Sort.Direction.DESC);
    }
    default String asValue(){
        return toString();
    }
}
