package systems.dennis.shared.mock;

import org.springframework.http.HttpMethod;

public interface PathToDataConverter {
     String convert(String s, HttpMethod query, Object data);
}
