package systems.dennis.shared.repository;

import systems.dennis.shared.model.FavoriteItemModel;

public interface FavoriteItemRepo extends PaginationRepository<FavoriteItemModel>{

}
