package systems.dennis.shared.repository;


import org.springframework.stereotype.Repository;
import systems.dennis.shared.entity.TableSettingModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface TableSettingRepo extends PaginationRepository<TableSettingModel> {

    List<TableSettingModel> findAllByTopicAndCurrentUser(String topic, Long user);

    Optional<TableSettingModel> findFirstByTopicAndNameAndCurrentUser(String topic, String name, Long currentUser);
}
