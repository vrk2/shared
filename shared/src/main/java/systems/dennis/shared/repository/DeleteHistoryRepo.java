package systems.dennis.shared.repository;

import org.springframework.stereotype.Repository;
import systems.dennis.shared.model.DeleteHistoryModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface DeleteHistoryRepo extends PaginationRepository<DeleteHistoryModel> {

    Optional<DeleteHistoryModel> findFirstByIdDeletedObjectAndClassDeletedObject(Long delId, String type);

    boolean existsByIdDeletedObjectAndClassDeletedObject(Long delId, String type);

    List<DeleteHistoryModel> findAllByClassDeletedObject(String type);
}
