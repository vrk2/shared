package systems.dennis.shared.repository;

import org.springframework.stereotype.Repository;
import systems.dennis.shared.model.EditHistoryModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface EditHistoryRepo extends PaginationRepository<EditHistoryModel> {

    Optional<EditHistoryModel> findFirstByClassEditedObjectAndIdEditedObject(String type, Long editedId);

    List<EditHistoryModel> findAllByClassEditedObject(String type);
}
