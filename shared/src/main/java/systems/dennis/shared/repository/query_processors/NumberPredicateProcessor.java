package systems.dennis.shared.repository.query_processors;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import systems.dennis.shared.repository.QueryCase;

import java.math.BigInteger;
import java.util.List;

@Slf4j
public class NumberPredicateProcessor extends AbstractClassProcessor{

    public NumberPredicateProcessor(QueryCase queryCase, Root root) {
        super(queryCase, root);
    }

    @Override
    public Object getValue(Object value) {
        try {
            if (value instanceof Number) {
                return getNumberFromString( String.valueOf(value), getQueryCase().getFieldClass());
            } else {
                return null;
            }

        } catch (Exception e) {
            log.warn("Search is not able to parse date: " + value, e);
            return null;
        }
    }

    private Number getNumberFromString(String value, Class<?> fieldClass) {

        if (Integer.class.equals(fieldClass)) {
            return Integer.valueOf(value);
        }
        if (Double.class.equals(fieldClass)) {
            return Double.valueOf(value);
        }
        if (Long.class.equals(fieldClass)) {
            return Long.valueOf(value);
        }
        if (Short.class.equals(fieldClass)) {
            return Short.valueOf(value);
        }

        if (BigInteger.class.equals(fieldClass)) {
            return BigInteger.valueOf(Long.valueOf(value));
        }

        throw new IllegalArgumentException(" cannot transform String to number! ");
    }

}
