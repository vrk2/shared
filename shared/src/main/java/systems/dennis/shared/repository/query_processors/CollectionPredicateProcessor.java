package systems.dennis.shared.repository.query_processors;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import systems.dennis.shared.repository.QueryCase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CollectionPredicateProcessor extends AbstractClassProcessor{

    public CollectionPredicateProcessor(QueryCase queryCase, Root root) {
        super(queryCase, root);
    }


    @Override
    public void processDefault(CriteriaBuilder criteriaBuilder, List<Predicate> predicates) {
        Expression<Collection<String>> collectionExpression = getPath(getRoot());
        QueryCase queryCase = getQueryCase();
        if (QueryCase.CONTAINS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.isMember(String.valueOf(queryCase.getValue()), collectionExpression));
        }
        if (QueryCase.NOT_CONTAINS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.isNotMember(String.valueOf(queryCase.getValue()), collectionExpression));
        }
        if (QueryCase.NOT_EMPTY.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.isNotEmpty(collectionExpression));
        }
        if (QueryCase.EMPTY.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.isEmpty(collectionExpression));
        }
        if (QueryCase.IN.equalsIgnoreCase(queryCase.getOperator())) {
            CriteriaBuilder.In inClause = criteriaBuilder.in(getPath(getRoot()));

            Collection cValue = (Collection) queryCase.getValue();
            cValue.forEach((x)-> inClause.value(x) );
            predicates.add(inClause);
        }
        if (QueryCase.NOT_IN.equalsIgnoreCase(queryCase.getOperator())) {
            CriteriaBuilder.In inClause = criteriaBuilder.in(getPath(getRoot()));

            Collection cValue = (Collection) queryCase.getValue();
            cValue.forEach((x)-> inClause.value(x) );
            predicates.add(criteriaBuilder.not(inClause));
        }
        if (QueryCase.NOT_EQUALS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            Collection cValue = (Collection) queryCase.getValue();

            List<Predicate> notEqualPredicates = new ArrayList<>();

            for (Object value : cValue) {
                Predicate notEqualPredicate = criteriaBuilder.notEqual(collectionExpression, value);
                notEqualPredicates.add(notEqualPredicate);
            }
            Predicate combinedNotEqualPredicates = criteriaBuilder.and(notEqualPredicates.toArray(new Predicate[notEqualPredicates.size()]));
            predicates.add(combinedNotEqualPredicates);
        }

    }

    @Override
    public Object getValue(Object value) {
        return value;
    }
}
