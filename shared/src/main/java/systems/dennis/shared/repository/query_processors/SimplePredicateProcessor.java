package systems.dennis.shared.repository.query_processors;

import jakarta.persistence.criteria.Root;
import systems.dennis.shared.repository.QueryCase;

public class SimplePredicateProcessor extends AbstractClassProcessor {
    public SimplePredicateProcessor(QueryCase queryCase, Root root) {
        super(queryCase, root);
    }

    @Override
    public Object getValue(Object value) {
        return value;
    }
}
