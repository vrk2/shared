package systems.dennis.shared.repository.query_processors;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import systems.dennis.shared.repository.QueryCase;

import java.text.SimpleDateFormat;
import java.util.List;

import static systems.dennis.shared.utils.bean_copier.DateAndStringConverter.DEFAULT_DATE_TEMPLATE;


@Slf4j
public class DatePredicateProcessor extends AbstractClassProcessor {
    public static final SimpleDateFormat formatter = new SimpleDateFormat(DEFAULT_DATE_TEMPLATE);
    public DatePredicateProcessor(QueryCase queryCase, Root root) {
        super(queryCase, root);
    }

    @Override
    public Object getValue(Object value) {

        try {
            value = formatter.parse(String.valueOf(getQueryCase().getValue()));
        } catch (Exception e) {
            log.warn("Search is not able to parse date: " + value, e);
            return null;
        }

        return value;
    }
}


