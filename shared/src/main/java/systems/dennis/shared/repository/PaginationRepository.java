package systems.dennis.shared.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Component;


@NoRepositoryBean
@Component
public interface PaginationRepository<T> extends CrudRepository<T, Long>, JpaSpecificationExecutor<T>  {
    Page<T> findAll(Pageable pageRequest);

    Page<T> findAll(Specification<T> specification, Pageable pageable);


    /**
     *
     * @param entity must not be {@literal null}.
     * @return
     * @param <S>
     *
     * @deprecated Use getService() save method instead of repository. Using this method can provoke different errors and unexpected behaviour
     */
    @Deprecated
    <S extends T> S save(S entity);


}
