package systems.dennis.shared.repository;

import systems.dennis.shared.model.AppSettingsModel;

public interface AppSettingsRepo extends PaginationRepository<AppSettingsModel>{
}
