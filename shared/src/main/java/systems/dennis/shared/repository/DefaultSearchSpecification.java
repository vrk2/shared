package systems.dennis.shared.repository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;

import jakarta.persistence.criteria.*;
import systems.dennis.shared.repository.query_processors.AbstractClassProcessor;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
@Slf4j
public class DefaultSearchSpecification<T> implements Specification<T> {

    private List<QueryCase> params;


    private boolean distinct = false;

    public DefaultSearchSpecification(List<QueryCase> params) {
        this.params = params;
    }

    public DefaultSearchSpecification(List<QueryCase> params, boolean distinct) {
        this.params = params;
        this.distinct = distinct;
    }

    public DefaultSearchSpecification(){
        this.params = new ArrayList<>();

    }

    public <E>DefaultSearchSpecification<E> addCase(QueryCase queryCase){
        this.params.add(queryCase);
        return (DefaultSearchSpecification<E>) this;
    }


    @Deprecated
    /**
     * Use {@link QueryCase#specification()} medthod instead
     */
    public DefaultSearchSpecification(QueryCase queryCase) {
        this.params = new ArrayList<>();
        addCase(queryCase);
    }

    @Override
    public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        query.distinct(distinct);

        for (QueryCase queryCase : params) {
            var qq = AbstractClassProcessor.processor(queryCase, root);

            if (!qq.isNotNullCase()){
                qq.addToNullOrNotNullPredicate(criteriaBuilder, root, predicates);
            } else {
                qq.processDefault(criteriaBuilder, predicates);
            }
        }
        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }




}