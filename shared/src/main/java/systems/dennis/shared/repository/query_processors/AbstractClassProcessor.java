package systems.dennis.shared.repository.query_processors;

import jakarta.persistence.criteria.*;
import lombok.Data;
import systems.dennis.shared.exceptions.SearchException;
import systems.dennis.shared.repository.QueryCase;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;


@Data
public abstract class AbstractClassProcessor {
    private QueryCase queryCase;
    private Root root;

    private Class<?> type = String.class;

    private Object parameter = null;

    public AbstractClassProcessor (QueryCase queryCase, Root root){

        this.queryCase = queryCase;
        this.root = root;

        if (queryCase.getFieldClass() == null && queryCase.getValue() != null){
            type = queryCase.getValue().getClass();
        }

    }

    public boolean isNotNullCase() {
        return !QueryCase.NOT_NULL_OPERATOR.equalsIgnoreCase(queryCase.getOperator())
                && !QueryCase.NULL_OPERATOR.equalsIgnoreCase(queryCase.getOperator());
    }
    public void addToNullOrNotNullPredicate(CriteriaBuilder criteriaBuilder, Root root, List<Predicate> predicates) {
        if (Objects.equals(getQueryCase().getOperator(), QueryCase.NULL_OPERATOR)) {
            predicates.add(criteriaBuilder.isNull(getPath(root)));
        } else {
            predicates.add(criteriaBuilder.isNotNull(getPath(root)));
        }
    }
    public  <T> Expression<T> getPath(Root root) {
        if (!getQueryCase().isComplex()) {
            return root.get(getQueryCase().getField());
        }

        String[] paths = getQueryCase().getJoinOn().split("\\.");
        Join join = null;
        for (String j : paths) {
            if (join == null) {
                join = root.join(j);
            } else {
                join = join.join(j);
            }
        }

        return join.get(getQueryCase().getField());
    }

    public void processDefault( CriteriaBuilder criteriaBuilder,  List<Predicate> predicates) {

        Object value = getValue(queryCase.getValue());

        if (value == null) {
            throw new ArithmeticException("Item is wrong type");
        }
        if (QueryCase.LESS_THEN.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.lessThan(getPath(root), (Date) value));
        }
        if (QueryCase.MORE_THEN.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.greaterThan(getPath(root), (Date) value));
        }
        if (QueryCase.LESS_EQUALS.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(getPath(root), (Date) value));
        }
        if (QueryCase.MORE_EQUALS.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(getPath(root), (Date) value));
        }


        if (QueryCase.NOT_NULL_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.isNotNull(getPath(root)));
        }
        if (QueryCase.NULL_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            predicates.add(criteriaBuilder.isNull(getPath(root)));
        }
        if (QueryCase.EQUALS_OPERATOR.equalsIgnoreCase(queryCase.getOperator()) && !String.class.isAssignableFrom(queryCase.getValue().getClass())) {
            predicates.add(criteriaBuilder.equal(getPath(root), value));
        }

        //There is a bug that excludes elements with a value of null from the search results
        if (QueryCase.NOT_EQUALS_OPERATOR.equalsIgnoreCase(queryCase.getOperator())) {
            addIsNullOrNotEqualPredicate(criteriaBuilder, predicates, value);
        }
        if (QueryCase.IN.equalsIgnoreCase(queryCase.getOperator())) {
            CriteriaBuilder.In inClause = criteriaBuilder.in(getPath(root));

            Collection cValue = (Collection) value;
            cValue.forEach((x)-> inClause.value(x) );
            predicates.add(inClause);
        }



    }

    protected void addIsNullOrNotEqualPredicate(CriteriaBuilder criteriaBuilder, List<Predicate> predicates, Object value) {
        predicates.add(
                criteriaBuilder.or(
                        criteriaBuilder.isNull(getPath(root)),
                        criteriaBuilder.notEqual(getPath(root), value)));
    }

    public abstract Object getValue(Object value);

    public static AbstractClassProcessor processor(QueryCase queryCase, Root root){
        if (queryCase.getFieldClass() != null && Collection.class.isAssignableFrom(queryCase.getFieldClass())){
            return new CollectionPredicateProcessor(queryCase, root);
        }

        if (Date.class.equals(queryCase.getFieldClass()) && !Date.class.isAssignableFrom(queryCase.getValue().getClass())){
            return new DatePredicateProcessor(queryCase, root);
        }

        if  (queryCase.getFieldClass() != null && (Number.class.isAssignableFrom(queryCase.getFieldClass()) || isPrimitiveNumber(queryCase.getFieldClass()))) {
            return new NumberPredicateProcessor(queryCase, root);
        }

        if (queryCase.getFieldClass() != null && (String.class.isAssignableFrom(queryCase.getFieldClass()))){
            return new StringPredicateProcessor(queryCase, root);
        } else {
            return new SimplePredicateProcessor(queryCase, root);
        }


    }

    private static boolean isPrimitiveNumber(Class c) {
        return int.class == c || long.class == c || short.class == c || double.class == c || float.class == c ;
    }

}
