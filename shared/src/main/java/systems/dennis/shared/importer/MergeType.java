package systems.dennis.shared.importer;

public enum MergeType {
    UPDATE, SKIP, ADD_NEW
}
