package systems.dennis.shared.importer;



import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Import {
    Class<?> performer() default Importer.class;
    String pathKey();
    String method() default "GET";
    Class<?> returnType();
    Authorization auth();
    MergeType mergeType() default MergeType.UPDATE;
    PojoChecker pojoChecker();
    Class<? extends DataHolder> dataHolder() default DataHolder.class;
    Class<? extends AbstractConditionalImport> conditional() default AbstractConditionalImport.class;
}
