package systems.dennis.shared.importer;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Authorization {
    Authorization NONE = new Authorization(){
        @Override
        public Class<? extends Annotation> annotationType() {
            return Authorization.class;
        }

        @Override
        public int type() {
            return 0;
        }

        @Override
        public String login() {
            return null;
        }

        @Override
        public String password() {
            return null;
        }
    };
    int BASIC = 0;
    int BEARER = 1;

    int type() default  BASIC;

    String login();

    String password();
}
