package systems.dennis.shared.importer;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.DefaultForm;

public interface AbstractConditionalImport<Form extends DefaultForm> {
    Boolean satisfy(Form form, WebContext.LocalWebContext context);
}
