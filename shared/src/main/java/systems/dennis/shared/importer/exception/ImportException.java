package systems.dennis.shared.importer.exception;

public class ImportException extends RuntimeException {
    public ImportException(String s) {
        super(s);
    }
}
