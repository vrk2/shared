package systems.dennis.shared.importer;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.BaseEntity;

public interface PojoExistsChecker {
    BaseEntity exists (Import imp, BaseEntity entity, WebContext.LocalWebContext context, String [] params);
}
