package systems.dennis.shared.importer;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.logging.log4j.util.Strings;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.importer.exception.ImportException;
import systems.dennis.shared.repository.QueryCase;
import systems.dennis.shared.utils.bean_copier.BeanCopier;

import jakarta.persistence.Column;
import java.lang.reflect.Field;
import java.util.List;

@NoArgsConstructor
public class PojoByFieldChecker implements PojoExistsChecker{
    @SneakyThrows
    @Override
    public BaseEntity exists(Import imp, BaseEntity entity, WebContext.LocalWebContext context, String[] params) {

        check(entity, params);


        Field field = BeanCopier.findField(params[0], entity.getClass());

        field.setAccessible(true);

        Object value = field.get(entity);

        if (value == null) {
            throw new ImportException("Field " + field.getName() + " is seems to be empty, but it should not");
        }

        field.setAccessible(false);

        String fieldName = field.getName();
        var annName = field.getAnnotation(Column.class);
        if (annName != null && !Strings.isEmpty(annName.name() )){
            fieldName = field.getAnnotation(Column.class).name();
        }

        QueryCase cs = QueryCase.equalsOfInSensitive(fieldName, value);

        List<? extends BaseEntity> res =  context.getBean(imp.pojoChecker().serviceClass()).find(cs.specification(), 0L, 1, 0).getContent();


        return res.size() > 0 ?  res.get(0) : null;
    }

    private void check(BaseEntity entity, String[] params) {
        if (entity == null) {
            throw new ImportException("Entity is null");
        }

        if (params == null || params.length < 1){
            throw new ImportException("No params set for the PojoChecker " + getClass());
        }
    }
}
