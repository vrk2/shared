package systems.dennis.shared.pojo_view.list;

import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.TEXT;

public @interface Remote {
    String searchField() default "";

    String searchName() default "";

    String searchType() default TEXT;

    String fetcher() default "";

    boolean editable() default true;





}
