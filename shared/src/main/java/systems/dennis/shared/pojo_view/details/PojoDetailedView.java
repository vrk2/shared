package systems.dennis.shared.pojo_view.details;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
public @interface PojoDetailedView {
    boolean enableTitle() default true;

    String title() default "";

    String [] actions() default {};

    String remoteType() default "";
    boolean enableLinksOnLinkedObjects() default true;
}
