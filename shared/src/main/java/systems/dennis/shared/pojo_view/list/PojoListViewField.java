package systems.dennis.shared.pojo_view.list;

import systems.dennis.shared.pojo_view.DefaultDataConverter;
import systems.dennis.shared.pojo_view.UIAction;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.TEXT;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
public @interface PojoListViewField {
    String NO_WITH_SPECIFICATION = "inherit";
    String SAME = "SAME_OBJECT_AS_FIELD_NAME";

    boolean visible() default true;

    boolean searchable() default false;

    boolean sortable() default true;

    String type() default TEXT;

    int order() default 0;

    UIAction[] actions() default {};

    Class<? extends DefaultDataConverter> dataConverter() default DefaultDataConverter.class;

    boolean showContent() default true;

    String format() default "";


    Class<?> fieldClass() default PojoListViewField.class;

    String widthInTable() default NO_WITH_SPECIFICATION;

    String css() default "";

    Remote remote() default @Remote( );

    String group() default "";

    boolean available() default true;

}
