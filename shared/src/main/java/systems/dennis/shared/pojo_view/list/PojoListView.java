package systems.dennis.shared.pojo_view.list;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
public @interface PojoListView {
    public static final String NO_WITH_SPECIFICATION = "NONE";
    String widthInTable() default NO_WITH_SPECIFICATION;


    String defaultSortParam() default "id";
    boolean defaultSortASC() default true;
    boolean showId() default false;

    int maxPageResults() default 15;
    boolean enableTitle() default true;

    String title() default "";

    String [] actions() default {};

    String favoriteType() default "";
    boolean enableSearching() default false;
}
