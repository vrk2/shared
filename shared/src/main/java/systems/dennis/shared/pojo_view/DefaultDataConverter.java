package systems.dennis.shared.pojo_view;

import systems.dennis.shared.config.WebContext;

public interface DefaultDataConverter<T, E> {
    default <F>F convert(T object, E data, WebContext.LocalWebContext context){
        return (F) object;
    }
}
