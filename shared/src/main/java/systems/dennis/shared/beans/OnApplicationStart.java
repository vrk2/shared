package systems.dennis.shared.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import systems.dennis.shared.config.WebContext;

@Configuration
public class OnApplicationStart  {

    @Autowired(required = false)
    @Bean
    public CommandLineRunner init(WebContext context, OnAppStart onAppStart){
        return args -> {
            if (onAppStart != null){
                onAppStart.onAppRun(context);
            }
        };
    }


}
