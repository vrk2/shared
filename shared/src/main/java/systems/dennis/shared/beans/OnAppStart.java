package systems.dennis.shared.beans;

import org.springframework.stereotype.Component;
import systems.dennis.shared.config.WebContext;

@Component
public interface OnAppStart {
    public void onAppRun(WebContext context);
}
