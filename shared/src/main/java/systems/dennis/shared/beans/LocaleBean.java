package systems.dennis.shared.beans;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.KeyValue;
import systems.dennis.shared.exceptions.TranslationException;
import systems.dennis.shared.utils.ApplicationContext;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@Slf4j
@Service
@SessionScope
public class LocaleBean extends ApplicationContext {

    public LocaleBean(WebContext context) {
        super(context);
    }

    private Map<String, List<KeyValue>> cache = new HashMap<>();

    @Value("${global.use_cache_translation:true}")
    private boolean useCache;

    @Value("${global.messages.path:./i18n}")
    private String messagesPath;


    public String transform(String localeName) {

        File directory = new File(messagesPath);
        if(directory == null || !directory.isDirectory()) throw new TranslationException(localeName);

        List<File> filesFromDirectory = Arrays.asList(directory.listFiles());


        for(File file : filesFromDirectory) {

            if(file.getName().contains(".properties")) {

                String fileNameWithoutProp = List.of(file.getName().split("\\.")).get(0);
                List<String> fileParts = List.of(fileNameWithoutProp.split("_"));
                if(fileParts.get(0).equals("messages") && fileParts.get(1).equals(localeName) && fileParts.size() == 3) {
                    return localeName + "_" + fileParts.get(2);
                }

            }

        }
        throw new TranslationException(localeName);

    }

    public List<KeyValue> getMessageTranslations(String lang) {
        if (lang == null || lang.isBlank()) {
            throw new TranslationException(lang);
        }

        if (useCache && cache.containsKey(lang)) {
            return cache.get(lang);
        } else {

            var i18n = messagesPath;

            if (i18n != null && Files.exists(Path.of(i18n))) {
                var langToLoad = getBean(LocaleBean.class).transform(lang);
                i18n = i18n + File.separator + "messages_" + langToLoad + ".properties";

                try {
                    Properties properties = new Properties();
                    properties.load(new InputStreamReader(new FileInputStream(i18n), StandardCharsets.UTF_8));

                    List<KeyValue> res = new ArrayList<>();
                    var items = properties.keys();

                    while (items.hasMoreElements()) {
                        var key = items.nextElement().toString();
                        res.add(new KeyValue(key, properties.getProperty(key)));
                    }
                    cache.put(lang, res);
                    return res;
                } catch (Exception e) {
                    if (useCache) {
                        cache.put(lang, Collections.singletonList(new KeyValue("error", "lang_not_supported")));
                    }
                }
            }
            return null;
        }
    }

    public void clearCache() {
        cache.clear();
    }
}
