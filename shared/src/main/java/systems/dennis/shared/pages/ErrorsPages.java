package systems.dennis.shared.pages;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;
import systems.dennis.shared.controller.GlobalExceptionController;

import static systems.dennis.shared.config.WebConstants.asPage;

@Controller
@RequestMapping ("/errors")
public class ErrorsPages {

    @GetMapping("/access/denied")
    public String getPage(Model model, WebRequest request){
        model.addAttribute("redirect", request.getAttribute(GlobalExceptionController.REDIRECT_PARAM, RequestAttributes.SCOPE_SESSION));
        return asPage("/errors", "/access-denied");
    }
}
