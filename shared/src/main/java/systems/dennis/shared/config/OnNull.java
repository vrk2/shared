package systems.dennis.shared.config;

public interface OnNull<T> {
    T def();
}
