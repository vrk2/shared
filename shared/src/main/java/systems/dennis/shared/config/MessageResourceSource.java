package systems.dennis.shared.config;

import lombok.SneakyThrows;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.AbstractResourceBasedMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

@Service
@Slf4j
public class MessageResourceSource  extends AbstractResourceBasedMessageSource {
    private final static Map<String, Properties> values = new HashMap<>();
    @Value("${global.messages.path: .}")
    private String messagesPath;

    public MessageResourceSource (){

    }

    @SneakyThrows
    public void init(String path){
        File dir = new File(path);
        if (!dir.exists() || !dir.isDirectory() || dir.listFiles() == null){
            log.error("Not a valid massage source dir " + dir.getAbsolutePath() );
            return;
            //throw new IllegalArgumentException("The source path doesn't exist or a file not a directory");
        }
        for (File file : dir.listFiles()){
            var ext = file.getName().replace(".properties", "");
            var locale = ext .substring(ext.length() -2).toLowerCase();
            Properties properties = new Properties();
            properties.load(new FileReader(file.getAbsoluteFile(), StandardCharsets.UTF_8));
            values.put(locale, properties);
        }
    }
    @Override
    protected MessageFormat resolveCode(String code, Locale locale) {
        if (values.size() == 0) {
            init(messagesPath);
        }
        Object res;
        try {
            var country = locale.getCountry().toLowerCase();
            if (country == null || country.length() == 0){
                //TODO NOT THE BEST BUT AT LEAST FIX
                country = locale.getLanguage().substring(3,5);

            }
            res = values.get(country).get(code);
        } catch (Exception e){
            res = null;
        }
        if (res == null){
            System.out.println(code);
            return new MessageFormat("[" + code + "]" );
        }

        return new MessageFormat(res.toString());
    }

    public void refresh() {
        synchronized (values){
            values.clear();
            init(messagesPath);
        }


    }
}
