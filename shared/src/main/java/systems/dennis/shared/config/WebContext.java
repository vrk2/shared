package systems.dennis.shared.config;

import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import systems.dennis.shared.annotations.security.ISecurityUtils;
import systems.dennis.shared.beans.LocaleBean;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.repository.QueryCase;
import systems.dennis.shared.utils.PaginationRequestUtils;
import systems.dennis.shared.utils.bean_copier.BeanCopier;

import java.util.*;

@AllArgsConstructor
@Data
@Service
@Slf4j
public class WebContext {
    public static final Map<String, Object> data = new HashMap<>();
    @Autowired
    private HttpServletRequest request;

    @Autowired
    private MessageResourceSource messages;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private PaginationRequestUtils paginationRequestUtils;

    @Autowired
    private Environment environment;

    @Autowired
    private LocaleBean localeBean;


    public Object toCache(String what, Object cache) {
        return data.put(what, cache);
    }

    public <T> T fromCache(String what, OnNull<T> def) {
        var res = data.get(what);
        if (res == null) return def.def();
        return (T) res;
    }

    @SneakyThrows
    public String getEnv(String key) {
        var res = environment.getProperty(key);
        if (res == null) {
            throw new ItemNotFoundException(key);
        }
        return res;
    }

    public interface Transformer<T> {
        T transform(String object);
    }

    public <T> T getEnv(String key, T def, Transformer<T> transformer) {
        var res = environment.getProperty(key);
        if (res == null) {
            return def;
        } else {
            return transformer.transform(res);
        }
    }

    public String getToken() {
        return getBean(ISecurityUtils.class).getToken();
    }

    private String getMessageTranslation(String header, String lang) {
        messages.resolveCode(header, new Locale(localeBean.transform(lang)));
        final String message = messages.getMessage(
                header.toLowerCase(),
                new Object[]{},
                "?_" + header + "_?",
                new Locale(localeBean.transform(lang))); //--> en_EN -> en_US

        if (message == null || (message.startsWith("?_") && message.endsWith("_?"))) {
            log.warn(header + " not found !");
        }
        return message;

    }

    private String getMessageTranslation(String header, String lang, Object... params) {
        return getMessageTranslation(header,new Locale(localeBean.transform(lang)) , params);
    }

    private String getMessageTranslation(String header, Locale locale, Object... params) {
        final String message = messages.getMessage(
                header.toLowerCase(),
                params,
                "?_" + header + "_?",
                locale);

        if (message == null || (message.startsWith("?_") && message.endsWith("_?"))) {
            log.warn(header + " not found !");
        }
        return message;

    }

    public Long currentUser() {
        return getBean(ISecurityUtils.class).getUserDataId();
    }

    private ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public <T> T getBean(Class<T> value) {
        return getApplicationContext().getBean(value);
    }


    public static class LocalWebContext {
        private final WebContext webContext;
        private String scope;

        public HttpServletRequest getRequest() {
            return webContext.getRequest();
        }

        public String httpParam(String param) {
            return webContext.getRequest().getParameter(param);
        }


        public String getMessageTranslation(String header, String lang) {
            return webContext.getMessageTranslation(
                    scope + "."
                            + header.toLowerCase(), lang);
        }


        public String getMessageTranslation(String header, String lang, Class parentScope) {
            return webContext.getMessageTranslation(
                    parentScope.getName() + "."
                            + header.toLowerCase(), lang);
        }

        public String getMessageTranslation(String header, String lang,  Object... params ) {
            return webContext.getMessageTranslation(
                    scope + "."
                            + header.toLowerCase(), lang,  params);
        }

        private LocalWebContext(String scope, WebContext context) {
            this.scope = scope;
            this.webContext = context;

        }

        public static LocalWebContext of(String scope, WebContext context) {
            return new LocalWebContext(scope, context);

        }

        @SneakyThrows
        public String getEnv(String key) {
            return webContext.getEnv(key);
        }

        public <T> T getEnv(String key, T def, Transformer<T> transformer) {
            return webContext.getEnv(key, def, transformer);
        }

        public Long getCurrentUser() {
            return getBean(ISecurityUtils.class).getUserDataId();
        }


        public Specification getRequestSpecification(Class<?> pojo, List<QueryCase> caseList) {
            return webContext.getPaginationRequestUtils().getFilteringParams(this, pojo, caseList);
        }

        public <T> T transform(Object item, Class<? extends T> testPlanViewClass) {
            return getBean(BeanCopier.class).copy(item, testPlanViewClass);
        }

        public String getScoped(String header) {
            return scope + "." + header;
        }

        public <T> T getBean(Class<T> bean) {
            return webContext.getApplicationContext().getBean(bean);
        }

    }

}
