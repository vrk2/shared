package systems.dennis.shared.utils.bean_copier;

import lombok.Data;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.utils.AddonComponent;

import java.io.Serializable;

@Data
public class ObjectDefinition implements Serializable {
    private long key;

    private String value;
    public ObjectDefinition(BaseEntity entity){
        if (entity == null){
            return;
        }
        this.key = entity.getId();
        this.value = entity.asValue();

        if (entity instanceof AddonComponent){
            this.additional = ((AddonComponent) entity).getAddon();
        }
    }
    private Object additional;
}
