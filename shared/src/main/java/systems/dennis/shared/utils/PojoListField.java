package systems.dennis.shared.utils;

import lombok.Data;

import java.util.List;

@Data
public class PojoListField {


    private Boolean visible;
    private Boolean searchable;
    private Boolean sortable;
    private Integer order;
    private List<String> actions;
    private Boolean showContent;
    private String format;

    private String type;

    private String remoteType;

    private String searchField;
    private String searchName;

    private String searchType;
    private String css;
    private String field;
    private String translation;
    private String group = null;
}
