package systems.dennis.shared.utils.bean_copier;

import systems.dennis.shared.config.WebContext;

public class BeanCopierTransformer implements AbstractTransformer {
    @Override
    public Object transform(Object object, DataTransformer dataTransformer, Class<?> toType,
                                                                                WebContext.LocalWebContext context) {
        if (context == null){
            return null;
        }
        return context.getBean(BeanCopier.class).copy(object, toType);
    }
}
