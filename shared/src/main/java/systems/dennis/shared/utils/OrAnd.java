package systems.dennis.shared.utils;

import java.util.Collection;
import java.util.Objects;

public class OrAnd {
    public static boolean or(Object item, Object ... compareWith){
        for (Object o : compareWith){
            if (Objects.equals(item, o)){
                return true;
            }
        }
        return false;
    }

    public static boolean and(Object item, Object ... compareWith){
        for (Object o : compareWith){
            if (!Objects.equals(item, o)){
                return false;
            }
        }
        return true;
    }

    public static boolean isNullOr(Object item, Object ... compareWith){
        if (item == null){
            return true;
        }

        return or(item, compareWith);
    }


    public static boolean isNullOrBool(Object item, Boolean ... compareWith){
        if (item == null){
            return true;
        }

        for (Boolean b: compareWith){
            if (b){
                return true;
            }
        }

        return false;
    }


    public static  <T> boolean isNullOrBool(T item, CheckTrue<T> onTrue){
        if (item == null){
            return true;
        }



        return onTrue.checkTrue(item);
    }


    public static boolean isEmpty(Collection<?> item){
        if (item == null){
            return true;
        }

        return item.isEmpty();
    }

    public interface CheckTrue<T>{
        boolean checkTrue(T x);
    }
}
