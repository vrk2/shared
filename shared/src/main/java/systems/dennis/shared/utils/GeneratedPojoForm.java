package systems.dennis.shared.utils;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
public class GeneratedPojoForm {

    public static GeneratedPojoForm INSTANCE;
    private String method;
    private String action;
    private String title;
    private Boolean showTitle;
    private String commitText;

    private Map<String, Object> value;

    private String objectType;


    private List<PojoFormField> fieldList = new ArrayList<>();
}
