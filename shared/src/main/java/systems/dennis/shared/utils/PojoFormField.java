package systems.dennis.shared.utils;

import lombok.Data;
import lombok.SneakyThrows;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.KeyValue;
import systems.dennis.shared.pojo_form.DataProvider;
import systems.dennis.shared.pojo_form.PojoFormElement;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Data
public class PojoFormField implements Serializable {
    private String translation;
    private Integer order;
    private String field;
    private String type;
    private boolean required;
    private boolean visible;
    private boolean showPlaceHolder;
    private boolean checked;
    private boolean showLabel;
    List<KeyValue> dataProviders = new ArrayList<>();
    private String searchField;
    private String searchType;
    private String searchName;
    private String fieldNote;

    private String placeHolder;

    private String remoteType;

    private String group;

    private boolean isId = false;

    @SneakyThrows
    public static PojoFormField from(Field x, WebContext.LocalWebContext context) {
        PojoFormField field  = new PojoFormField();

        PojoFormElement element = x.getAnnotation(PojoFormElement.class);
        field.setField(x.getName());
        field.setTranslation((context.getScoped(x.getDeclaringClass().getSimpleName() + "." + x.getName())).toLowerCase());
        field.setOrder( element == null  ? 0 : element.order());
        field.setRequired(element != null && element.required());
        field.setVisible(element != null && element.visible());
        field.setType(element == null ? "text" : element.type() );
        field.setPlaceHolder(element == null ? "" : field.placeHolder );
        field.setShowPlaceHolder(element != null && element.showPlaceHolder());
        field.setChecked(element != null && element.checked().checked());
        field.setShowLabel(element == null || element.showLabel());
        field.setGroup(null);


        field.setId(element != null && element.ID());
        if (element != null) {
            field.setSearchField(element.remote().searchField());
            field.setRemoteType(element.remote().fetcher());
            field.setSearchType(element.remote().searchType());
            field.setSearchName(element.remote().searchName());

            field.setFieldNote(element.fieldNote());
            field.setGroup(element.group());

            if (element.dataProvider() != null && element.dataProvider() != DataProvider.class) {
                var items = element.dataProvider().getConstructor().newInstance().getItems(context);
                field.getDataProviders().addAll(items);

            }
        }
        return field;
    }
}
