package systems.dennis.shared.utils;

import lombok.SneakyThrows;

public class ErrorSupplier<T> implements Supplier<T> {

    private final Exception e;

    public ErrorSupplier(Exception e){

        this.e = e;
    }

    @SneakyThrows
    @Override
    public T onNull() {
         throw e;
    }
}
