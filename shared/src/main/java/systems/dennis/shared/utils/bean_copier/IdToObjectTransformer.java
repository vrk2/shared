package systems.dennis.shared.utils.bean_copier;

import lombok.SneakyThrows;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.service.AbstractService;

public class IdToObjectTransformer implements AbstractTransformer {
    @SneakyThrows
    @Override
    public Object transform(Object object, DataTransformer transformerDescription, Class<?> toType, WebContext.LocalWebContext context) {
        if (object == null) {
            return null;
        }
        if (toType == Long.class){
            return ((BaseEntity) object).getId();
        }
        return  ((AbstractService) context.getBean(transformerDescription.additionalClass()))
                .findById((Long) object)
                .orElseThrow(()-> new ItemNotFoundException(String.valueOf(object)));
    }
}
