package systems.dennis.shared.utils.bean_copier;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.exceptions.AbstractTransformerException;

public interface AbstractTransformer {

    default boolean returnSameIfTypesAreEqual(Object from, Class<?> toType) {
        if (toType == null) {
            throw new AbstractTransformerException(getClass().getSimpleName());
        }
        return from == null || toType.isAssignableFrom(from.getClass());
    }

    Object transform(Object object, DataTransformer transformerDescription, Class<?> toType, WebContext.LocalWebContext context);
}
