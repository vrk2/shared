package systems.dennis.shared.utils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import systems.dennis.shared.config.WebContext;

@Data
@Slf4j
public class ApplicationContext{

    private final WebContext.LocalWebContext context;


    public <T> T getBean(Class<T> cl){
        return getContext().getBean(cl);
    }


    public ApplicationContext(WebContext context){
        this.context = WebContext.LocalWebContext.of(getClass().getSimpleName(), context);
    }

    public Long getCurrentUser(){
       return getContext().getCurrentUser();
    }

    public static Logger getDefaultLogger() {
        return log;
    }
}
