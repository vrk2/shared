package systems.dennis.shared.utils;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_view.DEFAULT_TYPES;
import systems.dennis.shared.pojo_view.DefaultDataConverter;
import systems.dennis.shared.pojo_view.list.PojoListViewField;
import systems.dennis.shared.pojo_view.UIAction;
import org.springframework.ui.Model;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;

import java.lang.reflect.Field;
import java.util.*;

import static systems.dennis.shared.utils.bean_copier.BeanCopier.findField;

@Slf4j
public class PojoHelper {
    public static List<PojoHeader> getHeaders(final Class<?> jobReportClass) {

        final List<PojoHeader> headers = new ArrayList<>();
        Field[] declaredFields = jobReportClass.getDeclaredFields();
        Field[] superDeclared= jobReportClass.getSuperclass().getDeclaredFields();

        for (Field field: superDeclared){
            getHeadersFromField(field, headers);
        }


        for (Field field : declaredFields) {
          getHeadersFromField(field, headers);
        }



        return headers;
    }

    private static void getHeadersFromField(Field field, List<PojoHeader> headers) {
        PojoListViewField pojoListViewField = field.getAnnotation(PojoListViewField.class);
        if (pojoListViewField != null) {
            var pojoFieldWidth =  pojoListViewField.widthInTable() ;
          
            headers.add(new PojoHeader(field.getName(),
                    null, pojoListViewField.order(),
                    field.getName(),
                    pojoListViewField.format() ,
                    pojoListViewField.dataConverter(),
                    getActions(pojoListViewField),
                    pojoListViewField.sortable()  ,
                    pojoListViewField.searchable(),
                    pojoListViewField.visible(),
                    pojoListViewField.remote().searchType(),
                    pojoListViewField.showContent(), pojoFieldWidth ));
        } else {
            if (!java.lang.reflect.Modifier.isStatic(field.getModifiers())){

                headers.add(new PojoHeader(field.getName(), null , 0, field.getName(), "" , DefaultDataConverter.class, null , true, true, true , DEFAULT_TYPES.TEXT ,  true, PojoListViewField.NO_WITH_SPECIFICATION));
            }
        }
    }

    private static UIAction[] getActions(PojoListViewField pojoListViewField) {
        return pojoListViewField.actions();
    }

    public static Object addData(PojoHeader header, Object dataObject, WebContext.LocalWebContext context) {
        String field = header.getField();
        Class<?> cz = dataObject.getClass();
        try {
            Field f = findField(field, cz);

            f.setAccessible(true);
            Object value = f.get(dataObject);

            if (header.getDataConverter() != DefaultDataConverter.class) {
                DefaultDataConverter converter = (DefaultDataConverter) header.getDataConverter().getDeclaredConstructor().newInstance();
                value = converter.convert(value, dataObject, context);
            }

            f.setAccessible(false);

            return value;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }



    public static List<Map<String, Object>> toMap(List<PojoHeader> headers, Page<?> values, WebContext.LocalWebContext context, Model model){
        var listResult = new ArrayList<Map<String, Object>>();
        sortData(headers);
        for (Object object : values) {

            //todo possible bug with ordering here
            //this is deprecated form creation part, so we can ignore it for a moment
            var result = new TreeMap<String, Object>();
            for (PojoHeader header : headers) {
                result.put(PojoHeader.get(header, context, false), addData(header, object, context));
            }
            listResult.add(result);
        }

        return listResult;

    }

    public static void sortData(List<PojoHeader> headers) {
        headers.sort(Comparator.comparing(PojoHeader::getOrder));
    }
}
