package systems.dennis.shared.utils.bean_copier;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface DataTransformer {
    Class<? extends AbstractTransformer> transFormWith() ;
    String [] params() default {};
    Class<?> additionalClass() default Class.class;
}
