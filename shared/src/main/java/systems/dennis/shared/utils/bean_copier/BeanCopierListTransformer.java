package systems.dennis.shared.utils.bean_copier;

import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.BaseEntity;

import java.util.ArrayList;
import java.util.List;

public class BeanCopierListTransformer implements AbstractTransformer {
    @Override
    public Object transform(Object object, DataTransformer dataTransformer, Class<?> toType, WebContext.LocalWebContext context) {

        var beanCopier = context.getBean(BeanCopier.class);
        List<?> items = (List<?>) object;
        List result = new ArrayList<>();

        items.forEach(x-> result.add(beanCopier.copy(x,  determineTargetClass(x.getClass(), dataTransformer))));

        return result;
    }

    private Class<?> determineTargetClass(Class<?> cl, DataTransformer dataTransformer) {
        if (BaseEntity.class.isAssignableFrom(cl)) {
            return dataTransformer.additionalClass().getAnnotation(DataRetrieverDescription.class).form();
        }
        return dataTransformer.additionalClass().getAnnotation(DataRetrieverDescription.class).model();
    }
}
