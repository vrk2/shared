package systems.dennis.shared.utils.security;

import org.springframework.security.core.Authentication;
import systems.dennis.shared.annotations.security.selfchecker.AbstractSelfChecker;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.exceptions.ItemNotUserException;
import systems.dennis.shared.utils.bean_copier.BeanCopier;

import java.util.Objects;

public class DefaultIdChecker implements AbstractSelfChecker {

    @Override
    public void check(Authentication dto, Object object, WebContext.LocalWebContext context) {
        Long userDataId = BeanCopier.readValue(object, "userDataId");

        if (!Objects.equals(context.getCurrentUser(), userDataId)) {
            throw new ItemNotUserException();
        }
    }
}
