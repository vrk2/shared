package systems.dennis.shared.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.Module;

import java.util.HashMap;

public class Mapper {
    public static final ObjectMapper mapper = createObjectMapper();

    private static HashMap<String, ObjectMapper> storage = new HashMap<>();

    private static ObjectMapper createObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    public static ObjectMapper instance(String name, Module... modules) {
        if (storage.get(name) != null) {
            return storage.get(name);
        }
        var map = createObjectMapper();
        for (Module module : modules) {
            map.registerModule(module);
        }
        storage.put(name, map);
        return map;
    }
}
