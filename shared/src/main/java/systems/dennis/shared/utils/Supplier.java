package systems.dennis.shared.utils;

public interface Supplier <T> {
    T onNull();
}

