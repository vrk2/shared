package systems.dennis.shared.utils.bean_copier;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import systems.dennis.shared.annotations.FormTransient;
import systems.dennis.shared.annotations.ObjectByIdPresentation;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.exceptions.BeanCopierException;
import systems.dennis.shared.utils.ApplicationContext;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service()
@Scope(value = "singleton")
public class BeanCopier extends ApplicationContext {

    public static final String GETTER_PREFIX = "get";
    public static final String SETTER_PREFIX = "set";
    public static final String IS_PREFIX = "is";


    public static final int TYPE_ALL_FIELDS = 0;
    public static final int TYPE_TRANSIENT_FIELDS = 1;
    public static final int TYPE_NON_TRANSIENT_FIELDS = 2;

    public BeanCopier(WebContext context) {
        super(context);
    }

    @SneakyThrows
    public <T> T copy(Object from, Class<T> cl) {
        if (from == null) {
            return null;
        }
        if (cl == null) {
            throw new BeanCopierException("Parameter \"c\" in method copy(Object from, Class<T> c) for class "
                    + getClass().getName() + " is null");
        }

        var inst = cl.getConstructor().newInstance();
        var fields = getFields(cl, new ArrayList<>());

        for (Field field : fields) {
            copyField(from, inst, field);
        }

        return inst;
    }

    public BaseEntity copyTransientFields(BaseEntity model, BaseEntity original) {
        var transientFields = getFields(original.getClass(), new ArrayList<>(), TYPE_TRANSIENT_FIELDS);
        for (Field field : transientFields) {
            copyField(original, model, field, true);
        }
        return model;
    }

    @SneakyThrows
    public <T> T clone(T object) {

        var fields = getFields(object.getClass(), new ArrayList<>(), TYPE_ALL_FIELDS);
        T inst = (T) object.getClass().getConstructor().newInstance();
        for (Field field : fields) {
            cloneField(object, inst, field);
        }

        return inst;
    }

    public static Field findField(String name, Class<?> c) {
        if (name == null || c == null) {
            return null;
        }

        try {
            return c.getDeclaredField(name);
        } catch (Exception e) {
            if (c.getSuperclass() != null) {
                return findField(name, c.getSuperclass());
            } else {
                throw new BeanCopierException("field " + name + " is not known for class ");
            }
        }
    }

    private void cloneField(Object from, Object to, Field fieldTo) {
        log.trace("Copying field: " + fieldTo.getName());
        if (fieldTo.getAnnotation(Transient.class) != null) return;
        try {
            var setter = findFieldAssociatedMethod(to.getClass(), fieldTo, SETTER_PREFIX);
            if (setter == null) {
                log.error("Field not found in " + to.getClass() + " ->  " + fieldTo.getName());
                return;
            }
            var getter = findFieldAssociatedMethod(from.getClass(), fieldTo, GETTER_PREFIX);
            if (getter != null) {
                try {
                    setter.invoke(to, getter.invoke(from));
                } catch (Exception e) {
                    log.debug("Something wrong with field " + fieldTo.getName(), e);
                }
            } else {
                log.info("Cannot find setter for:  " + fieldTo.getName());
            }
        } catch (Exception e) {
            log.error("Cannot copy field " + fieldTo.getName() + " to " + to.getClass(), e);
        }
    }


    private void copyField(Object from, Object to, Field fieldTo) {
        copyField(from, to, fieldTo, false);
    }

    private void copyField(Object from, Object to, Field fieldTo, boolean force) {
        log.debug("Copying field: " + fieldTo.getName());
        if ((fieldTo.getAnnotation(Transient.class) != null || fieldTo.getAnnotation(FormTransient.class) != null) && !force)
            return;
        try {
            var setter = findFieldAssociatedMethod(to.getClass(), fieldTo, SETTER_PREFIX);
            if (setter == null) {
                log.error("Field not found in " + to.getClass() + " ->  " + fieldTo.getName());
                return;
            }
            var getter = findFieldAssociatedMethod(from.getClass(), fieldTo, GETTER_PREFIX);
            if (getter != null) {
                try {
                    Field fromField = findField(fieldTo.getName(), from.getClass());
                    DataTransformer transformer = fromField.getAnnotation(DataTransformer.class);

                    if (transformer == null || to.getClass().equals(from.getClass())) {
                        setter.invoke(to, getter.invoke(from));
                    } else {
                        var converter = transformer.transFormWith().newInstance();
                        setter.invoke(to, converter.transform(getter.invoke(from), transformer, setter.getParameterTypes()[0], getContext()));
                    }
                } catch (Exception e) {
                    log.info("Something wrong with field " + fieldTo.getName(), e);
                }
            } else {
                log.debug("Cannot find setter for:  " + fieldTo.getName());
            }
        } catch (Exception e) {
            log.error("Cannot copy field " + fieldTo.getName() + " to " + to.getClass(), e);
        }
    }

    private static List<Field> getFields(Class<?> cl, List<Field> fields) {
        return getFields(cl, fields, TYPE_NON_TRANSIENT_FIELDS);
    }


    private static List<Field> getFields(Class<?> cl, List<Field> fields, int collectFieldType) {
        if (cl == null) {
            return null;
        }
        if (cl.getSuperclass().getSuperclass() != null) {

            getFields(cl.getSuperclass(), fields, collectFieldType);

        }

        var declaredFields = cl.getDeclaredFields();
        for (var field : declaredFields) {
            if (!java.lang.reflect.Modifier.isStatic(field.getModifiers())) {

                if (collectFieldType == TYPE_ALL_FIELDS) {
                    fields.add(field);
                }

                boolean isTransient = field.getAnnotation(Transient.class) != null || field.getAnnotation(FormTransient.class) != null;
                boolean onlyTransient = collectFieldType == TYPE_TRANSIENT_FIELDS;
                if ((onlyTransient && isTransient) || (!onlyTransient && !isTransient)) {
                    fields.add(field);
                }
            }
        }
        return fields;
    }

    public static void withEach(Class<?> c, WithEach withEach) {
        if (c != null && withEach != null) {
            var fields = new ArrayList<Field>();
            getFields(c, fields,  TYPE_TRANSIENT_FIELDS);
            getFields(c, fields);
            fields.forEach(withEach::with);

        }
    }

    public static boolean isFieldDefaultFormType(String field, Class<?> c){
        return DefaultForm.class.isAssignableFrom( findField(field, c).getType());
    }

    public static Map<String, Object> values(DefaultForm c, BaseEntity entity) {
        if (c == null) {
            return Collections.emptyMap();
        }
        final Map<String, Object> result = new LinkedHashMap<>();
        withEach(c.getClass(), x -> {
                    x.setAccessible(true);
                    try {

                        if (x.getAnnotation(ObjectByIdPresentation.class) != null && entity.getId() != null) {
                            var f = findField(x.getName(), entity.getClass());
                            f.setAccessible(true);
                            BaseEntity origin = (BaseEntity) f.get(entity);
                            f.setAccessible(false);

                            result.put(x.getName(), new ObjectDefinition(origin));
                        } else {

                            result.put(x.getName(), x.get(c));
                        }
                    } catch (IllegalAccessException e) {
                        log.error("Cannot get value from : " + x.getClass() + "." + x.getName());
                    } finally {
                        x.setAccessible(false);
                    }

                }
        );
        return result;
    }

    public interface WithEach {
        void with(Field object);
    }

    private static <T> T setOrGetFieldValue(Object object, Field field, Object value, String pref) {
        if (pref.equals("set") && (field.getAnnotation(Transient.class) != null || field.getAnnotation(FormTransient.class) != null)) {
            return (T) object;
        }

        var method = findFieldAssociatedMethod(object.getClass(), field, pref);
        if (method == null) {
            log.error("Field not found in " + object.getClass() + " ->  " + field.getName());
            return (T) value; // object is correct ?
        }
        try {
            if (Objects.equals(pref, GETTER_PREFIX) || Objects.equals(pref, IS_PREFIX)) {

                return (T) method.invoke(object);
            } else {
                method.invoke(object, value);
                return (T) object;
            }
        } catch (Exception e) {
            throw new BeanCopierException("field not found or not be read my method: " + e.getMessage());
        }
    }

    public static void setFieldValue(Object model, Field field, Object value) {
        setOrGetFieldValue(model, field, value, SETTER_PREFIX);
    }

    public static void setFieldValue(Object model, String field, Object value) {
        setOrGetFieldValue(model, findField(field, model.getClass()), value, SETTER_PREFIX);
    }

    public static <T> T readValue(Object from, Field field) {
        return setOrGetFieldValue(from, field, null, GETTER_PREFIX);
    }

    public static <T> T readValue(Object from, String field) {
        Field f = findField(field, from.getClass());
        return readValue(from, f);
    }

    public static List<Field> findAnnotatedFields(Class<?> c, Class<?> annotation) {
        List<Field> fieldRes = new ArrayList<>();
        if (c != null && annotation != null) {
            Class annotationClass = annotation;
            fieldRes = getFields(c, fieldRes).stream()
                    .filter((Field x) -> x.getAnnotation(annotationClass) != null)
                    .collect(Collectors.toList());
        }
        return fieldRes;
    }


    private static Method findFieldAssociatedMethod(Class<?> cl, Field f, String pref) {
        try {
            Method[] methods = cl.getDeclaredMethods();
            log.trace("Searching for method " + pref + firstCapitalize(f.getName()) + " in class " + cl.getName());
            for (Method method : methods) {
                if (method.getName().equals(pref + firstCapitalize(f.getName()))) {
                    return method;
                }
            }
            return findFieldAssociatedMethod(cl.getSuperclass(), f, pref);
        } catch (Exception e) {
            log.error("e");
            return null;
        }
    }

    private static String firstCapitalize(String what) {
        if (what == null || what.trim().length() == 0) {
            return null;
        }
        var res = Character.toTitleCase(what.charAt(0));
        return res + what.substring(1);
    }

}
