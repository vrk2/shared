package systems.dennis.shared.utils.bean_copier;

import lombok.extern.slf4j.Slf4j;
import systems.dennis.shared.config.WebContext;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Slf4j
public class DateAndStringConverter implements AbstractTransformer {
    public final static String DEFAULT_DATE_TEMPLATE = "dd.MM.yyyy";
    @Override
    public Object transform(Object object, DataTransformer dataTransformer, Class<?> type, WebContext.LocalWebContext context) {

        if ("".equals(object) || object == null || dataTransformer == null) {
            return null;
        }
        String template;
        try {
            template = dataTransformer.params()[0];
        } catch ( Exception e){
            template = DEFAULT_DATE_TEMPLATE;
        }
        if (returnSameIfTypesAreEqual(object, type)) {
            return object;
        }

        if (template == null){
            template = DEFAULT_DATE_TEMPLATE;
        }

        SimpleDateFormat formatter = new SimpleDateFormat(template.trim());

        if (object.getClass() == String.class) {
            try {
                return formatter.parse(((String) object).trim());
            } catch (ParseException e) {
                log.error("Cannot pars given date", e);
                return null;
            }
        }

        return formatter.format(object);
    }
}
