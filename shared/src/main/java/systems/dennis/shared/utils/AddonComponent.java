package systems.dennis.shared.utils;

public interface AddonComponent {
    Object getAddon();
}
