package systems.dennis.shared.mail;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EmailSender {

    @Value("${spring.mail.host:}")
    private String host;
    @Value("${spring.mail.port:25}")
    private Integer port;
    @Value("${spring.mail.password:}")
    private String password;
    @Value("${spring.mail.username:}")
    private String user;
//
//    @Bean
//    public JavaMailSender getJavaMailSender() {
//        var email = new JavaMailSenderImpl();
//
//        email.setHost(host);
//        email.setPort(port);
//        email.setPassword(password);
//        email.setUsername(user);
//
//        return email;
//    }

//    @Autowired JavaMailSender impl;

    public void sendSimpleMessage(
            String to, String subject, String text) {

//        SimpleMailMessage message = new SimpleMailMessage();
//        message.setFrom("info@app.unilect.tech");
//        message.setTo(to);
//        message.setSubject(subject);
//        message.setText(text);
//        getJavaMailSender().send(message);

    }
}
