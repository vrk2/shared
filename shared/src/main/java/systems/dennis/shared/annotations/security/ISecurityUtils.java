package systems.dennis.shared.annotations.security;

import org.springframework.stereotype.Service;
import systems.dennis.shared.entity.DefaultForm;

@Service
public interface ISecurityUtils {
    Long getUserDataId();

    void assignUser(DefaultForm pojo);

    void assignUser(DefaultForm form, DefaultForm pojoOriginal);

    default void isMy(Object o){ };

    String getToken();

    boolean isAdmin();

    boolean hasRole(String role);

    String getUserLanguage();
}
