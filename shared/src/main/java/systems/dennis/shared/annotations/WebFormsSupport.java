package systems.dennis.shared.annotations;

import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.service.AbstractService;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface WebFormsSupport {
    Class<? extends AbstractService<?>> value() ;

    /**
     * Overrides Service parameter model, when it's required
     * @return model
     */
    Class<? extends DefaultForm> form() default DefaultForm.class;



}
