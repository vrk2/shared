package systems.dennis.shared.annotations.security.selfchecker;
import org.springframework.security.core.Authentication;
import systems.dennis.shared.config.WebContext;

public interface AbstractSelfChecker<T> {
    void check(Authentication dto, T object, WebContext.LocalWebContext context);
}
