package systems.dennis.shared.annotations.security;

import systems.dennis.shared.annotations.security.selfchecker.AbstractSelfChecker;
import systems.dennis.shared.annotations.security.selfchecker.NoChecker;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Id {
    Class<? extends AbstractSelfChecker> checker() default NoChecker.class;

}
