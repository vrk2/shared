package systems.dennis.shared.annotations;

import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.repository.PaginationRepository;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DataRetrieverDescription {
    Class<? extends BaseEntity> model();
    Class<? extends DefaultForm> form();
    Class<? extends PaginationRepository<?>> repo();

}
