package systems.dennis.shared.annotations.security.selfchecker;

import org.springframework.security.core.Authentication;
import systems.dennis.shared.config.WebContext;

public class NoChecker implements AbstractSelfChecker {

    public NoChecker(){}

    @Override
    public void check(Authentication dto, Object object, WebContext.LocalWebContext context) {

    }
}
