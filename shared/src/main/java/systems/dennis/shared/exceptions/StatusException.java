package systems.dennis.shared.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;


@Data
public class StatusException extends RuntimeException {

    private HttpStatus code = HttpStatus.BAD_REQUEST;

    public StatusException(String s, HttpStatus code){
        super(s);
        this.code = code;
    }

}
