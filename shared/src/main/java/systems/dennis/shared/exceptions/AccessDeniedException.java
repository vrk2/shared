package systems.dennis.shared.exceptions;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AccessDeniedException extends StandardException {

    private String target;

    public AccessDeniedException(String message) {
        super(message, "global.exceptions.access_denied");
        this.target = message;
    }
}
