package systems.dennis.shared.exceptions;

import lombok.Data;

import java.io.Serializable;

@Data
public class EditFieldException extends StandardException {

    private String target;

    public EditFieldException(Serializable object) {
        super(object, "global.exceptions.object_field_is_null");
        this.target = object.toString();
    }
}
