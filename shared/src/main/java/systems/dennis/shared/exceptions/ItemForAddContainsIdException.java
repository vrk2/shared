package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class ItemForAddContainsIdException extends StandardException {
    public ItemForAddContainsIdException() {
        super(null, "global.exceptions.id_is_not_allowed_in_add_process");
    }
}
