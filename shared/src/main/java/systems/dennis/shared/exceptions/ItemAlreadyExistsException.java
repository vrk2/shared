package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class ItemAlreadyExistsException extends StandardException {

    private String target;

    public ItemAlreadyExistsException(String target) {
        super(target, "global.exceptions.item_already_exists");
        this.target = target;
    }
}
