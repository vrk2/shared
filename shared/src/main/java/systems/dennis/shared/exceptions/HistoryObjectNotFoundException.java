package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class HistoryObjectNotFoundException extends StandardWithIdException {
    private Long id;
    private String target;

    public HistoryObjectNotFoundException(Long id, String type) {
        super(id, type, "global.exceptions.item_not_found_in_uninstall_history");
        this.id = id;
        this.target = type;
    }
}
