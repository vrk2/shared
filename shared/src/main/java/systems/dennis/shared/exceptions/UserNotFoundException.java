package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class UserNotFoundException extends StandardException {

    private String target;

    public UserNotFoundException(String innerNumber) {
        super(innerNumber, "global.exceptions.user_not_found");
        this.target = innerNumber;
    }
}
