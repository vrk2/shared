package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class AbstractTransformerException extends StandardException {

    private String target;

    public AbstractTransformerException(String className) {
        super(className, "global.exceptions.transform.to_class_is_null");
        this.target = className;
    }
}
