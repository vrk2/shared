package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class ItemWasDeletedException extends StandardWithIdException {
    private Long id;
    private String target;

    public ItemWasDeletedException(Long id, Class cl) {
        super(id, cl.getSimpleName(), "global.exceptions.item_was_deleted");
        this.id = id;
        this.target = cl.getSimpleName();
    }
}
