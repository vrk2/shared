package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class SearchException extends StandardException {

    private String target;

    public SearchException(String type) {
        super(type, "global.exceptions.search_for_type_failed");
        this.target = type;
    }
}
