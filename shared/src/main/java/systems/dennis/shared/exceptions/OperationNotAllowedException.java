package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class OperationNotAllowedException extends StandardException {

    private String target;

    public OperationNotAllowedException(String s) {
        super(s, "global.exceptions.operation_not_allowed");
        this.target = s;
    }

    public OperationNotAllowedException() {
        super(null, "global.exceptions.operation_not_allowed");
    }
}
