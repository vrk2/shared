package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class ItemNotUserException extends AccessDeniedException {

    public ItemNotUserException() {
        super("object_does_not_belong_to_user");
    }
}
