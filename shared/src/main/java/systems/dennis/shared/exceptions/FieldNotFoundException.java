package systems.dennis.shared.exceptions;

import java.io.Serializable;

public class FieldNotFoundException extends StandardException {

    public FieldNotFoundException(Serializable target, String message) {
        super(target, message);
    }

    public FieldNotFoundException(String message) {
        super(null, message);
    }
}
