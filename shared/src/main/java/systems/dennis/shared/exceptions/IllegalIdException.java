package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class IllegalIdException extends StandardWithIdException {
    private Long id;
    private String target;

    public IllegalIdException(Long id) {
        super(id, null, "global.exceptions.illegal_id");
        this.id = id;
    }
}
