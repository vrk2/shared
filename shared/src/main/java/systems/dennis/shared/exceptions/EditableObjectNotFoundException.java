package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class EditableObjectNotFoundException extends StandardWithIdException {
    private Long id;
    private String target;

    public EditableObjectNotFoundException(String type, Long id) {
        super(id, type, "global.exceptions.item_not_found_in_history_edited_objects");
        this.id = id;
        this.target = type;
    }
}
