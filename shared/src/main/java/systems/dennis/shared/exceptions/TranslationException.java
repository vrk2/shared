package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class TranslationException extends StandardException {

    private String target;

    public TranslationException(String e) {
        super(e, "global.exceptions.language_is_not_supported");
        this.target = e;
    }
}
