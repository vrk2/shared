package systems.dennis.shared.exceptions;

import lombok.Data;
import systems.dennis.shared.entity.KeyValue;
import systems.dennis.shared.pojo_form.ValidationContext;

import java.util.ArrayList;
import java.util.List;

@Data
public class ValidationFailedException extends RuntimeException {
    private List<KeyValue> errorMessages = new ArrayList<>();
    public ValidationFailedException(ValidationContext context) {
        errorMessages =  context.getFieldErrors();
    }
}
