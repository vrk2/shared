package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class UnmodifiedItemSaveAttemptException extends StandardException {

    private String target;

    public UnmodifiedItemSaveAttemptException() {
        super(null, "global.exceptions.unmodified_item_save_attempt");
    }

    public UnmodifiedItemSaveAttemptException(String s) {
        super(s, "global.exceptions.unmodified_item_save_attempt");
        this.target = s;
    }
}
