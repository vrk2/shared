package systems.dennis.shared.exceptions;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class StandardException extends RuntimeException {
    private Serializable target;
    private String messageTranslate;

    public StandardException(Serializable target, String message) {
        super(message);
        this.target = target;
        this.messageTranslate = message;
    }
}
