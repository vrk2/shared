package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class ItemDoesNotContainsIdValueException extends StandardException {
    public ItemDoesNotContainsIdValueException() {
        super(null, "global.exceptions.id_should_be_set_before_editing_item");
    }
}
