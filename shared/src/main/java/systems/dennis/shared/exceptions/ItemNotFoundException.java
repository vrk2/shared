package systems.dennis.shared.exceptions;

import lombok.Data;
import systems.dennis.shared.entity.DefaultForm;

@Data
public class ItemNotFoundException extends StandardWithIdException {

    private Long id;
    private String target;

    public ItemNotFoundException(Long id) {
        super(id, null, "global.exceptions.not_found");
        this.id = id;
    }

    public <T extends DefaultForm> ItemNotFoundException(DefaultForm object) {
        super(object.getId(), object.getClass().getSimpleName(), "global.exceptions.not_found");
        this.target = object.getClass().getSimpleName();
        this.id = object.getId();
    }

    public ItemNotFoundException(String object) {
        super(null, object, "global.exceptions.not_found");
        this.target = object;
    }
}
