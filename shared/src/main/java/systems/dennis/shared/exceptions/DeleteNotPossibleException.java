package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class DeleteNotPossibleException extends StandardException {

    public DeleteNotPossibleException() {
        super(null, "global.delete.not.possible");
    }
}
