package systems.dennis.shared.exceptions;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class StandardWithIdException extends StandardException {
    private Serializable id;

    public StandardWithIdException(Serializable id, Serializable target, String message) {
        super(target, message);
        this.id = id;
    }
}
