package systems.dennis.shared.exceptions;

import lombok.Data;

@Data
public class AuthorizationNotFoundException extends StandardException {

    private String target;

    public AuthorizationNotFoundException(String message) {
        super(message, "global.exceptions.authorization_not_found");
        this.target = message;
    }
}
