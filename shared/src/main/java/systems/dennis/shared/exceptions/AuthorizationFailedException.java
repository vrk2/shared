package systems.dennis.shared.exceptions;

import lombok.Data;

/**
 * Only used in a case when user tries to login and login fails to process in api
 * For UI please use AccessDeniedAcception
 */
@Data
public class AuthorizationFailedException extends StandardException {

    private String target;

    public AuthorizationFailedException(String login) {
        super(login, "global.exceptions.authorization_failed_for_user");
        this.target = login;
    }
}
