package systems.dennis.shared.validation;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_form.ValidationResult;

public interface ValueValidator<FIELD_CLASS, VALUE_CLASS> {
    ValidationResult validate(Class<?> serviceClass, FIELD_CLASS element, String field, VALUE_CLASS value, boolean edit ,  WebContext.LocalWebContext context);

}
