package systems.dennis.shared.validation;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.pojo_form.ValidationResult;
import systems.dennis.shared.repository.DefaultSearchSpecification;
import systems.dennis.shared.repository.QueryCase;
import systems.dennis.shared.service.AbstractService;

public class FieldIsUniqueValidator implements ValueValidator<Object, Object> {
    @Override
    public ValidationResult validate(Class serviceClass, Object element, String field, Object value, boolean edit, WebContext.LocalWebContext context) {
        boolean isValueEmpty = value == null || value.toString().isEmpty();

        if (isValueEmpty) {
            return ValidationResult.PASSED;
        }

        DefaultSearchSpecification<?> spec = QueryCase.equalsOf(field, value).specification();
        if (edit) {
            spec.addCase(QueryCase.notEqualsOf("id", ((DefaultForm) element).getId()));
        }

        AbstractService<?> service = (AbstractService<?>) context.getBean(serviceClass);
        if (service.count(spec) > 0) {
            return ValidationResult.fail("field.not.unique");
        }


        return ValidationResult.PASSED;
    }
}
