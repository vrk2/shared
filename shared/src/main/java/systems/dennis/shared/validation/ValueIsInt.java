package systems.dennis.shared.validation;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_form.ValidationResult;

public class ValueIsInt implements ValueValidator {
    public static ValueIsInt DEFAULT = new ValueIsInt();
    private String field;

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public ValidationResult validate(Class serviceClass, Object element, String field, Object value, boolean edit, WebContext.LocalWebContext context) {
        this.field = field;
        ValidationResult result = new ValidationResult();
        result.setResult(value != null && !String.valueOf(value).isEmpty());

        if (value == null) {
            result.setErrorMessage("value.is.empty" + field);
            result.setResult(false);
        }

        try {
            var res = Integer.parseInt(value.toString());
            result.setResult(true);
        } catch (Exception e) {
            result.setErrorMessage("value.not.integer." + field);
            result.setResult(false);
        }
        return result;
    }
}
