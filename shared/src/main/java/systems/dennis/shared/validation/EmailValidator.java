package systems.dennis.shared.validation;

import systems.dennis.shared.annotations.security.ISecurityUtils;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_form.ValidationResult;


import java.util.regex.Pattern;

public class EmailValidator implements ValueValidator<Object, String> {
    private final Pattern regexPattern =  Pattern.compile("^(?=.{1,64}@)[A-Za-z0-9]+([._-]?[A-Za-z0-9]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");

    @Override
    public ValidationResult validate(Class serviceClass, Object element, String field, String value,
                                     boolean edit,  WebContext.LocalWebContext context) {

        if (context.getBean(ISecurityUtils.class).isAdmin()){
            return ValidationResult.PASSED;
        }
        value = value.trim();
        var ok = regexPattern
                .matcher(value)
                .matches();

        if (!ok) {
            return ValidationResult.fail("incorrect.email");
        }

        return ValidationResult.PASSED;
    }
}
