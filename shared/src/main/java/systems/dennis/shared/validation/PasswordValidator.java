package systems.dennis.shared.validation;

import lombok.extern.slf4j.Slf4j;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.exceptions.BeanCopierException;
import systems.dennis.shared.pojo_form.ValidationResult;
import systems.dennis.shared.utils.bean_copier.BeanCopier;

import java.util.regex.Pattern;

@Slf4j
public class PasswordValidator implements ValueValidator<Object, String> {
    private final Pattern regex =Pattern.compile( "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!*()_{};:,<.>/\\\\-])" +
            "[0-9a-zA-Z@#$%^&+=!*()_{};:,<.>/\\\\-]{8,}$");

    @Override
    public ValidationResult validate(Class serviceClass, Object element, String field, String value, boolean edit, WebContext.LocalWebContext context) {
        value = value.trim();
        var ok = regex
                .matcher(value)
                .matches();

        if (!ok){
            return ValidationResult.fail("incorrect.password");
        }

        try {
            if (!value.equals(BeanCopier.readValue(element, "repeatPassword"))) {
                return ValidationResult.fail("passwsord_does_not_match");
            }
        } catch (BeanCopierException e){
            return ValidationResult.PASSED;
        } catch (Exception e){
            log.error("Something wrong with validation ", e);
        }
       return ValidationResult.PASSED;
    }
}
