package systems.dennis.shared.validation;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_form.ValidationResult;

public class ValueIsIntAndMoreThenZero implements ValueValidator {
    public static ValueIsIntAndMoreThenZero DEFAULT = new ValueIsIntAndMoreThenZero();
    private String field;

    @Override
    public ValidationResult validate(Class serviceclass, Object element, String field, Object value, boolean edit, WebContext.LocalWebContext context) {
        this.field = field;
        ValidationResult result = new ValidationResult();
        result.setResult(value != null && !String.valueOf(value).isEmpty());

        if (value == null) {
            result.setErrorMessage("value.is.empty" + field);
            result.setResult(false);
        }

        try {
            var res = Integer.parseInt(value.toString());
            if (res < 1) {
                result.setErrorMessage("value.not.less.one." + field);
                result.setResult(false);
            } else {
                result.setResult(true);
            }
        } catch (Exception e) {
            result.setErrorMessage("value.not.integer." + field);
            result.setResult(false);
        }
        return result;
    }

    public void setField(String field) {
        this.field = field;
    }
}
