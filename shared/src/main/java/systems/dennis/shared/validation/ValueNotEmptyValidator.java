package systems.dennis.shared.validation;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_form.ValidationResult;


public class ValueNotEmptyValidator implements ValueValidator {
    public static ValueNotEmptyValidator DEFAULT = new ValueNotEmptyValidator();

    @Override
    public ValidationResult validate(Class serviceClass , Object element, String field, Object value, boolean edit, WebContext.LocalWebContext context) {


        var res = value != null && !String.valueOf(value).trim().isEmpty();

        if (!res){
            return ValidationResult.fail("value.not.set." + field );

        }else {
            return ValidationResult.PASSED;
        }
    }

}
