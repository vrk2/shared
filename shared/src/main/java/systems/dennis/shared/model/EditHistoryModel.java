package systems.dennis.shared.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import systems.dennis.shared.entity.BaseEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import java.util.Date;

@Data
@Entity(name = "edit_history_object")
@NoArgsConstructor
public class EditHistoryModel extends BaseEntity {

    @Column(name = "edited_id")
    private Long idEditedObject;

    private Date editedDate = new Date();

    @Column(name = "object_type")
    private String classEditedObject;

    @Column(columnDefinition = "text")
    private String oldContent;

    @Column(columnDefinition = "text")
    private String newContent;

    @SneakyThrows
    public static EditHistoryModel from(BaseEntity original, BaseEntity edited) {
        ObjectMapper objectMapper = new ObjectMapper();
        String oldContent = objectMapper.writeValueAsString(original);
        String newContent = objectMapper.writeValueAsString(edited);

        EditHistoryModel model = new EditHistoryModel();
        model.setIdEditedObject(original.getId());
        model.setClassEditedObject(original.getClass().getSimpleName());
        model.setOldContent(oldContent);
        model.setNewContent(newContent);
        return model;
    }
}
