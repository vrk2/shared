package systems.dennis.shared.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import systems.dennis.shared.entity.BaseEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import java.util.Date;

@Data
@Entity(name = "delete_history_object")
@AllArgsConstructor
@NoArgsConstructor
public class DeleteHistoryModel extends BaseEntity {

    @Column(name = "deleted_id")
    private Long idDeletedObject;

    private Date deletedDate = new Date();

    @Column(name = "object_type")
    private String classDeletedObject;

    @Column(columnDefinition = "text")
    private String content;

    @SneakyThrows
    public static DeleteHistoryModel from(BaseEntity deleted) {
        ObjectMapper objectMapper = new ObjectMapper();
        String content = objectMapper.writeValueAsString(deleted);

        DeleteHistoryModel model = new DeleteHistoryModel();
        model.setIdDeletedObject(deleted.getId());
        model.setClassDeletedObject(deleted.getClass().getSimpleName());
        model.setContent(content);
        return model;
    }
}
