package systems.dennis.shared.model;

import jakarta.persistence.Entity;
import lombok.Data;
import systems.dennis.shared.entity.BaseEntity;

@Data
@Entity
public class AppSettingsModel extends BaseEntity {
    private String key;

    private String value;
}
