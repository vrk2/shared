package systems.dennis.shared.model;

import jakarta.persistence.Entity;
import lombok.Data;
import systems.dennis.shared.annotations.FormTransient;
import systems.dennis.shared.entity.BaseEntity;

@Data
@Entity
public class FavoriteItemModel extends BaseEntity {

    private Long modelId;

    private String type;

    @FormTransient
    private Long userDataId;
}
