*-Version 2.2.4.2-*
Web details support
Edit field support



*-Version 2.2.4-*
Full form data support. 
Allows to get data about forms and list through json format 
Implementation is MagicList And MagicForm
Also includes JS support as magic.list.js and magic.js

Includes solution for Searching and column sorting ordering, displaying

Import calls now after import method and can be overridden by  implementation class
*-Version 2.1.3-*
Fixed problems with Validation context
added default language config

*-Version 2.1.2.3-*

Pagination fix. Independend dif
No pages shown when only 1 page is present


-Version 2.1.2-*

Added ImportController support

*-Version 2.1.0-*

 - [x] All default controllers are now returning FORM TYPE
 - [x] All DB_TYPE is now extended from BaseEntity
 - [x] DefaultForm is now Renamed to DefaultForm
 - [x] All FORM_TYPE is now extended from DefaultForm
 - [x] Import functional first implementation 
 - [x] DeleteFormController is not DELETED 
 - [x] API description is now strict DB_TYPE, FORM for all subsystems
